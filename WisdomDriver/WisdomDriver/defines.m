//
//  defines.m
//  WisdomDriver
//
//  Created by  apple on 13-1-12.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "defines.h"

NSString * HTTP_PREFIX = @"http://119.37.193.93/";

NSString * APPURL = @"http://itunes.apple.com/lookup?id=578294538";
NSString * APP_UPDATE_URL = @"itms-apps://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftwareUpdate?id=578294538&mt=8";

//百度地图Key
NSString * MAP_KEY = @"6FC39A11AC5208CB8EF2DBDEAFEF372D5D3A23DB";

NSString * kNotifyUpdateLocation    = @"updateUserLocation";
NSString * kDefaultAgentID          = @"AgentID";
NSString * kDefaultUserTel          = @"UserTelPhone";
NSString * kDefaultPassword         = @"Password";
NSString * kDefaultNickName         = @"NickName";
NSString * kDefaultFirstTimeRun     = @"FirstTimeRun";
NSString * kDefaultIsAutoLogin      = @"IsAutoLogin";
NSString * kDefaultIsLoginRecord    = @"IsLoginRecord";
NSString * kDefaultIsVibrationAlert = @"isVibrationAlert";
NSString * kDefaultIsSoundAlert     = @"isSoundAlert";

//二维码
NSString * kQRCodeImageName         = @"QRCode.png";
NSString * kQRCodeDimensionalCode   = @"dimensionalCode";

//用户信息
NSString * kUserInfoClientID        = @"clientID";
NSString * kUserInfoClientLogo      = @"clientLogo";
NSString * kUserInfoNickName        = @"name";
NSString * kUserInfoSex             = @"sex";
NSString * kUserInfoSexMale         = @"男";
NSString * kUserInfoSexFemale       = @"女";
NSString * kUserInfoAgentID         = @"agentID";

//司机信息
NSString * kDriverName              = @"driverName";
NSString * kDriverTel               = @"driverTel";
NSString * kDriverAgentName         = @"agentName";
NSString * kDriverAgentTel          = @"agentTel";
NSString * kDriverLongitude         = @"driverLon";
NSString * kDriverLatitude          = @"driverLat";
NSString * kDriverStatus            = @"driverStatus";

//常用地址相关
NSString * kAddrStr         = @"AddrName";
NSString * kAddrLatitude    = @"AddrLatitude";
NSString * kAddrLogitude    = @"AddrLongitude";
NSString * kAddrAccuracy    = @"AddrAccuracy";

//分享app文字内容
NSString * kShareShortMsg   = @"我发现了一个好代驾应用：智代驾，快来体验下吧，很好用、很方便！";
NSString * kShareSinaWeibo  = @"sina,我发现了一个好代驾应用：智代驾，快来体验下吧，很好用、很方便！";
NSString * kShareQQWeibo    = @"qq,我发现了一个好代驾应用：智代驾，快来体验下吧，很好用、很方便！";

//预约清单Key
NSString * kReserveNumber       = @"orderID";
NSString * kReserveStartAddr    = @"startAddress";
NSString * kReserveEndAddr      = @"endAddress";
NSString * kReserveDriveTime    = @"daijiaTime";
NSString * kReserveDriverCount  = @"driverNum";
NSString * kReserveDriverName   = @"driverName";
NSString * kReserveDriverTel    = @"driverTel";
NSString * kReserveRateStandard = @"ratestandard";

//电话号码
NSString * kPhoneCustomService  = @"4001-120-120";
NSString * kPhoneOrderService   = @"4001-120-120";

//会员卡
NSString * kMemCardNumer        = @"mcardID";
NSString * kMemCardCreateTime   = @"fakaTime";
NSString * kMemCardType         = @"cardType";
NSString * kMemCardCash         = @"cash";
NSString * kMemCardGiftCash     = @"giftCash";
NSString * kMemCardConsumeCount = @"consumeNum";

//消费记录
NSString * kConsumeOrderID      = @"orderID";
NSString * kConsumeGuestTel     = @"guestTel";
NSString * kConsumeStartAddr    = @"startAddress";
NSString * kConsumeEndAddr      = @"endAddress";
NSString * kConsumeDaijiaTime   = @"daijiaTime";
NSString * kConsumeEndTime      = @"endTime";
NSString * kConsumeDriverNum    = @"driverNum";
NSString * kConsumeDriverName   = @"driverName";
NSString * kConsumeAgentName    = @"agentName";
NSString * kConsumeTotalPrice   = @"totalPrice";

/*
Result中各数字代表的值
0-表示失败，1-表示成功，2-已注册，3-验证码超时，4-验证码错误5-原始密码错误
6-手机号码与发送验证码的手机不匹配，7-sessionID无效，8-请先获取手机验证码9-该订单已评价，-1-该用户未绑定会员卡无法生成二维码 -2-该用户未绑定会员卡无法请客
 */
//错误信息
NSString * kSOAPError0 = @"失败";
NSString * kSOAPError1 = @"成功";
NSString * kSOAPError2 = @"该手机号已注册，请更换新的手机号";
NSString * kSOAPError3 = @"验证码获取失败，请重新获取";
NSString * kSOAPError4 = @"验证码错误，请从新输入";
NSString * kSOAPError5 = @"原始密码错误";
NSString * kSOAPError6 = @"手机号码与发送验证码的手机不匹配";
NSString * kSOAPError7 = @"sessionID无效";
NSString * kSOAPError8 = @"请先获取手机验证码";
NSString * kSOAPError9 = @"该订单已评价，请勿重复评价";

NSString * kAgreementText = @"agreement.html";
