//
//  OrderJudgeVC.m
//  WisdomDriver
//
//  Created by  apple on 13-1-20.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "OrderJudgeVC.h"
#import "RunTimeShare.h"
#import "ClientJSONObject.h"
#import "SVProgressHUD.h"
#import "defines.h"

@interface OrderJudgeVC ()<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>{
    NSMutableDictionary * consumeDict;
    UITableView * judgeTable;
    UITextView * judgeText;
    int grade;
    int star;
    
    NSMutableArray * gradeBtnArr;
    NSMutableArray * starBtnArr;
}

@end

@implementation OrderJudgeVC
-(id)initWithBasicDict:(NSDictionary*)dict{
    self = [super init];
    consumeDict = [[NSMutableDictionary alloc] initWithDictionary:dict];
    return  self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    [self addTopViewWithTitle:@"评价"];
    
    grade = 3;
    star = 1;
    [self addJudgeCtrl];
}

#pragma mark - 添加顶部条栏和文字,返回按钮
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //返回按钮
    UIButton * backBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 58.0, 30.0)];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [backBut setTitle:@"返回" forState:UIControlStateNormal];
    [backBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBut addTarget:self action:@selector(clickOnBackBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBut];
    [backBut release];
    
    //编辑(提交)按钮，默认是编辑状态
    UIButton * settingBut = [[UIButton alloc] initWithFrame:CGRectMake(256.0, 8.0, 58.0, 30.0)];
    [settingBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [settingBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [settingBut setTitle:@"提交" forState:UIControlStateNormal];
    [settingBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [settingBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [settingBut addTarget:self action:@selector(clickOnSubmitBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:settingBut];
    [settingBut release];
}

-(void)clickOnBackBut{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 提交按钮
-(void)clickOnSubmitBut{
    [self showWithStatusMsg:@"正在提交评价"];
    [self performSelector:@selector(uploadJudge) withObject:nil afterDelay:0.5];
}

-(void)uploadJudge{
    //orderID=&userTel=&driverName=&evaluate=&level=&star=
    NSString * orderID = [consumeDict valueForKey:kConsumeOrderID];
    NSString * evaluate = judgeText.text;
    NSString * level = [NSString stringWithFormat:@"%d",grade];
    NSString * starStr = [NSString stringWithFormat:@"%d",star];
    
    NSDictionary * dict = [[ClientJSONObject clientJSONObject] evaluateWithOrderID:orderID evalute:evaluate level:level star:starStr];
    [self dismiss];
    
    NSInteger result = [[dict valueForKey:@"result"] integerValue];
    if (result == 1) {
        [self dismissSuccessMsg:@"评价成功，欢迎继续使用"];
        [self.navigationController popViewControllerAnimated:TRUE];
    }else if(result == 0){
        [self dismissErrorMsg:@"评价失败，请重新评价"];
    }else if(result == 9){
        [self dismissErrorMsg:@"已评价，请勿重复评价"];
    }
}

#pragma mark - 添加评价控件
-(void)addJudgeCtrl{
    //加载常用地址数据列表AddressTable
    UITableView * contentTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 50.0, 320.0, 315.0)
                                                style:UITableViewStyleGrouped];
    contentTable.delegate = self;
    contentTable.dataSource = self;
    contentTable.backgroundColor = [UIColor clearColor];
    contentTable.bounces = NO;
    contentTable.scrollEnabled = NO;
    [self.view addSubview:contentTable];
    [contentTable release];
    
    UILabel * tintLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 350, 300, 15)];
    tintLb.backgroundColor = [UIColor clearColor];
    tintLb.font = [UIFont systemFontOfSize:14.0];
    tintLb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    tintLb.tag = 1000;
    tintLb.text = @"如：你对下单、等候时间、代驾过程、结算等";
    [self.view addSubview:tintLb];
    [tintLb release];
    
    [contentTable reloadData];
}

#pragma mark - UITableView的协议处理
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return (indexPath.row == 0) ? 120 : 45;
    }
    return 45.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellIdentifier = @"ConsumeCell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
        cell.userInteractionEnabled = YES;
    }
    for (UIView * subView in cell.subviews) {
        if (subView.tag>100) {
            [subView removeFromSuperview];
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            //评价内容
            judgeText = [[UITextView alloc] initWithFrame:CGRectMake(20, 0, 280, 110)];
            judgeText.backgroundColor = [UIColor clearColor];
            judgeText.font = [UIFont systemFontOfSize:17.0];
            judgeText.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
            judgeText.tag = 1000;
            judgeText.text = @"输入您对司机的评价";
            judgeText.delegate = self;
            [cell addSubview:judgeText];
        }else if (indexPath.row == 1){
            //等级
            gradeBtnArr = [[NSMutableArray alloc] init];
            for (int i = 0; i < 3; i++) {
                UIButton * btn = [[UIButton alloc] initWithFrame:CGRectMake(25+100*i, 0, 40, 40)];
                NSString * imgName = (grade == (3-i)) ? @"btn_checkbuttondown.png" : @"btn_checkbuttonup.png";
                [btn setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
                btn.tag = 3-i;
                [btn addTarget:self action:@selector(gradeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
                [cell addSubview:btn];
                [gradeBtnArr addObject:btn];
                [btn release];
                
                UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(67+100*i, 10, 20, 20)];
                lb.font = [UIFont systemFontOfSize:14.0];
                lb.backgroundColor = [UIColor clearColor];
                lb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
                [cell addSubview:lb];
                [lb release];
                
                if (i == 0) {
                    lb.text = @"好";
                }else if (i == 1){
                    lb.text = @"中";
                }else if (i == 2){
                    lb.text = @"差";
                }
            }
        }
    }else if (indexPath.section == 1){
        if (indexPath.row == 0) {
            UILabel * titleLb = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 280, 45)];
            titleLb.backgroundColor = [UIColor clearColor];
            titleLb.font = [UIFont systemFontOfSize:14.0];
            titleLb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
            titleLb.tag = 1000;
            titleLb.text = @"点击小星星就能给整个代驾过程的服务打分";
            [cell addSubview:titleLb];
            [titleLb release];
        }else if (indexPath.row == 1){
            //星星
            starBtnArr = [[NSMutableArray alloc] init];
            for (int i = 0; i < 5; i++) {
                UIButton * btn = [[UIButton alloc] initWithFrame:CGRectMake(20+50*i, 2, 40, 40)];
                NSString * imgName = (i < star) ? @"btn_stardown.png" : @"btn_starup.png";
                [btn setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
                btn.tag = i;
                [btn addTarget:self action:@selector(starBtnClick:) forControlEvents:UIControlEventTouchUpInside];
                [cell addSubview:btn];
                [starBtnArr addObject:btn];
                [btn release];
            }
        }
    }
        
    return cell;
}

#pragma mark - 评价控件事件
-(void)gradeBtnClick:(UIButton*)btn{
    grade = btn.tag;
    [self updateGrade];
}

-(void)starBtnClick:(UIButton*)btn{
    star = btn.tag + 1;
    [self updateStar];
}

#pragma mark - 刷新评价控件
-(void)updateGrade{
    for (int i = 0; i < gradeBtnArr.count; i++) {
        UIButton * tmpBtn = [gradeBtnArr objectAtIndex:i];
        if (i == (gradeBtnArr.count - grade)) {
            //tmpBtn.backgroundColor = [UIColor yellowColor];
            [tmpBtn setBackgroundImage:[UIImage imageNamed:@"btn_checkbuttondown.png"] forState:UIControlStateNormal];
        }else{
            //tmpBtn.backgroundColor = [UIColor greenColor];
            [tmpBtn setBackgroundImage:[UIImage imageNamed:@"btn_checkbuttonup.png"] forState:UIControlStateNormal];
        }
    }
}

-(void)updateStar{
    for (int i = 0; i < starBtnArr.count; i++) {
        UIButton * tmpBtn = [starBtnArr objectAtIndex:i];
        if (i < star) {
            //tmpBtn.backgroundColor = [UIColor yellowColor];
            [tmpBtn setImage:[UIImage imageNamed:@"btn_stardown.png"] forState:UIControlStateNormal];
        }else{
            //tmpBtn.backgroundColor = [UIColor greenColor];
            [tmpBtn setImage:[UIImage imageNamed:@"btn_starup.png"] forState:UIControlStateNormal];
        }
    }
}

#pragma mark -
#pragma mark 显示提醒框

- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框

- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}

#pragma - mark 键盘隐藏
-(void)touchesEnded: (NSSet *)touches withEvent: (UIEvent *) event {
    NSLog(@"touchesEnded:withEvent:");
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([judgeText isFirstResponder] && [touch view] != judgeText) {
        [judgeText resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

#pragma mark - 输入框UITextField的协议处理
- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    if(textView.text == nil || textView.text.length == 0){
        textView.text = @"输入您对司机的评价";
    }
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"输入您对司机的评价"]) {
        textView.text = @"";
    }
}

//按下Done按钮的调用方法，我们让键盘消失
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
    
}
@end
