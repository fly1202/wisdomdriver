//
//  ConsumeListVC.m
//  WisdomDriver
//
//  Created by  apple on 13-1-20.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "ConsumeListVC.h"
#import "RunTimeShare.h"
#import "ClientJSONObject.h"
#import "SVProgressHUD.h"
#import "ConsumeDetailVC.h"
#import "defines.h"

@interface ConsumeListVC ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableDictionary * cardDict;
    NSMutableArray * selfArr;
    NSMutableArray * guestArr;
    NSMutableArray * titleArr;
    UITableView * consumeTable;
    
    BOOL isSelfSeg;
    UIButton * selfBtn;
    UIButton * guestBtn;
}

@end

@implementation ConsumeListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    //顶部条栏和文字处理，取消按钮
    [self addTopViewWithTitle:@"预约清单"];
    
    [self addConsumeTable];
    
    [self showWithStatusMsg:@"正在获取消费记录"];
    [self performSelector:@selector(getConsumeArr) withObject:nil afterDelay:0.5];
}

#pragma mark - 添加顶部条栏和文字,返回按钮
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //我的记录、请客记录
    isSelfSeg = TRUE;
    UIImageView * segBg = [[UIImageView alloc] initWithFrame:CGRectMake(95, 8, 130, 30)];
    segBg.image = [UIImage imageNamed:@"btn_segbg.png"];
    segBg.userInteractionEnabled = TRUE;
    [self.view addSubview:segBg];
    [segBg release];
    
    selfBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 64, 30)];
    [selfBtn.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [selfBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [selfBtn setTitle:@"我的记录" forState:UIControlStateNormal];
    [selfBtn setBackgroundImage:[UIImage imageNamed:@"btn_segleftselected2.png"] forState:UIControlStateNormal];
    [selfBtn addTarget:self action:@selector(segBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [segBg addSubview:selfBtn];
    
    guestBtn = [[UIButton alloc] initWithFrame:CGRectMake(65, 0, 64, 30)];
    [guestBtn.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [guestBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [guestBtn setTitle:@"请客记录" forState:UIControlStateNormal];
    [guestBtn setBackgroundImage:[UIImage imageNamed:@"btn_segricthselected1.png"] forState:UIControlStateNormal];
    [guestBtn addTarget:self action:@selector(segBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [segBg addSubview:guestBtn];
    
    UIImageView * lineImg = [[UIImageView alloc] initWithFrame:CGRectMake(64, 0, 1, 30)];
    lineImg.image = [UIImage imageNamed:@"btn_segline.png"];
    [segBg addSubview:lineImg];
    [lineImg release];
    
    //返回按钮
    UIButton * backBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 58.0, 30.0)];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [backBut setTitle:@"返回" forState:UIControlStateNormal];
    [backBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBut addTarget:self action:@selector(clickOnBackBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBut];
    [backBut release];
}

-(void)clickOnBackBut{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma - mark Seg按钮
-(void)segBtnClick:(UIButton*)btn{
    isSelfSeg = !isSelfSeg;
    if (isSelfSeg) {
        [selfBtn setBackgroundImage:[UIImage imageNamed:@"btn_segleftselected2.png"] forState:UIControlStateNormal];
        [guestBtn setBackgroundImage:[UIImage imageNamed:@"btn_segricthselected1.png"] forState:UIControlStateNormal];
    }else{
        [selfBtn setBackgroundImage:[UIImage imageNamed:@"btn_segleftselected1.png"] forState:UIControlStateNormal];
        [guestBtn setBackgroundImage:[UIImage imageNamed:@"btn_segricthselected2.png"] forState:UIControlStateNormal];
    }
    
    for (int i = 0; i<3; i++) {
        UILabel * lb = [titleArr objectAtIndex:i];
        if (i == 0) {
            lb.text = isSelfSeg ? @"序号" : @"电话";
        }else if (i == 1) {
            lb.text = isSelfSeg ? @"金额" : @"金额";
        }else if (i == 2) {
            lb.text = isSelfSeg ? @"日期" : @"日期";
        }
    }
    
    [consumeTable reloadData];
}

#pragma - mark 会员卡信息
-(void)addCardInfoCtrl{
    UIImageView * bottomView = [[UIImageView alloc] initWithFrame:CGRectMake(5.0, 50.0, 310.0, 80.0)];
    bottomView.image = [UIImage imageNamed:@"bg_customtable5.png"];
    [self.view addSubview:bottomView];
    [bottomView release];
    //卡号
    UILabel * numLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 290, 15)];
    numLb.backgroundColor = [UIColor clearColor];
    numLb.font = [UIFont systemFontOfSize:14.0];
    numLb.text = [NSString stringWithFormat:@"卡号：%@",[cardDict valueForKey:kMemCardNumer]];
    [bottomView addSubview:numLb];
    [numLb release];
    //办卡时间
    UILabel * timeLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 25, 190, 15)];
    timeLb.backgroundColor = [UIColor clearColor];
    timeLb.font = [UIFont systemFontOfSize:14.0];
    timeLb.text = [NSString stringWithFormat:@"办卡时间：%@",[cardDict valueForKey:kMemCardCreateTime]];
    [bottomView addSubview:timeLb];
    [timeLb release];
    //级别
    UILabel * typeLb = [[UILabel alloc] initWithFrame:CGRectMake(180, 25, 120, 15)];
    typeLb.backgroundColor = [UIColor clearColor];
    typeLb.font = [UIFont systemFontOfSize:14.0];
    typeLb.text = [NSString stringWithFormat:@"级别：%@",[cardDict valueForKey:kMemCardType]];
    [bottomView addSubview:typeLb];
    [typeLb release];
    //卡内余额
    UILabel * cashLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 40, 190, 15)];
    cashLb.backgroundColor = [UIColor clearColor];
    cashLb.font = [UIFont systemFontOfSize:14.0];
    cashLb.text = [NSString stringWithFormat:@"卡内余额：%@元",[cardDict valueForKey:kMemCardCash]];
    [bottomView addSubview:cashLb];
    [cashLb release];
    //赠送余额
    UILabel * giftLb = [[UILabel alloc] initWithFrame:CGRectMake(180, 40, 120, 15)];
    giftLb.backgroundColor = [UIColor clearColor];
    giftLb.font = [UIFont systemFontOfSize:14.0];
    giftLb.text = [NSString stringWithFormat:@"赠送余额：%@元",[cardDict valueForKey:kMemCardGiftCash]];
    [bottomView addSubview:giftLb];
    [giftLb release];
    //消费次数
    UILabel * countLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 55, 190, 15)];
    countLb.backgroundColor = [UIColor clearColor];
    countLb.font = [UIFont systemFontOfSize:14.0];
    countLb.text = [NSString stringWithFormat:@"消费次数：%@",[cardDict valueForKey:kMemCardConsumeCount]];
    [bottomView addSubview:countLb];
    [countLb release];
}

-(void)addConsumeTable{
    //加载title
    UIImageView * titleBg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 135, 320, 30)];
    titleBg.image = [UIImage imageNamed:@"acc_xinxikuang.png"];
    [self.view addSubview:titleBg];
    [titleBg release];
    
    if (titleArr) {
        [titleArr removeAllObjects];
        titleArr = nil;
    }
    titleArr = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<3; i++) {
        UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(10+100*i, 0, 100, 30)];
        if (i == 2) {
            lb.frame = CGRectMake(10+80*i, 0, 170, 30);
        }
        lb.backgroundColor = [UIColor clearColor];
        lb.font = [UIFont systemFontOfSize:14.0];
        lb.textColor = [UIColor whiteColor];
        [titleBg addSubview:lb];
        [titleArr addObject:lb];
        if (i == 0) {
            lb.text = isSelfSeg ? @"序号" : @"电话";
        }else if (i == 1) {
            lb.text = isSelfSeg ? @"金额" : @"金额";
        }else if (i == 2) {
            lb.text = isSelfSeg ? @"日期" : @"日期";
        }
    }
    
    //加载常用地址数据列表AddressTable
    consumeTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 165.0, 320.0, 295.0) style:UITableViewStylePlain];
    consumeTable.delegate = self;
    consumeTable.dataSource = self;
    consumeTable.backgroundColor = [UIColor clearColor];
    consumeTable.bounces = NO;
    //consumeTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:consumeTable];
    [consumeTable release];
}

#pragma mark - 获取消费清单
-(void)getConsumeArr{
    if (selfArr) {
        [selfArr removeAllObjects];
        selfArr = nil;
    }
    if (guestArr) {
        [guestArr removeAllObjects];
        guestArr = nil;
    }
    if (cardDict) {
        cardDict = nil;
    }
    
    selfArr = [[NSMutableArray alloc] init];
    guestArr = [[NSMutableArray alloc] init];
    cardDict = [[NSMutableDictionary alloc] init];
    
    /*
    // test data
    for (int i = 0; i < 8; i++) {
        NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithCapacity:10];
        [dict setValue:@"智驾网" forKey:kConsumeAgentName];
        [dict setValue:@"2013-1-19 10:09:34" forKey:kConsumeDaijiaTime];
        [dict setValue:@"001" forKey:kConsumeOrderID];
        [dict setValue:@"280" forKey:kConsumeTotalPrice];
        [dict setValue:@"18688969596" forKey:kConsumeGuestTel];
                
        [selfArr addObject:dict];
        [guestArr addObject:dict];
    }
    [cardDict setValue:@"2345324324354" forKey:kMemCardNumer];
    [cardDict setValue:@"2013-01-19" forKey:kMemCardCreateTime];
    [cardDict setValue:@"3星会员" forKey:kMemCardType];
    [cardDict setValue:@"3000" forKey:kMemCardCash];
    [cardDict setValue:@"6000" forKey:kMemCardGiftCash];
    [cardDict setValue:@"14" forKey:kMemCardConsumeCount];
    */
    
    //会员卡信息
    NSDictionary * dict = [[ClientJSONObject clientJSONObject] getMembercardInfoWithTel:[RunTimeShare getUserTel]];
    for(NSString *aKey in dict) {
        [cardDict setValue:[dict valueForKey:aKey] forKey:aKey];
    }
    //我的消费记录
    [selfArr addObjectsFromArray:[[ClientJSONObject clientJSONObject] getConsumeListWithTel:[RunTimeShare getUserTel]]];
    //请客记录
    [guestArr addObjectsFromArray:[[ClientJSONObject clientJSONObject] getGuestConsumeListWithTel:[RunTimeShare getUserTel]]];
     
    [self dismiss];
    
    [self addCardInfoCtrl];
    [consumeTable reloadData];
}

#pragma mark - UITableView的协议处理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray * tmpArr = isSelfSeg ? selfArr : guestArr;
    return [tmpArr count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray * tmpArr = isSelfSeg ? selfArr : guestArr;
    NSDictionary * dict = [tmpArr objectAtIndex:indexPath.row];
    ConsumeDetailVC * vc = [[ConsumeDetailVC alloc] initWithBasicDict:dict];
    [self.navigationController pushViewController:vc animated:TRUE];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellIdentifier = @"ConsumeCell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
        cell.userInteractionEnabled = YES;
    }
    for (UIView * subView in cell.subviews) {
        if (subView.tag>100) {
            [subView removeFromSuperview];
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    NSArray * tmpArr = isSelfSeg ? selfArr : guestArr;
    NSDictionary * dict = [tmpArr objectAtIndex:[indexPath row]];
    
    for (int i = 0; i<3; i++) {
        UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(10+100*i, 0, 100, 50)];
        if (i == 2) {
            lb.frame = CGRectMake(10+80*i, 0, 170, 50);
        }
        lb.backgroundColor = [UIColor clearColor];
        lb.font = [UIFont systemFontOfSize:14.0];
        lb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        lb.tag = 1000 + i;
        [cell addSubview:lb];
        if (i == 0) {
            lb.text = isSelfSeg ? [dict valueForKey:kConsumeOrderID] : [dict valueForKey:kConsumeGuestTel];
        }else if (i == 1) {
            lb.text = isSelfSeg ? [dict valueForKey:kConsumeTotalPrice] : [dict valueForKey:kConsumeTotalPrice];
            lb.text = [NSString stringWithFormat:@"%@元",lb.text];
        }else if (i == 2) {
            lb.text = isSelfSeg ? [dict valueForKey:kConsumeDaijiaTime] : [dict valueForKey:kConsumeDaijiaTime];
        }
    }
    
    return cell;
}

#pragma mark -
#pragma mark 显示提醒框

- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框

- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}
@end
