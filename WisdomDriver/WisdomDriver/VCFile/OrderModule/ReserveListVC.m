//
//  ReserveListVC.m
//  WisdomDriver
//
//  Created by  apple on 13-1-20.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "ReserveListVC.h"
#import "SVProgressHUD.h"
#import "RunTimeShare.h"
#import "ClientJSONObject.h"
#import "ReserveDetailVC.h"
#import "defines.h"

@interface ReserveListVC ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray * reserveArr;
    UITableView * reserveTable;
}
@end

@implementation ReserveListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    //顶部条栏和文字处理，取消按钮
    [self addTopViewWithTitle:@"预约清单"];
    
    //预约表格
    [self addReserveTable];
    
    [self showWithStatusMsg:@"正在获取预约清单"];
    [self performSelector:@selector(getReserveArr) withObject:nil afterDelay:0.5];
}

#pragma mark - 添加顶部条栏和文字,取消按钮
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //取消按钮
    UIButton *cancelBut = [[UIButton alloc] initWithFrame:CGRectMake(3.0, 8.0, 60.0, 30.0)];
    [cancelBut setTitle:@"返回" forState:UIControlStateNormal];
    [cancelBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [cancelBut addTarget:self action:@selector(clickOnCancelBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelBut];
    [cancelBut release];
}

-(void)addReserveTable{
    //加载常用地址数据列表AddressTable
    reserveTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 47.0, 320.0, 413.0) style:UITableViewStylePlain];
    reserveTable.delegate = self;
    reserveTable.dataSource = self;
    reserveTable.backgroundColor = [UIColor clearColor];
    reserveTable.bounces = NO;
    reserveTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:reserveTable];
    [reserveTable release];
}

-(void)clickOnCancelBut{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 获取预约清单
-(void)getReserveArr{
    if (reserveArr) {
        [reserveArr removeAllObjects];
        reserveArr = nil;
    }
    
    reserveArr = [[NSMutableArray alloc] init];
    // test data
    for (int i = 0; i < 0; i++) {
        
        NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithCapacity:10];
        [dict setValue:@"123212432432" forKey:kReserveNumber];
        [dict setValue:@"2013-1-19 10:09:34" forKey:kReserveDriveTime];
        [reserveArr addObject:dict];
    }

    [reserveArr addObjectsFromArray:[[ClientJSONObject clientJSONObject]
                                     getOrderListWithTel:[RunTimeShare getUserTel]]];
    [self dismiss];
    
    [reserveTable reloadData];
}

#pragma mark - UITableView的协议处理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [reserveArr count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary * dict = [reserveArr objectAtIndex:indexPath.row];
    ReserveDetailVC * vc = [[ReserveDetailVC alloc] initWithBasicDict:dict];
    [self.navigationController pushViewController:vc animated:TRUE];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellIdentifier = @"reserveCell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
        cell.userInteractionEnabled = YES;
    }
    for (UIView * subView in cell.subviews) {
        if (subView.tag>100) {
            [subView removeFromSuperview];
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    NSDictionary * reserveDict = [reserveArr objectAtIndex:[indexPath row]];
    
    //订单号
    UILabel * numLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 1.0, 268.0, 23.0)];
    numLbl.text = [NSString stringWithFormat:@"订单号:%@",[reserveDict valueForKey:kReserveNumber]];
    numLbl.backgroundColor = [UIColor clearColor];
    numLbl.font = [UIFont systemFontOfSize:16.0];
    numLbl.textColor = [UIColor colorWithRed:86.0/255.0 green:86.0/255.0 blue:86.0/255.0 alpha:1.0];
    numLbl.tag = 200;
    [cell addSubview:numLbl];
    [numLbl release];
    
    //订单时间
    UILabel * timeLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 25.0, 268.0, 23.0)];
    timeLbl.text = [NSString stringWithFormat:@"预约时间:%@",[reserveDict valueForKey:kReserveDriveTime]];
    timeLbl.backgroundColor = [UIColor clearColor];
    timeLbl.font = [UIFont systemFontOfSize:14.0];
    timeLbl.textColor = [UIColor colorWithRed:86.0/255.0 green:86.0/255.0 blue:86.0/255.0 alpha:1.0];
    timeLbl.tag = 200;
    [cell addSubview:timeLbl];
    [timeLbl release];
    
    UIImageView * line = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 49.0, 320.0, 1.0)];
    line.image = [UIImage imageNamed:@"shezhi_lineacc.png"];
    line.tag = 200;
    [cell addSubview:line];
    [line release];
    
    return cell;
}

#pragma mark -
#pragma mark 显示提醒框

- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框

- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}
@end
