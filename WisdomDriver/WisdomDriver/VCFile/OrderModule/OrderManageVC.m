//
//  OrderManageVC.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-8.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "OrderManageVC.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "ClientJSONObject.h"
#import "RunTimeShare.h"
#import "SVProgressHUD.h"
#import "CustomBadge.h"
#import "defines.h"
#import "NSData+Base64.h"

@interface OrderManageVC ()<ABPeoplePickerNavigationControllerDelegate>{
    UIView * bottomView;
    CustomBadge * customBadge;
    UIImageView * codeView;
}

@end

@implementation OrderManageVC
@synthesize vcDelegate;
- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 460.0)];
    //[bottomView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [bottomView addSubview:bgImgView];
    [self.view addSubview:bottomView];
    
    [self addTopViewWithTitle:@"订单管理"];
    [self addContentCtrl];
    NSLog(@"OrderManagerVC viewDidLoad");
}

-(void)loginSuccess{
    
}

-(void)viewDidAppear:(BOOL)animated{
    NSLog(@"OrderManagerVC viewDidAppear");
    [super viewDidAppear:animated];
}

#pragma mark - 添加顶部条栏和文字
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //发送好友
    UIButton * settingBut = [[UIButton alloc] initWithFrame:CGRectMake(248.0, 8.0, 64.0, 30.0)];
    [settingBut setBackgroundImage:[UIImage imageNamed:@"navBarRightButBottom.png"] forState:UIControlStateNormal];
    [settingBut setTitle:@"发送好友" forState:UIControlStateNormal];
    [settingBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [settingBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [settingBut addTarget:self action:@selector(clickOnSendBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:settingBut];
    [settingBut release];
}

#pragma mark - 获取二维码
-(UIImage*)getQRCodeImage{
    UIImage * img = [RunTimeShare getQRCodeImage];
    if (img == nil) {
        NSDictionary * dict = [[ClientJSONObject clientJSONObject]
                               getDimensionalCodeWithTel:[RunTimeShare getUserTel]];
        if ([[dict valueForKey:kQRCodeDimensionalCode] isEqualToString:@"-1"]) {
            [self dismissErrorMsg:@"该用户未绑定会员卡无法生成二维码"];
        }else{
            NSString * info = [dict valueForKey:kQRCodeDimensionalCode];
            UIImage* qrcodeImage = [RunTimeShare generateQRImageWithStr:info size:250];
            [RunTimeShare setQRCodeImage:qrcodeImage];
            img = qrcodeImage;
        }
    }
    return img;
}

-(void)addQRImage{
    
    codeView.image = [self getQRCodeImage];
}

#pragma - mark 内容控件
-(void)addContentCtrl{
    //二维码图像
    codeView = [[UIImageView alloc] initWithFrame:CGRectMake(50, 50, 220, 220)];
    
    [self.view addSubview:codeView];
    [codeView release];
    
    //提示信息
    UILabel * txtLb = [[UILabel alloc] initWithFrame:CGRectMake(42, 270, 220, 15)];
    txtLb.font = [UIFont systemFontOfSize:14.0];
    txtLb.textColor = [UIColor grayColor];
    txtLb.backgroundColor = [UIColor clearColor];
    txtLb.text = @"扫一扫上面的二维码图案";
    [self.view addSubview:txtLb];
    [txtLb release];
    
    //按钮
    NSArray * textArr = [[NSArray alloc] initWithObjects:@"预约清单",@"消费记录", nil];
    for (int i=0; i<[textArr count]; i++) {
        UIImageView * bView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 295.0+55.0*i, 300.0, 45.0)];
        bView.image = [UIImage imageNamed:@"bg_customtable1.png"];
        bView.userInteractionEnabled = YES;
        [self.view addSubview:bView];
        [bView release];
        
        UIImageView * arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 15.0, 10.0, 14.0)];
        arrowView.image = [UIImage imageNamed:@"acc_jiantou.png"];
        [bView addSubview:arrowView];
        [arrowView release];
        
        UILabel * txtLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, 200.0, 45.0)];
        txtLbl.backgroundColor = [UIColor clearColor];
        txtLbl.text = [textArr objectAtIndex:i];
        [bView addSubview:txtLbl];
        [txtLbl release];
        
        UIButton * but = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 300.0, 45.0)];
        but.backgroundColor = [UIColor clearColor];
        but.tag = 200+i;
        [but addTarget:self action:@selector(clickOnOrderBut:) forControlEvents:UIControlEventTouchUpInside];
        [bView addSubview:but];
        [but release];
    }
}

-(void)addBadge:(NSInteger)count{
    if (customBadge) {
        [customBadge removeFromSuperview];
        customBadge = nil;
    }
    customBadge = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%d",count]
                                     withStringColor:[UIColor whiteColor]
                                      withInsetColor:[UIColor redColor]
                                      withBadgeFrame:YES
                                 withBadgeFrameColor:[UIColor whiteColor]
                                           withScale:1.0
                                         withShining:YES];
    customBadge.frame = CGRectMake(85, 285, customBadge.frame.size.width, customBadge.frame.size.height);
    [self.view addSubview:customBadge];
}

#pragma - mark 发送好友
-(void)clickOnSendBut{
    NSDictionary * dict = [[ClientJSONObject clientJSONObject]
                           getDimensionalCodeWithTel:[RunTimeShare getUserTel]];
    if ([[dict valueForKey:kQRCodeDimensionalCode] isEqualToString:@"-1"]) {
        [self dismissErrorMsg:@"非会员用户，无法使用此功能。"];
        return;
    }
    
    if (vcDelegate && [vcDelegate respondsToSelector:@selector(enterChoseGuestVC)]) {
        [vcDelegate enterChoseGuestVC];
    }
}

/*

#pragma mark - 通讯录协议
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    // assigning control back to the main controller
    [peoplePicker dismissModalViewControllerAnimated:YES];
}

- (BOOL)peoplePickerNavigationController: (ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    
    [peoplePicker dismissModalViewControllerAnimated:YES];
    
    if (property == kABPersonPhoneProperty) {
        
        ABMutableMultiValueRef phoneMulti = ABRecordCopyValue(person, property);
        int index = ABMultiValueGetIndexForIdentifier(phoneMulti,identifier);
        NSString *phone = (NSString*)ABMultiValueCopyValueAtIndex(phoneMulti, index);
        //do something
        [self showWithStatusMsg:@"正在生成请客二维码"];
        [self performSelectorOnMainThread:@selector(sendQRImgToGuest:) withObject:phone waitUntilDone:FALSE];
        [phone release];
    }
    return NO;
}

-(void)sendQRImgToGuest:(NSString*)guestPhone{
    NSString * tel = [RunTimeShare getUserTel];
    NSDictionary * dict = [[ClientJSONObject clientJSONObject] getGuestDCodeWithTel:tel guestTel:guestPhone];
    if ([[dict valueForKey:@"dimensionalCode"] integerValue] != -2) {
        
        [self dismissSuccessMsg:@"二维码生成成功"];
        
        //generate QRCode image and send
        UIImage * qrImg = [RunTimeShare generateQRImageWithStr:[dict valueForKey:@"dimensionalCode"] size:250];
        
        //Convert the image into data
        NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(qrImg)];
        //Create a base64 string representation of the data using NSData+Base64
        NSString *base64String = [imageData base64EncodedString];
        
        //[self.vcDelegate enterSendMailVC:base64String];
        [self performSelectorOnMainThread:@selector(sendMail:) withObject:base64String waitUntilDone:TRUE];
    }else{
        [self dismissErrorMsg:@"该用户未绑定会员卡无法请客"];
    }
}

-(void)sendMail:(NSString *)str{
    [self.vcDelegate enterSendMailVC:str];
}
*/

#pragma mark MFMailComposeViewControllerDelegate
-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [controller dismissModalViewControllerAnimated:YES];
}

#pragma - mark 消费按钮
-(void)clickOnOrderBut:(id)sender{
    UIButton * btn = (UIButton*)sender;
    if ((btn.tag-200) == 0) {
        //预约清单
        [customBadge removeFromSuperview];
        customBadge = nil;
        if (vcDelegate && [vcDelegate respondsToSelector:@selector(enterReserveListVC)]) {
            [vcDelegate enterReserveListVC];
        }
    }else{
        //消费记录
        if (vcDelegate && [vcDelegate respondsToSelector:@selector(enterConsumeListVC)]) {
            [vcDelegate enterConsumeListVC];
        }
    }
}

#pragma mark -
#pragma mark 显示提醒框

- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框

- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}
@end
