//
//  ReserveDetailVC.m
//  WisdomDriver
//
//  Created by  apple on 13-1-20.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "ReserveDetailVC.h"
#import "RunTimeShare.h"
#import "ClientJSONObject.h"
#import "SVProgressHUD.h"
#import "defines.h"

@interface ReserveDetailVC (){
    NSMutableDictionary * reserveDict;
}

@end

@implementation ReserveDetailVC
-(id)initWithBasicDict:(NSDictionary*)dict{
    self = [super init];
    reserveDict = [[NSMutableDictionary alloc] initWithDictionary:dict];
    return  self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    [self addTopViewWithTitle:@"详情"];
    [self showWithStatusMsg:@"正在获取订单详情"];
    [self performSelector:@selector(getReserveInfo) withObject:nil afterDelay:0.5];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 添加顶部条栏和文字,返回按钮
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //返回按钮
    UIButton * backBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 58.0, 30.0)];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [backBut setTitle:@"返回" forState:UIControlStateNormal];
    [backBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBut addTarget:self action:@selector(clickOnBackBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBut];
    [backBut release];
    
    //编辑(提交)按钮，默认是编辑状态
    UIButton * settingBut = [[UIButton alloc] initWithFrame:CGRectMake(256.0, 8.0, 58.0, 30.0)];
    [settingBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [settingBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [settingBut setTitle:@"客服" forState:UIControlStateNormal];
    [settingBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [settingBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [settingBut addTarget:self action:@selector(clickOnPhoneBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:settingBut];
    [settingBut release];
}

-(void)clickOnBackBut{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 客服按钮
-(void)clickOnPhoneBut{
    NSString *phone = [NSString stringWithFormat:@"tel://%@",kPhoneCustomService];
    UIWebView*callWebview =[[UIWebView alloc] init];
    NSURL *telURL =[NSURL URLWithString:phone];
    [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
    [self.view addSubview:callWebview];
    [callWebview release];
}

#pragma mark - 添加内容控件
-(void)addContentCtrl{
    //订单号
    UILabel * numLb = [[UILabel alloc] initWithFrame:CGRectMake(20, 52, 300, 15)];
    numLb.backgroundColor = [UIColor clearColor];
    numLb.text = [NSString stringWithFormat:@"单号:%@",[reserveDict valueForKey:kReserveNumber]];
    numLb.font = [UIFont systemFontOfSize:17.0];
    numLb.textAlignment = UITextAlignmentLeft;
    [self.view addSubview:numLb];
    [numLb release];
    
    //开始、结束地址、代价时间、司机数
    UIImageView * bottomView1 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 75.0, 300.0, 180.0)];
    bottomView1.image = [UIImage imageNamed:@"bg_customtable4.png"];
    bottomView1.userInteractionEnabled = YES;
    [self.view addSubview:bottomView1];
    [bottomView1 release];
    
    NSArray * strArr = [[NSArray alloc] initWithObjects:@"开始地址",@"结束地址",@"代价时间",@"所需司机", nil];
    for (int i=0; i<[strArr count]; i++) {
        UILabel * txtLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 45.0*i, 110.0, 45.0)];
        txtLbl.backgroundColor = [UIColor clearColor];
        txtLbl.textAlignment = NSTextAlignmentLeft;
        txtLbl.text = [strArr objectAtIndex:i];
        [bottomView1 addSubview:txtLbl];
        [txtLbl release];
        
        UILabel * cntLb = [[UILabel alloc] initWithFrame:CGRectMake(100.0, 45.0*i, 200.0, 45.0)];
        cntLb.backgroundColor = [UIColor clearColor];
        cntLb.textAlignment = NSTextAlignmentLeft;
        cntLb.textColor = [UIColor grayColor];
        [bottomView1 addSubview:cntLb];
        [cntLb release];
        
        if (i == 0) {
            cntLb.text = [reserveDict valueForKey:kReserveStartAddr];
        }else if (i == 1){
            cntLb.text = [reserveDict valueForKey:kReserveEndAddr];
        }else if (i == 2){
            cntLb.text = [reserveDict valueForKey:kReserveDriveTime];
        }else if (i == 3){
            cntLb.text = [reserveDict valueForKey:kReserveDriverCount];
        }
    }
    
    //司机姓名、电话、收费标准
    UIImageView * bottomView2 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 265.0, 300.0, 135.0)];
    bottomView2.image = [UIImage imageNamed:@"bg_customtable3.png"];
    bottomView2.userInteractionEnabled = YES;
    [self.view addSubview:bottomView2];
    [bottomView2 release];
    
    NSArray * titleArr = [[NSArray alloc] initWithObjects:@"司机姓名",@"司机电话",@"收费标准", nil];
    for (int i=0; i<[titleArr count]; i++) {
        UILabel * txtLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 45.0*i, 110.0, 45.0)];
        txtLbl.backgroundColor = [UIColor clearColor];
        txtLbl.textAlignment = NSTextAlignmentLeft;
        txtLbl.text = [titleArr objectAtIndex:i];
        [bottomView2 addSubview:txtLbl];
        [txtLbl release];
        
        UILabel * cntLb = [[UILabel alloc] initWithFrame:CGRectMake(100.0, 45.0*i, 200.0, 45.0)];
        cntLb.backgroundColor = [UIColor clearColor];
        cntLb.textColor = [UIColor grayColor];
        cntLb.textAlignment = NSTextAlignmentLeft;
        [bottomView2 addSubview:cntLb];
        [cntLb release];
        
        if (i == 0) {
            cntLb.text = [reserveDict valueForKey:kReserveDriverName];
        }else if (i == 1){
            cntLb.text = [reserveDict valueForKey:kReserveDriverTel];
        }else if (i == 2){
            cntLb.text = [reserveDict valueForKey:kReserveRateStandard];
        }
    }
    
    //取消订单
    UIImageView * bottomView3 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 410.0, 300.0, 45.0)];
    bottomView3.image = [UIImage imageNamed:@"btn_xiadandefault1.png"];
    bottomView3.userInteractionEnabled = YES;
    [self.view addSubview:bottomView3];
    [bottomView3 release];
    
    UIButton * button = [[UIButton alloc] init];
    button.frame = bottomView3.frame;
    button.backgroundColor = [UIColor clearColor];
    [button setImage:[UIImage imageNamed:@"btn_xiadandefault1.png"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"btn_xiadanselected2.png"] forState:UIControlStateSelected];
    [button addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    [button release];
    
    UILabel * btnLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 45)];
    btnLb.backgroundColor = [UIColor clearColor];
    btnLb.textColor = [UIColor whiteColor];
    btnLb.text = @"取消订单";
    btnLb.font = [UIFont systemFontOfSize:18.0];
    btnLb.textAlignment = UITextAlignmentCenter;
    [button addSubview:btnLb];
    [btnLb release];
}

#pragma - mark 取消订单按钮
-(void)cancelBtnClick{
    [self clickOnPhoneBut];
}

#pragma - mark 获取订单详情
-(void)getReserveInfo{
    NSDictionary * dict = [[ClientJSONObject clientJSONObject]
                           getOrderDetail:[reserveDict valueForKey:kReserveNumber]];
    for(NSString *aKey in dict) {
        [reserveDict setValue:[[dict valueForKey:aKey] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:aKey];
    }
    /*
    //test data
    [reserveDict setValue:@"深圳市龙岗区布吉丽湖花园" forKey:kReserveStartAddr];
    [reserveDict setValue:@"深圳市宝安西乡客运站" forKey:kReserveEndAddr];
    [reserveDict setValue:@"2" forKey:kReserveDriverCount];
    [reserveDict setValue:@"王全安" forKey:kReserveDriverName];
    [reserveDict setValue:@"18765432764" forKey:kReserveDriverTel];
    [reserveDict setValue:@"xxxxxxxxxx" forKey:kReserveRateStandard];
    */
    
    [self dismiss];
    
    [self addContentCtrl];
}

#pragma mark -
#pragma mark 显示提醒框

- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框

- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}
@end
