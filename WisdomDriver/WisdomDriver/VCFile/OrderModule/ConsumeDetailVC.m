//
//  ConsumeDetailVC.m
//  WisdomDriver
//
//  Created by  apple on 13-1-20.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "ConsumeDetailVC.h"
#import "RunTimeShare.h"
#import "ClientJSONObject.h"
#import "SVProgressHUD.h"
#import "OrderJudgeVC.h"
#import "defines.h"

@interface ConsumeDetailVC ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableDictionary * consumeDict;
    UITableView * contentTable;
}

@end

@implementation ConsumeDetailVC
-(id)initWithBasicDict:(NSDictionary*)dict{
    self = [super init];
    consumeDict = [[NSMutableDictionary alloc] initWithDictionary:dict];
    return  self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    [self addTopViewWithTitle:@"消费详情"];
    [self showWithStatusMsg:@"正在获取消费详情"];
    [self performSelector:@selector(getConsumeInfo) withObject:nil afterDelay:0.5];
}

#pragma mark - 添加顶部条栏和文字,返回按钮
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //返回按钮
    UIButton * backBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 58.0, 30.0)];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [backBut setTitle:@"返回" forState:UIControlStateNormal];
    [backBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBut addTarget:self action:@selector(clickOnBackBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBut];
    [backBut release];
    
    //编辑(提交)按钮，默认是编辑状态
    UIButton * settingBut = [[UIButton alloc] initWithFrame:CGRectMake(256.0, 8.0, 58.0, 30.0)];
    [settingBut setBackgroundImage:[UIImage imageNamed:@"navBarRightButBottom.png"] forState:UIControlStateNormal];
    [settingBut setTitle:@"客服" forState:UIControlStateNormal];
    [settingBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [settingBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [settingBut addTarget:self action:@selector(clickOnPhoneBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:settingBut];
    [settingBut release];
}

-(void)clickOnBackBut{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 客服按钮
-(void)clickOnPhoneBut{
    NSString *phone = [NSString stringWithFormat:@"tel://%@",kPhoneCustomService];
    UIWebView*callWebview =[[UIWebView alloc] init];
    NSURL *telURL =[NSURL URLWithString:phone];
    [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
    [self.view addSubview:callWebview];
    [callWebview release];
}

#pragma - mark 获取消费详情
-(void)getConsumeInfo{
    
    NSDictionary * dict = [[ClientJSONObject clientJSONObject]
                           getConsumeDetailWithOrderID:[consumeDict valueForKey:kReserveNumber]];
    for(NSString *aKey in dict) {
        [consumeDict setValue:[dict valueForKey:aKey] forKey:aKey];
    }
    /*
    //test data
    [consumeDict setValue:@"深圳市龙岗区布吉丽湖花园" forKey:kConsumeStartAddr];
    [consumeDict setValue:@"深圳市宝安西乡客运站" forKey:kConsumeEndAddr];
    [consumeDict setValue:@"2" forKey:kConsumeDriverNum];
    [consumeDict setValue:@"王全安" forKey:kConsumeDriverName];
    [consumeDict setValue:@"2012-12-25" forKey:kConsumeDaijiaTime];
    [consumeDict setValue:@"2012-12-25 22:15:00" forKey:kConsumeEndTime];
    [consumeDict setValue:@"智驾通" forKey:kConsumeAgentName];
    [consumeDict setValue:@"180" forKey:kConsumeTotalPrice];
    [consumeDict setValue:@"1wer3233243234" forKey:kConsumeOrderID];
    */
    [self dismiss];
    
    [self addContentCtrl];
}

#pragma - mark 添加内容控件
-(void)addContentCtrl{
    UIScrollView * scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 47, 320, 413)];
    //[scroll setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [scroll addSubview:bgImgView];
    
    scroll.userInteractionEnabled = TRUE;
    [self.view addSubview:scroll];
    
    //订单号
    UILabel * numLb = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 300, 15)];
    numLb.backgroundColor = [UIColor clearColor];
    numLb.text = [NSString stringWithFormat:@"单号：%@",[consumeDict valueForKey:kConsumeOrderID]];
    numLb.font = [UIFont systemFontOfSize:17.0];
    numLb.textAlignment = UITextAlignmentLeft;
    [scroll addSubview:numLb];
    [numLb release];
    
    //加载常用地址数据列表AddressTable
    contentTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 22.0, 320.0, 375.0)
                                                style:UITableViewStyleGrouped];
    contentTable.delegate = self;
    contentTable.dataSource = self;
    contentTable.backgroundColor = [UIColor clearColor];
    contentTable.bounces = NO;
    contentTable.scrollEnabled = NO;
    //consumeTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [scroll addSubview:contentTable];
    [contentTable release];
    
    //评价
    UIImageView * bottomView3 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 415.0, 300.0, 45.0)];
    bottomView3.image = [UIImage imageNamed:@"btn_xiadandefault1.png"];
    bottomView3.userInteractionEnabled = YES;
    [scroll addSubview:bottomView3];
    [bottomView3 release];
    
    UIButton * button = [[UIButton alloc] init];
    button.frame = bottomView3.frame;
    button.backgroundColor = [UIColor clearColor];
    [button setImage:[UIImage imageNamed:@"btn_xiadandefault1.png"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"btn_xiadanselected2.png"] forState:UIControlStateSelected];
    [button addTarget:self action:@selector(judgeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [scroll addSubview:button];
    [button release];
    
    UILabel * btnLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 45)];
    btnLb.backgroundColor = [UIColor clearColor];
    btnLb.textColor = [UIColor whiteColor];
    btnLb.text = @"评 价";
    btnLb.font = [UIFont systemFontOfSize:18.0];
    btnLb.textAlignment = UITextAlignmentCenter;
    [button addSubview:btnLb];
    [btnLb release];
    
    scroll.contentSize = CGSizeMake(320, 500);
}

-(void)judgeBtnClick{
    NSDictionary * dict = [[ClientJSONObject clientJSONObject]
                           isNotEvaluateWithOrderID:[consumeDict valueForKey:kConsumeOrderID]];
    if ([[dict valueForKey:@"result"] isEqualToString:@"9"]) {
        [self dismissErrorMsg:@"该订单已评价"];
        return;
    }
    OrderJudgeVC * vc = [[OrderJudgeVC alloc] initWithBasicDict:consumeDict];
    [self.navigationController pushViewController:vc animated:TRUE];
}

#pragma mark - UITableView的协议处理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 8;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellIdentifier = @"ConsumeCell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
        cell.userInteractionEnabled = YES;
    }
    for (UIView * subView in cell.subviews) {
        if (subView.tag>100) {
            [subView removeFromSuperview];
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UILabel * titleLb = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 120, 45)];
    titleLb.backgroundColor = [UIColor clearColor];
    titleLb.font = [UIFont systemFontOfSize:17.0];
    titleLb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    titleLb.tag = 1000;
    [cell addSubview:titleLb];
    [titleLb release];
    
    UILabel * cntLb = [[UILabel alloc] initWithFrame:CGRectMake(130, 0, 170, 45)];
    cntLb.backgroundColor = [UIColor clearColor];
    cntLb.font = [UIFont systemFontOfSize:17.0];
    cntLb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    cntLb.tag = 1000;
    [cell addSubview:cntLb];
    [cntLb release];
    
    if (indexPath.row == 0) {
        titleLb.text = @"开始地址";
        cntLb.text = [consumeDict valueForKey:kConsumeStartAddr];
    }else if (indexPath.row == 1) {
        titleLb.text = @"结束地址";
        cntLb.text = [consumeDict valueForKey:kConsumeEndAddr];
    }else if (indexPath.row == 2) {
        titleLb.text = @"代驾时间";
        cntLb.text = [consumeDict valueForKey:kConsumeDaijiaTime];
    }else if (indexPath.row == 3) {
        titleLb.text = @"代驾结束时间";
        cntLb.text = [consumeDict valueForKey:kConsumeEndTime];
    }else if (indexPath.row == 4) {
        titleLb.text = @"所需司机";
        cntLb.text = [consumeDict valueForKey:kConsumeDriverNum];
    }else if (indexPath.row == 5) {
        titleLb.text = @"司机姓名";
        cntLb.text = [consumeDict valueForKey:kConsumeDriverName];
    }else if (indexPath.row == 6) {
        titleLb.text = @"代理商名称";
        cntLb.text = [consumeDict valueForKey:kConsumeAgentName];
    }else if (indexPath.row == 7) {
        titleLb.text = @"消费金额";
        cntLb.text = [consumeDict valueForKey:kConsumeTotalPrice];
    }
    
    return cell;
}

#pragma mark -
#pragma mark 显示提醒框

- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框

- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}
@end
