//
//  OrderManageVC.h
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-8.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@protocol OrderManageVCDelegate <NSObject>

-(void)enterReserveListVC;
-(void)enterConsumeListVC;
-(void)enterChoseGuestVC;
-(void)enterSendMailVC:(NSString *)base64Img;

@end

@interface OrderManageVC : UIViewController <MFMailComposeViewControllerDelegate>
{
    id<OrderManageVCDelegate> vcDelegate;
}
@property (nonatomic, assign) id<OrderManageVCDelegate> vcDelegate;
-(void)addBadge:(NSInteger)count;
-(void)addQRImage;
-(void)loginSuccess;
@end
