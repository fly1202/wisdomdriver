//
//  MainTabVC.h
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-7.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginMainVC.h"
#import "ForgetPasswordVC.h"
#import "RegisterVC.h"
#import "MapViewController.h"
#import "WantDriverVC.h"
#import "OrderManageVC.h"
#import "StandardVC.h"
#import "SettingVC.h"
#import "PersonSettingVC.h"
#import "SystemSettingVC.h"
#import "SystemMessageVC.h"
#import "ShareViewController.h"
#import "ChoseLocationVC.h"
#import "UsedAddressVC.h"
#import "ReserveListVC.h"
#import "ConsumeListVC.h"
#import "ClientJSONObject.h"
#import "RunTimeShare.h"

@interface MainTabVC : UIViewController<LoginMainVCDelegate,RegisterVCDelegate,MapViewControllerDelegate,StandardVCDelegate,SettingVCDelegate,WantDriverVCDelegate,ShareViewControllerDelegate,OrderManageVCDelegate>

@end
