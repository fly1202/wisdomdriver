//
//  SystemSettingVC.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-8.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#define versonAlertTag  222
#define agentsAlertTag  333

#import "SystemSettingVC.h"
#import "AboutUsVC.h"
#import "ClientJSONObject.h"
#import "RunTimeShare.h"
#import "defines.h"
#import "SVProgressHUD.h"
#import "AgentListVC.h"
#import "SBJson.h"

@interface SystemSettingVC () <AgentListVCDelegate>{
    BOOL  isNewVersion;
    BOOL  isNewAgents;
    
    NSString * newVerUrl;
    NSString * agentInfo;
}

@end

@implementation SystemSettingVC

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        isNewVersion = NO;
        isNewAgents = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //self.view.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0];
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    [self addSystemSettingUI];

    [self addTopViewWithTitle:@"系统设置"];
    
}

#pragma mark - 添加顶部条栏和文字,返回按钮
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //返回按钮
    UIButton * backBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 58.0, 30.0)];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [backBut setTitle:@"返回" forState:UIControlStateNormal];
    [backBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBut addTarget:self action:@selector(clickOnBackBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBut];
    [backBut release];
    
}

-(void)clickOnBackBut{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 加载系统设置的按钮UI布局
-(void)addSystemSettingUI{
    NSArray * strArr = [[NSArray alloc] initWithObjects:@"关于我们",@"版本检测",@"代理商", nil];
    for (int i=0; i<[strArr count]; i++) {
        UIImageView * bottomView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 55.0+55.0*i, 300.0, 45.0)];
        bottomView.image = [UIImage imageNamed:@"bg_customtable1.png"];
        bottomView.userInteractionEnabled = YES;
        [self.view addSubview:bottomView];
        [bottomView release];
        
        UIImageView * arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 15.0, 10.0, 14.0)];
        arrowView.image = [UIImage imageNamed:@"acc_jiantou.png"];
        [bottomView addSubview:arrowView];
        [arrowView release];
        
        UILabel * txtLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, 200.0, 45.0)];
        txtLbl.backgroundColor = [UIColor clearColor];
        txtLbl.text = [strArr objectAtIndex:i];
        [bottomView addSubview:txtLbl];
        [txtLbl release];
        
        UIButton * but = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 300.0, 45.0)];
        but.backgroundColor = [UIColor clearColor];
        but.tag = 200+i;
        [but addTarget:self action:@selector(clickOnSettingBut:) forControlEvents:UIControlEventTouchUpInside];
        [bottomView addSubview:but];
        [but release];
    }
}

#pragma mark - 点击按钮进入不同的界面
-(void)clickOnSettingBut:(id)sender{
    int curTag = ((UIButton *)sender).tag - 200;
    switch (curTag) {
        case 0:
        {
            //关于我们的界面
            AboutUsVC * aboutVC = [[AboutUsVC alloc] init];
            [self.navigationController pushViewController:aboutVC animated:YES];
        }
            break;
        case 1:
        {
            //版本检测
            [self VersionDetection];
        }
            break;
        case 2:
        {
            //代理商设置
            [self AgentsDetection];
        }
            break;
        default:
            break;
    }
}

#pragma mark - 进行新版本检测
-(void)VersionDetection{
    //曹工，进行新版本检测
    NSString * ver = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    /*
    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
    CFShow(infoDic);
    
    NSString *appVersion = [infoDic objectForKey:@"CFBundleVersion"];
    */
    [self onCheckVersion:ver];
}

#pragma mark - 版本监测
-(void)onCheckVersion:(NSString *)currentVersion
{
    NSString *URL = APPURL;
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:URL]];
    [request setHTTPMethod:@"POST"];
    NSHTTPURLResponse *urlResponse = nil;
    NSError *error = nil;
    NSData *recervedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
    
    NSString *results = [[NSString alloc] initWithBytes:[recervedData bytes] length:[recervedData length] encoding:NSUTF8StringEncoding];
    SBJsonParser *jsonParser = [[SBJsonParser alloc] init];
    NSDictionary *dic = [[[jsonParser objectWithString:results] copy] autorelease];
    
    NSArray *infoArray = [dic objectForKey:@"results"];
    if ([infoArray count]) {
        NSDictionary *releaseInfo = [infoArray objectAtIndex:0];
        NSString *lastVersion = [releaseInfo objectForKey:@"version"];
        
        if (![lastVersion isEqualToString:currentVersion]) {
            newVerUrl = [releaseInfo objectForKey:@"trackVireUrl"];
            UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"更新" message:@"有新的版本更新，是否前往更新？" delegate:self cancelButtonTitle:@"更新" otherButtonTitles:@"关闭", nil] autorelease];
            alert.tag = versonAlertTag;
            [alert show];
        }else{
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"没有检测到新版本,敬请期待." delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
    }
}

#pragma mark - 进行新代理商检测
-(void)AgentsDetection{
    //曹工，进行新代理商资料检测
    AgentListVC * vc = [[AgentListVC alloc] init];
    vc.agentListVCDelegate = self;
    [self.navigationController pushViewController:vc animated:TRUE];
    
    /*
    NSString * agentID = [RunTimeShare getAgentID];
    NSDictionary * agentDict = [[ClientJSONObject clientJSONObject] getAgentBasicInfoWithID:agentID];
    //{“sysInformation”:”会员最新活动，冲100送100。”}
    agentInfo = [agentDict valueForKey:@"sysInformation"];
    if (agentInfo && agentInfo.length > 0) {
        isNewAgents = YES;
    }
    
    //目前假设有新资料
    isNewAgents = YES;
    if (isNewAgents) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"更新提示" message:@"检测到有新的代理商资料,请前去下载." delegate:self cancelButtonTitle:@"下载" otherButtonTitles:@"取消", nil];
        alert.tag = agentsAlertTag;
        [alert show];
        [alert release];
    }
    else {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"没有检测到新的代理商资料,敬请期待." delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
     */
}

#pragma mark - 代理商页面代理
-(void)didSelectAgent:(NSDictionary*)agent{
    [self.navigationController popToViewController:self animated:TRUE];
}

#pragma mark - 有关提醒框的协议处理
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if ((buttonIndex==0)&&(alertView.tag == versonAlertTag)) {
        //处理去下载新版本的事情
        NSString *iTunesLink = APP_UPDATE_URL;
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
    }
    
    if ((buttonIndex==0)&&(alertView.tag == agentsAlertTag)) {
        //处理去下载新代理商资料的事情
    }
}

#pragma mark -
#pragma mark 显示提醒框

- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框

- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}
@end
