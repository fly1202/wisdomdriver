//
//  ShareViewController.h
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-8.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ShareViewControllerDelegate <NSObject>

-(void)enterShortMsgVC;

@end

@interface ShareViewController : UIViewController
{
    id<ShareViewControllerDelegate> shareDelegate;
}
@property (nonatomic,assign) id<ShareViewControllerDelegate> shareDelegate;
@end
