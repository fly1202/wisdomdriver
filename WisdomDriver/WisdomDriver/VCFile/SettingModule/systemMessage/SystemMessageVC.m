//
//  SystemMessageVC.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-8.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "SystemMessageVC.h"
#import "messageDetailVC.h"
#import "ClientJSONObject.h"
#import "RunTimeShare.h"
#import "defines.h"
#import <QuartzCore/QuartzCore.h>

@interface SystemMessageVC (){
    NSMutableArray  * messageArr;
}

-(void)getMessageFromNet;

@end

@implementation SystemMessageVC

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        [self getMessageFromNet];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //self.view.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0];
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    [self addTopViewWithTitle:@"系统消息"];
    
    [self addMessageUITableView];
}

#pragma mark - 添加顶部条栏和文字,返回按钮
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //返回按钮
    UIButton * backBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 58.0, 30.0)];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [backBut setTitle:@"返回" forState:UIControlStateNormal];
    [backBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBut addTarget:self action:@selector(clickOnBackBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBut];
    [backBut release];
    
}

-(void)clickOnBackBut{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 从服务器获取最新的系统信息
-(void)getMessageFromNet{
   //曹工，目前我是使用假数据
    if (messageArr) {
        [messageArr removeAllObjects];
        messageArr = nil;
    }
    messageArr = [[NSMutableArray alloc] init];
    //先获取回来，根据接口实际情况再调整
    NSArray * dict = [[ClientJSONObject clientJSONObject] getSystemInfoWithAgentID:[RunTimeShare getAgentID]];
    if (dict) {
        [messageArr addObjectsFromArray:dict];
    }else{
        //数组里面保存字典，每个字典包涵一张图片，一个标题，一个时间，还有详细文章
        //目前假设有40条信息
        for (int i=0; i<40; i++) {
            NSMutableDictionary * infoDict = [[NSMutableDictionary alloc] init];
            UIImage * picImage = [UIImage imageNamed:@"icon.png"];
            [infoDict setObject:picImage forKey:@"picImage"];
            [infoDict setObject:[NSString stringWithFormat:@"第%i条标题",i] forKey:@"messageTitle"];
            [infoDict setObject:@"2013-01-08" forKey:@"messageTime"];
            [infoDict setObject:[NSString stringWithFormat:@"第%i条 详细信息详细信息详细信息详细信息详细信息详细信息详细信息详细信息详细信息详细信息详细信息详细信息详细信息详细信息详细信息详细信息详细信息详细信息详细信息详细信息",i] forKey:@"messageDetail"];
            [messageArr addObject:infoDict];
        }
    }
}

#pragma mark - 加载显示系统信息的表格UI
-(void)addMessageUITableView{
    UITableView * messageTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 47.0, 320.0, 413.0) style:UITableViewStylePlain];
    messageTable.backgroundColor = [UIColor clearColor];
    messageTable.delegate = self;
    messageTable.dataSource = self;
    messageTable.bounces = NO;
    messageTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:messageTable];
    [messageTable release];
}

#pragma mark - 表格操作协议处理
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSDictionary * curDict = [messageArr objectAtIndex:[indexPath row]];
    messageDetailVC * detailVC = [[messageDetailVC alloc] initWithMessageInfo:curDict];
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - 表格数据协议处理 
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [messageArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * messageCellIdentify = @"messageCellIdentify";
    UITableViewCell * curCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:messageCellIdentify];
    if (curCell==nil) {
        curCell = [tableView dequeueReusableCellWithIdentifier:messageCellIdentify];
    }
    
    for (UIView * subview in curCell.subviews) {
        if (subview.tag>100) {
            [subview removeFromSuperview];
        }
    }
    
    //曹工，具体怎么布局你到时再调整了
    
    NSDictionary * curDict = (NSDictionary*)[messageArr objectAtIndex:[indexPath row]];
    
    curCell.textLabel.text = (NSString *)[curDict objectForKey:@"sysInformation"];
    
    /*
    //图片
    UIImageView * picView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 5.0, 50.0, 50.0)];
    picView.image = (UIImage *)[curDict objectForKey:@"picImage"];
    picView.tag = 111;
    [curCell addSubview:picView];
    [picView release];
    // Get the Layer of any view
    CALayer * l = [picView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:10.0];
    
    //标题
    UILabel * titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(80.0, 0.0, 80.0, 25.0)];
    titleLbl.backgroundColor = [UIColor clearColor];
    titleLbl.text = (NSString *)[curDict objectForKey:@"messageTitle"];
    titleLbl.tag = 112;
    [curCell addSubview:titleLbl];
    [titleLbl release];
    
    //时间
    UILabel * timeLbl = [[UILabel alloc] initWithFrame:CGRectMake(200.0, 0.0, 100.0, 25.0)];
    timeLbl.backgroundColor = [UIColor clearColor];
    timeLbl.text = (NSString *)[curDict objectForKey:@"messageTime"];
    timeLbl.tag = 113;
    [curCell addSubview:timeLbl];
    [timeLbl release];
    
    //详细的文章
    NSMutableString * curMessageStr = [[NSMutableString alloc] initWithString:(NSString *)[curDict objectForKey:@"messageDetail"]];
    if ([curMessageStr length]>20) {
        NSRange range = NSMakeRange(0, 20);
        curMessageStr = [NSString stringWithFormat:@"%@....",[curMessageStr substringWithRange:range]];
    }
    UITextView * detailTxt = [[UITextView alloc] initWithFrame:CGRectMake(74.0, 26.0, 222.0, 50.0)];
    detailTxt.userInteractionEnabled = NO;
    detailTxt.text = curMessageStr;
    detailTxt.backgroundColor = [UIColor clearColor];
    [curCell addSubview:detailTxt];
    [detailTxt release];
    */
    //分隔线
    UIImageView * line = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 79.0, 320.0, 1.0)];
    line.image = [UIImage imageNamed:@"shezhi_lineacc.png"];
    line.tag = 115;
    [curCell addSubview:line];
    [line release];

    return curCell;
}
@end
