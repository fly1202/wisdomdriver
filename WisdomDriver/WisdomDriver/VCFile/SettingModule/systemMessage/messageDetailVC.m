//
//  messageDetailVC.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-14.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "messageDetailVC.h"

@interface messageDetailVC (){
    NSDictionary * curMessageInfoDict;
}

@end

@implementation messageDetailVC

- (id)initWithMessageInfo:(NSDictionary *)messageInfo
{
    self = [super init];
    if (self) {
        // Custom initialization
        curMessageInfoDict = [[NSDictionary alloc] initWithDictionary:messageInfo];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //self.view.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0];
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    //NSString * title = (NSString *)[curMessageInfoDict objectForKey:@"messageTitle"];
    NSString * title = @"消息详情";
    [self addTopViewWithTitle:title];
    
    [self addDetailMessageUI];
}

#pragma mark - 添加顶部条栏和文字,返回按钮
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //返回按钮
    UIButton * backBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 58.0, 30.0)];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [backBut setTitle:@"返回" forState:UIControlStateNormal];
    [backBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBut addTarget:self action:@selector(clickOnBackBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBut];
    [backBut release];
    
}

-(void)clickOnBackBut{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 加载详细内容的页面UI布局
-(void)addDetailMessageUI{
    //曹工，具体怎么布局和功能处理，这个就由你来弄了
    UITextView * txtView = [[UITextView alloc] initWithFrame:CGRectMake(0, 45, 320, 400)];
    txtView.editable = FALSE;
    txtView.backgroundColor = [UIColor clearColor];
    txtView.font = [UIFont systemFontOfSize:15.0];
    txtView.textColor = [UIColor blackColor];
    txtView.text = [curMessageInfoDict objectForKey:@"sysInformation"];
    
    [self.view addSubview:txtView];
}

@end
