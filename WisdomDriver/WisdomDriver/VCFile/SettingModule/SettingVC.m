//
//  SettingVC.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-8.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "SettingVC.h"
#import "RunTimeShare.h"
#import "TCWBEngine.h"
#import "WeiboAccounts.h"

@interface SettingVC (){
    
}

-(void)addSettingUI;

@end

@implementation SettingVC

@synthesize setDelegate;

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    [self addTopViewWithTitle:@"设置"];
    
    [self addSettingUI];
}

#pragma mark - 添加顶部条栏和文字,收费标准,设置
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //返回按钮
    UIButton * backBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 58.0, 30.0)];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [backBut setTitle:@"返回" forState:UIControlStateNormal];
    backBut.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBut addTarget:self action:@selector(clickOnBackBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBut];
    [backBut release];
}

-(void)clickOnBackBut{
    [self.setDelegate SettingVCDidDismiss];
}

#pragma mark - 加载各个具体设置UI
-(void)addSettingUI{
    //加载个人设置和系统设置
    UIImageView * bottomView1 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 55.0, 300.0, 90.0)];
    bottomView1.image = [UIImage imageNamed:@"bg_customtable2.png"];
    bottomView1.userInteractionEnabled = YES;
    [self.view addSubview:bottomView1];
    [bottomView1 release];
    
    NSArray * strArr = [[NSArray alloc] initWithObjects:@"个人设置",@"系统设置", nil];
    for (int i=0; i<[strArr count]; i++) {
        UILabel * txtLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 45.0*i, 200.0, 45.0)];
        txtLbl.backgroundColor = [UIColor clearColor];
        txtLbl.text = [strArr objectAtIndex:i];
        [bottomView1 addSubview:txtLbl];
        [txtLbl release];
        
        UIImageView * arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 45.0*i+15.0, 10.0, 14.0)];
        arrowView.image = [UIImage imageNamed:@"acc_jiantou.png"];
        [bottomView1 addSubview:arrowView];
        [arrowView release];
        
        UIButton * but = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 45.0*i, 300.0, 45.0)];
        but.tag = 1000+i;
        but.backgroundColor = [UIColor clearColor];
        [but addTarget:self action:@selector(clickOnSettingBut:) forControlEvents:UIControlEventTouchUpInside];
        [bottomView1 addSubview:but];
        [but release];
    }
    
    //加载系统消息按钮
    UIImageView * bottomView2 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 160.0, 300.0, 45.0)];
    bottomView2.image = [UIImage imageNamed:@"bg_customtable1.png"];
    bottomView2.userInteractionEnabled = YES;
    [self.view addSubview:bottomView2];
    [bottomView2 release];

    UILabel * txtLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, 200.0, 45.0)];
    txtLbl.backgroundColor = [UIColor clearColor];
    txtLbl.text = @"系统消息";
    [bottomView2 addSubview:txtLbl];
    [txtLbl release];
    
    UIImageView * arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 15.0, 10.0, 14.0)];
    arrowView.image = [UIImage imageNamed:@"acc_jiantou.png"];
    [bottomView2 addSubview:arrowView];
    [arrowView release];
    
    UIButton * messageBut = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 300.0, 45.0)];
    messageBut.backgroundColor = [UIColor clearColor];
    messageBut.tag = 1002;
    [messageBut addTarget:self action:@selector(clickOnSettingBut:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView2 addSubview:messageBut];
    [messageBut release];

    //加载分享按钮
    UIImageView * bottomView3 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 220.0, 300.0, 45.0)];
    bottomView3.image = [UIImage imageNamed:@"bg_customtable1.png"];
    bottomView3.userInteractionEnabled = YES;
    [self.view addSubview:bottomView3];
    [bottomView3 release];
    
    UILabel * txtLbl3 = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, 200.0, 45.0)];
    txtLbl3.backgroundColor = [UIColor clearColor];
    txtLbl3.text = @"分享";
    [bottomView3 addSubview:txtLbl3];
    [txtLbl3 release];
    
    UIImageView * arrowView3 = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 15.0, 10.0, 14.0)];
    arrowView3.image = [UIImage imageNamed:@"acc_jiantou.png"];
    [bottomView3 addSubview:arrowView3];
    [arrowView3 release];
    
    UIButton * shareBut = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 300.0, 45.0)];
    shareBut.backgroundColor = [UIColor clearColor];
    shareBut.tag = 1003;
    [shareBut addTarget:self action:@selector(clickOnSettingBut:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView3 addSubview:shareBut];
    [shareBut release];

    //加载退出登录的按钮
    UIButton * logoutBut = [[UIButton alloc] initWithFrame:CGRectMake(10.0, 290.0, 300.0, 44.0)];
    [logoutBut setBackgroundImage:[UIImage imageNamed:@"logoutBtnView.png"] forState:UIControlStateNormal];
    [logoutBut setTitle:@"退出登录" forState:UIControlStateNormal];
    [logoutBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logoutBut.titleLabel.font = [UIFont systemFontOfSize:18.0];
    [logoutBut addTarget:self action:@selector(clickOnLogoutBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutBut];
    [logoutBut release];
}

#pragma mark - 点击进入不同的设置界面
-(void)clickOnSettingBut:(id)sender{
    int curTag = ((UIButton *)sender).tag - 1000;
    [self.setDelegate EnterSettingDetailVCWithTag:curTag];
}

#pragma mark - 退出登录
-(void)clickOnLogoutBut{
    //先退出设置界面
    [self.setDelegate SettingVCDidDismiss];
    
    [RunTimeShare setIsAutoLogin:FALSE];
    //再加载回到登录界面
    [self.setDelegate UserClickOnLogoutBut];
    
    // setup qq weibo engine
    TCWBEngine * weiboEngine = [[TCWBEngine alloc] initWithAppKey:WiressSDKDemoAppKey andSecret:WiressSDKDemoAppSecret andRedirectUrl:REDIRECTURI];
    //[weiboEngine setRootViewController:self];
    [weiboEngine logOut];
    
    [[WeiboAccounts shared] signOut];
}

@end
