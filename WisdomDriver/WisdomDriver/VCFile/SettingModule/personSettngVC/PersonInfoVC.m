//
//  PersonInfoVC.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-9.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "PersonInfoVC.h"
#import "DCRoundSwitch.h"
#import "ClientJSONObject.h"
#import "SVProgressHUD.h"
#import "RunTimeShare.h"
#import "defines.h"

@interface PersonInfoVC ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>{
    BOOL isEditState;
    NSMutableDictionary * curPersonInfoDict;
    UIImageView * headView;  //头像
    UIView  * personBgView;
    NSMutableArray * fieldArr;
    DCRoundSwitch * sexSwich;
}

@end

@implementation PersonInfoVC

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        isEditState = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0];
    personBgView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 460.0)];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [personBgView addSubview:bgImgView];
    [self.view addSubview:personBgView];
    [personBgView release];
    
    [self addTopViewWithTitle:@"个人信息"];
    
    [self showWithStatusMsg:@"正在更新个人信息"];
    
    [self performSelector:@selector(updatePersonInfo) withObject:self afterDelay:0.5];
}

#pragma mark - 更新个人数据
-(void)updatePersonInfo{
    NSDictionary * personDict = [[ClientJSONObject clientJSONObject] getUserInfoWithTel:[RunTimeShare getUserTel]];
    [RunTimeShare setUserInfo:personDict];
    [self dismiss];
    [self getPersonInfomation];
    [self addPersonInfoUI];
}

#pragma mark - 添加顶部条栏和文字,返回按钮
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //返回按钮
    UIButton * backBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 58.0, 30.0)];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [backBut setTitle:@"返回" forState:UIControlStateNormal];
    [backBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBut addTarget:self action:@selector(clickOnBackBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBut];
    [backBut release];
    
    //编辑(提交)按钮，默认是编辑状态
    UIButton * settingBut = [[UIButton alloc] initWithFrame:CGRectMake(256.0, 8.0, 58.0, 30.0)];
    [settingBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [settingBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [settingBut setTitle:@"编辑" forState:UIControlStateNormal];
    [settingBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [settingBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [settingBut addTarget:self action:@selector(clickOnSettingBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:settingBut];
    [settingBut release];
}

-(void)clickOnBackBut{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 加载数据布局UI
-(void)addPersonInfoUI{
    //加载头像UI布局
    UIImageView * bottomView1 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 52.0, 300.0, 90.0)];
    bottomView1.image = [UIImage imageNamed:@"bg_customtable5.png"];
    bottomView1.userInteractionEnabled = YES;
    [personBgView addSubview:bottomView1];
    [bottomView1 release];

    UILabel * headLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 25.0, 70.0, 40.0)];
    headLbl.text = @"头像:";
    headLbl.textAlignment = UITextAlignmentRight;
    headLbl.backgroundColor = [UIColor clearColor];
    [bottomView1 addSubview:headLbl];
    [headLbl release];
    
    headView = [[UIImageView alloc] initWithFrame:CGRectMake(212.0, 10.0, 70.0, 70.0)];
    headView.image = (UIImage *)[curPersonInfoDict objectForKey:@"curHeadView"];
    [bottomView1 addSubview:headView];
    [headView release];
    
    //头像修改按钮
    UIButton * headViewChangeBut = [[UIButton alloc] initWithFrame:CGRectMake(208.0, 5.0, 78.0, 78.0)];
    headViewChangeBut.backgroundColor = [UIColor clearColor];
    [headViewChangeBut addTarget:self action:@selector(changeHeadView) forControlEvents:UIControlEventTouchUpInside];
    [bottomView1 addSubview:headViewChangeBut];
    [headViewChangeBut release];
    
    //姓名，性别，手机号编辑UI
    fieldArr = [[NSMutableArray alloc] init];
    
    UIImageView * bottomView2 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 152.0, 300.0, 135.0)];
    bottomView2.image = [UIImage imageNamed:@"bg_customtable3.png"];
    bottomView2.userInteractionEnabled = YES;
    [personBgView addSubview:bottomView2];
    [bottomView2 release];

    NSArray * strArr = [[NSArray alloc] initWithObjects:@"姓名:",@"性别:",@"手机号:",nil];
    for (int i=0; i<[strArr count]; i++) {
        UILabel * strLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 45.0*i, 70.0, 45.0)];
        strLbl.text = [strArr objectAtIndex:i];
        strLbl.textAlignment = UITextAlignmentRight;
        strLbl.backgroundColor = [UIColor clearColor];
        [bottomView2 addSubview:strLbl];
        [strLbl release];
        
        //增加性别修改UI
        if (i==1) {
            sexSwich = [[DCRoundSwitch alloc] initWithFrame:CGRectMake(212.0, 55.0, 70.0, 25.0)];
            sexSwich.onText = @"男";
            sexSwich.offText = @"女";
            [sexSwich setOn:YES animated:NO ignoreControlEvents:YES];
            NSString * XB = [curPersonInfoDict objectForKey:@"personSex"];
            if ([XB isEqualToString:kUserInfoSexFemale]) {
                [sexSwich setOn:NO animated:NO ignoreControlEvents:YES];
            }
            
            [sexSwich addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
            sexSwich.enabled = NO;
            [bottomView2 addSubview:sexSwich];
            [sexSwich release];
        }
        //增加姓名和手机号的输入框
        else{
            UITextField * textField = [[UITextField alloc] initWithFrame:CGRectMake(130.0, 45.0*i+10.0, 158.0, 40.0)];
            textField.backgroundColor = [UIColor clearColor];
            textField.placeholder = @"请输入姓名";
            if (![[curPersonInfoDict valueForKey:@"personName"] isEqualToString:@"null"]) {
                textField.text = [curPersonInfoDict valueForKey:@"personName"];
            }
            if (i==2) {
                textField.placeholder = @"请输入手机号码";
                textField.text = [curPersonInfoDict valueForKey:@"phoneNumber"];
            }
            textField.tag = i;
            textField.textAlignment = UITextAlignmentRight;
            textField.delegate = self;
            textField.enabled = NO;
            if (i==2) {
                textField.keyboardType = UIKeyboardTypePhonePad;
            }
            [bottomView2 addSubview:textField];
            [fieldArr addObject:textField];
            [textField release];
        }
    }
}

#pragma mark - 选择性别
-(void)switchAction:(UISwitch *)seader
{
    if (seader.on) {
        //男
        [curPersonInfoDict setObject:kUserInfoSexMale forKey:@"personSex"];
    }
    else
    {
        //女
        [curPersonInfoDict setObject:kUserInfoSexFemale forKey:@"personSex"];
    }
}

#pragma mark - 修改头像
-(void)changeHeadView{
    //如果不在修改状态中，不可点击
    if (isEditState) {
        return;
    }
    
    //曹工，请调用图片库进行修改，将数据存入curPersonInfoDict中
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"选择头像"
                                  delegate:self
                                  cancelButtonTitle:@"取消"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"拍照",@"从相册选取",nil];
    
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    [actionSheet release];
}

#pragma mark ActionSheet Delegate Methods
- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSLog(@"actionSheet:didDismissWithButtonIndex:%d",buttonIndex);
    if (buttonIndex == 0){
        // 拍照
        [self addPicEvent];
    } else if (buttonIndex == 1) {
        // 从相册选取
        if ([self startMediaBrowserFromViewController:self usingDelegate:self]) {
            NSLog(@"show PhotoAlbum");
        }
    } 
}

#pragma mark - 照片库选取
- (BOOL) startMediaBrowserFromViewController: (UIViewController*) controller
                               usingDelegate: (id <UIImagePickerControllerDelegate,
                                               UINavigationControllerDelegate>) delegate {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    mediaUI.title = @"选取头像";
    
    // Displays saved pictures and movies, if both are available, from the
    // Camera Roll album.
    mediaUI.mediaTypes =
    [UIImagePickerController availableMediaTypesForSourceType:
     UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    mediaUI.allowsEditing = YES;
    
    mediaUI.delegate = delegate;
    
    [controller presentModalViewController: mediaUI animated: YES];
    return YES;
}

- (void) addPicEvent
{
	UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
		sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	}
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = sourceType;
    [self presentModalViewController:picker animated:YES];
    [picker release];
}

- (void)saveImage:(UIImage *)image {
	NSLog(@"saveImage:");
    
    [curPersonInfoDict setObject:image forKey:@"curHeadView"];
    headView.image = (UIImage *)[curPersonInfoDict objectForKey:@"curHeadView"];
}
#pragma mark -
#pragma mark Camera View Delegate Methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSLog(@"imagePickerController didFinishPickingMediaWithInfo");
    [picker dismissModalViewControllerAnimated:YES];
    UIImage *image = [[info objectForKey:UIImagePickerControllerEditedImage] retain];
    [self performSelector:@selector(saveImage:)
               withObject:image
               afterDelay:0.5];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissModalViewControllerAnimated:YES];
}

#pragma mark - 编辑(提交)按钮功能
-(void)clickOnSettingBut:(id)sender{
    //提交信息，成功与否都给出提示
    //成功后存入本地数据库，不成功则显示提交前的信息
    NSString * tel = [[fieldArr objectAtIndex:1] text];
    NSString * name = [[fieldArr objectAtIndex:0] text];
    NSString * logo = @"";
    NSString * sex = [curPersonInfoDict valueForKey:@"personSex"];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [personBgView setFrame:CGRectMake(0.0, 0.0, 320.0, 460.0)];
    [UIView commitAnimations];

    isEditState = !isEditState;
    if (isEditState) {
        //进行提交动作，并将按钮转为编辑状态
        [(UIButton *)sender setTitle:@"编辑" forState:UIControlStateNormal];
        
        if (tel == nil || [tel length] == 0) {
            [self dismissErrorMsg:@"手机号码为空，请输入后提交"];
        }else if (name == nil || [name length] == 0) {
            [self dismissErrorMsg:@"姓名为空，请输入后提交"];
        }else{
            NSDictionary * dict = [[ClientJSONObject clientJSONObject] saveUserWithTel:tel logo:logo name:name sex:sex];
            NSString * result = [dict valueForKey:@"result"];
            if (result.integerValue == 1) {
                [self dismissSuccessMsg:@"个人信息提交成功"];
                [self savePersionInformation];
            }else{
                //目前只是给出一个提示
                [self dismissErrorMsg:@"个人信息提交失败，请重新提交"];
                [self getPersonInfomation];
            }
        }
    }
    else {
        //进入编辑，并将按钮转为提交状态，编辑后可以提交
        [(UIButton *)sender setTitle:@"提交" forState:UIControlStateNormal];
        
    }
    
    //修改输入框能否编辑
    [self setDataEditEnable];
}

#pragma mark - 将数据变为可以编辑
-(void)setDataEditEnable{
    //如果不在修改状态中，不可修改
    for (int i=0; i<[fieldArr count]; i++) {
        UITextField * curField = (UITextField *)[fieldArr objectAtIndex:i];
        curField.enabled = YES;
        //手机号不可编辑
        if (isEditState || i == ([fieldArr count]-1)) {
            curField.enabled = NO;
        }
    }

    sexSwich.enabled = YES;
    if (isEditState) {
        sexSwich.enabled = NO;
    }
}

#pragma mark - TextField Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [personBgView setFrame:CGRectMake(0.0, 0.0, 320.0, 460.0)];
    [UIView commitAnimations];
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.4];
    [personBgView setFrame:CGRectMake(0.0, -80.0, 320.0, 460.0)];
    [UIView commitAnimations];
    
	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
}


#pragma mark - 从本地数据中获取个人信息资料
-(void)getPersonInfomation{
    
    //曹工，这个功能需要你来实现，我现在只是做一个假设的数据，key值都已经设定好
    if (curPersonInfoDict) {
        curPersonInfoDict = nil;
    }
    curPersonInfoDict = [[NSMutableDictionary alloc] init];
    
    NSDictionary * dict = [RunTimeShare getUserInfo];
    
    [curPersonInfoDict setObject:[dict valueForKey:kUserInfoClientLogo] forKey:@"curHeadView"];
    [curPersonInfoDict setObject:[dict valueForKey:kUserInfoNickName] forKey:@"personName"];
    [curPersonInfoDict setObject:[dict valueForKey:kUserInfoSex] forKey:@"personSex"];
    [curPersonInfoDict setObject:[RunTimeShare getUserTel] forKey:@"phoneNumber"];
}

#pragma mark - 保存数据到本地
-(void)savePersionInformation{
    
    //保存头像
    UIImage * headImage = [curPersonInfoDict valueForKey:@"curHeadView"];
    NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
    [dict setValue:headImage forKey:kUserInfoClientLogo];
    [dict setValue:[curPersonInfoDict valueForKey:@"personName"] forKey:kUserInfoNickName];
    [dict setValue:[curPersonInfoDict valueForKey:@"personSex"] forKey:kUserInfoSex];
    [RunTimeShare setUserTel:[curPersonInfoDict valueForKey:@"phoneNumber"]];
    
    [RunTimeShare setUserInfo:dict];
    
    [curPersonInfoDict setObject:[dict objectForKey:kUserInfoClientLogo] forKey:@"curHeadView"];
    [curPersonInfoDict setObject:[dict objectForKey:kUserInfoNickName] forKey:@"personName"];
    [curPersonInfoDict setObject:[dict objectForKey:kUserInfoSex] forKey:@"personSex"];
    [curPersonInfoDict setObject:[RunTimeShare getUserTel] forKey:@"phoneNumber"];
}

#pragma mark -
#pragma mark 显示提醒框

- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框

- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}
@end
