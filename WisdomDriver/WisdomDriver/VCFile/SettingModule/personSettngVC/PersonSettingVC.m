//
//  PersonSettingVC.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-8.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "PersonSettingVC.h"
#import "PersonInfoVC.h"
#import "UsedAddressVC.h"
#import "RemindMethodVC.h"
#import "ChangePWVC.h"

@interface PersonSettingVC ()

@end

@implementation PersonSettingVC

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    [self addTopViewWithTitle:@"个人设置"];
    
    [self addButtonView];
}

#pragma mark - 添加顶部条栏和文字,返回按钮
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //返回按钮
    UIButton * backBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 58.0, 30.0)];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [backBut setTitle:@"返回" forState:UIControlStateNormal];
    [backBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBut addTarget:self action:@selector(clickOnBackBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBut];
    [backBut release];
    
}

-(void)clickOnBackBut{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 添加按钮UI
-(void)addButtonView{
    NSArray * textArr = [[NSArray alloc] initWithObjects:@"个人信息",@"常用地址",@"提醒方式",@"更改密码", nil];
    for (int i=0; i<[textArr count]; i++) {
        UIImageView * bottomView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 55.0+55.0*i, 300.0, 45.0)];
        bottomView.image = [UIImage imageNamed:@"bg_customtable1.png"];
        bottomView.userInteractionEnabled = YES;
        [self.view addSubview:bottomView];
        [bottomView release];
        
        UIImageView * arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 15.0, 10.0, 14.0)];
        arrowView.image = [UIImage imageNamed:@"acc_jiantou.png"];
        [bottomView addSubview:arrowView];
        [arrowView release];

        UILabel * txtLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, 200.0, 45.0)];
        txtLbl.backgroundColor = [UIColor clearColor];
        txtLbl.text = [textArr objectAtIndex:i];
        [bottomView addSubview:txtLbl];
        [txtLbl release];
        
        UIButton * but = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 300.0, 45.0)];
        but.backgroundColor = [UIColor clearColor];
        but.tag = 200+i;
        [but addTarget:self action:@selector(clickOnSettingBut:) forControlEvents:UIControlEventTouchUpInside];
        [bottomView addSubview:but];
        [but release];

    }
}

#pragma mark - 点击各个按钮进入详情设置
-(void)clickOnSettingBut:(id)sender{
    int selTag = ((UIButton*)sender).tag - 200;
    switch (selTag) {
        case 0:
        {
            //个人信息设置
            PersonInfoVC * personInfoVC = [[PersonInfoVC alloc] init];
            [self.navigationController pushViewController:personInfoVC animated:YES];
        }
            break;
        case 1:
        {
            //常用地址设置
            UsedAddressVC * addressVC = [[UsedAddressVC alloc] init];
            [self.navigationController pushViewController:addressVC animated:YES];
        }
            break;
        case 2:
        {
            //提醒方式设置
            RemindMethodVC * remindVC = [[RemindMethodVC alloc] init];
            [self.navigationController pushViewController:remindVC animated:YES];
        }
            break;
        case 3:
        {
            //更改密码设置
            ChangePWVC * changeVC = [[ChangePWVC alloc] init];
            [self.navigationController pushViewController:changeVC animated:YES];
        }
            break;
        default:
            break;
    }
}

@end
