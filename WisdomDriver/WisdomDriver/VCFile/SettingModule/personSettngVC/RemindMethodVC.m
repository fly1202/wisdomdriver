//
//  RemindMethodVC.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-11.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "RemindMethodVC.h"
#import "DCRoundSwitch.h"
#import "RunTimeShare.h"

@interface RemindMethodVC ()

@end

@implementation RemindMethodVC

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    [self addTopViewWithTitle:@"提醒方式"];
    
    [self addRemindUI];
}

#pragma mark - 添加顶部条栏和文字,收费标准,设置
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //返回按钮
    UIButton * backBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 58.0, 30.0)];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [backBut setTitle:@"返回" forState:UIControlStateNormal];
    [backBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBut addTarget:self action:@selector(clickOnBackBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBut];
    [backBut release];
}

-(void)clickOnBackBut{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 提醒方式设置的UI布局
-(void)addRemindUI{
    UIImageView * bottomView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 52.0, 300.0, 90.0)];
    bottomView.image = [UIImage imageNamed:@"bg_customtable2.png"];
    bottomView.userInteractionEnabled = YES;
    
    NSArray * strArr = [[NSArray alloc] initWithObjects:@"震动提醒",@"声音提醒", nil];
    NSArray * swithArr = [[NSArray alloc] initWithObjects:@"震动",@"声音", nil];
    for (int i=0; i<[strArr count]; i++) {
        UILabel * strLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 45.0*i, 100.0, 45.0)];
        strLbl.backgroundColor = [UIColor clearColor];
        strLbl.text = [strArr objectAtIndex:i];
        [bottomView addSubview:strLbl];
        [strLbl release];
        
        DCRoundSwitch * curSwitch = [[DCRoundSwitch alloc] initWithFrame:CGRectMake(200.0, 45.0*i+9.0, 70.0, 25.0)];
        
        //是否提醒根据本地数据保存来定
        if (i==0) {
            [curSwitch setOn:[RunTimeShare isVibrationAlert] animated:NO ignoreControlEvents:YES];
        }
        else{
            [curSwitch setOn:[RunTimeShare isSoundAlert] animated:NO ignoreControlEvents:YES];
        }
        curSwitch.onText = [swithArr objectAtIndex:i];
        curSwitch.offText = @"停止";
        curSwitch.tag = i;
        [curSwitch addTarget:self action:@selector(changeAlertMethod:) forControlEvents:UIControlEventValueChanged];
        
        [bottomView addSubview:curSwitch];
        [curSwitch release];
    }
    
    [self.view addSubview:bottomView];
    [bottomView release];
}

#pragma mark - 修改提醒方式
-(void)changeAlertMethod:(id)sender{
    int curSwithTag = ((DCRoundSwitch *)sender).tag;
    switch (curSwithTag) {
        case 0:
        {
            [RunTimeShare setIsVibrationAlert:((DCRoundSwitch *)sender).on];
        }
            break;
        case 1:
        {
            [RunTimeShare setSoundAlert:((DCRoundSwitch *)sender).on];
        }
            break;
        default:
            break;
    }
}

@end
