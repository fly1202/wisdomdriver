//
//  ChangePWVC.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-12.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "ChangePWVC.h"
#import "ClientJSONObject.h"
#import "SVProgressHUD.h"
#import "RunTimeShare.h"

#define TAG_SUCCESS 1000+1

@interface ChangePWVC (){
    UIView * bottomView;
    NSMutableArray * fieldArr;
}

@end

@implementation ChangePWVC

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //self.view.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0];
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    [self addChangePasswordUI];
    
    [self addTopViewWithTitle:@"修改密码"];
    
}

#pragma mark - 添加顶部条栏和文字,收费标准,设置
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //返回按钮
    UIButton * backBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 58.0, 30.0)];
    [backBut setBackgroundImage:[UIImage imageNamed:@"navBarLeftButBottom.png"] forState:UIControlStateNormal];
    [backBut setTitle:@"取消" forState:UIControlStateNormal];
    [backBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBut addTarget:self action:@selector(clickOnBackBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBut];
    [backBut release];
}

-(void)clickOnBackBut{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 加载修改密码的UI布局
-(void)addChangePasswordUI{
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 460.0)];
    //bottomView.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0];
    //[bottomView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [bottomView addSubview:bgImgView];
    [self.view addSubview:bottomView];
    [bottomView release];
    
    fieldArr = [[NSMutableArray alloc] init];
    NSArray *strArr = [[NSArray alloc] initWithObjects:@"原始密码:",@"新密码:",@"重复输入:", nil];
    for (int i=0; i<[strArr count]; i++) {
        UIImageView * bgView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 53.0+55.0*i, 300.0, 45.0)];
        bgView.image = [UIImage imageNamed:@"bg_customtable1.png"];
        bgView.userInteractionEnabled = YES;
        [bottomView addSubview:bgView];
        [bgView release];

        UILabel * strLbl = [[UILabel alloc] initWithFrame:CGRectMake(5.0, 5.0, 80.0, 35.0)];
        strLbl.backgroundColor = [UIColor clearColor];
        strLbl.textAlignment = UITextAlignmentRight;
        strLbl.text = [strArr objectAtIndex:i];
        [bgView addSubview:strLbl];
        [strLbl release];
        
        UITextField * textField = [[UITextField alloc] initWithFrame:CGRectMake(93.0, 12.0, 198.0, 44.0)];
        textField.backgroundColor = [UIColor clearColor];
        textField.tag = i;
        textField.secureTextEntry = TRUE;
        textField.delegate = self;
        textField.font = [UIFont systemFontOfSize:20.0];
        [bgView addSubview:textField];
        [fieldArr addObject:textField];
        [textField release];
    }
    
    //提交修改密码
    UIButton * submitBut = [[UIButton alloc] initWithFrame:CGRectMake(10.0, 228.0, 300.0, 44.0)];
    [submitBut setBackgroundImage:[UIImage imageNamed:@"blueBut.png"] forState:UIControlStateNormal];
    [submitBut setTitle:@"提交修改" forState:UIControlStateNormal];
    [submitBut.titleLabel setFont:[UIFont systemFontOfSize:18.0]];
    [submitBut addTarget:self action:@selector(submitChangePassword) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:submitBut];
    [submitBut release];
}

#pragma mark - 提交修改后的密码
-(void)submitChangePassword{
    //曹工，在数组fieldArr中，第一个field是原先的密码，你必须和之前保存的密码对比是否正确，后面的field是新的密码，要确保两次输入相同才可以提交
    //提交成功后将新密码保存并提示，点击提示后退出这个界面
    //失败则给出提示，清空输入框，留在这个界面
    NSString * recordPW = [[fieldArr objectAtIndex:0] text];
    NSString * tel = [RunTimeShare getUserTel];
    NSString * newPwd = [[fieldArr objectAtIndex:2] text];
    
    if ([[[fieldArr objectAtIndex:1] text] length] < 6 ||
        [[[fieldArr objectAtIndex:1] text] length] > 12) {
        
        [self dismissErrorMsg:@"请输入6-12位新密码."];
        return;
    }
    
    //判断重复密码
    if (![[[fieldArr objectAtIndex:1] text] isEqualToString:[[fieldArr objectAtIndex:2] text]]) {
        [self dismissErrorMsg:@"两次输入的新密码不同，请重新输入."];
        return;
    }
    
    NSDictionary * dict = [[ClientJSONObject clientJSONObject] changePwdWithTel:tel pwd:recordPW newPwd:newPwd];
    NSString * result = [dict valueForKey:@"result"];
    
    switch ([result integerValue]) {
            case 1:
        {
            [self dismissSuccessMsg:@"修改密码已经成功，请下次登录使用新密码."];
            [[RunTimeShare sharedInstance] setUserPwd:newPwd];
            [self clickOnBackBut];
        }
            break;
            case 0:
        {
            [self dismissErrorMsg:@"修改密码失败，请重新修改."];
        }
            break;
            case 5:
        {
            [self dismissErrorMsg:@"原始密码错误，请重新修改."];
        }
            break;
        default:
            break;
    }
}

#pragma mark - TextField Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5f];
    [bottomView setFrame:CGRectMake(0.0, 0.0, 320.0, 460.0)];
    [UIView commitAnimations];

    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag==2) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5f];
        [bottomView setFrame:CGRectMake(0.0, -20.0, 320.0, 460.0)];
        [UIView commitAnimations];
    }
    else{
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5f];
        [bottomView setFrame:CGRectMake(0.0, 0.0, 320.0, 460.0)];
        [UIView commitAnimations];
    }
	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];

}

#pragma mark -
#pragma mark 显示提醒框

- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框

- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}
@end
