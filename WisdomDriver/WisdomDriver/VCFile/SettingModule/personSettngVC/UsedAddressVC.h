//
//  UsedAddressVC.h
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-10.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UsedAddressVCDelegate <NSObject>

-(void)didSelectAddress:(NSDictionary*)addrDict;

@end

@interface UsedAddressVC : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    id<UsedAddressVCDelegate> vcDelegate;
}
@property (nonatomic,assign) id<UsedAddressVCDelegate> vcDelegate;
@end
