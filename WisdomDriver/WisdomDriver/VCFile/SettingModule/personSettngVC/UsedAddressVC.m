//
//  UsedAddressVC.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-10.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "UsedAddressVC.h"
#import "RunTimeShare.h"
#import "defines.h"
#import "ChoseLocationVC.h"

@interface UsedAddressVC ()<ChoseLocationVCDelegate>{
    NSMutableArray * UsedAddressInfoArr;
    UITableView * addressTable;
}

-(void)getUsedAddressInfoArr;

@end

@implementation UsedAddressVC
@synthesize vcDelegate;
- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.view.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0];
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    [self getUsedAddressInfoArr];
    
    [self addTopViewWithTitle:@"常用地址"];
    
    //加载常用地址数据列表AddressTable
    addressTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 47.0, 320.0, 413.0) style:UITableViewStylePlain];
    addressTable.delegate = self;
    addressTable.dataSource = self;
    addressTable.backgroundColor = [UIColor clearColor];
    addressTable.bounces = NO;
    addressTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:addressTable];
    [addressTable release];
}

#pragma mark - 添加顶部条栏和文字,返回按钮
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //返回按钮
    UIButton * backBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 58.0, 30.0)];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [backBut setTitle:@"返回" forState:UIControlStateNormal];
    [backBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBut addTarget:self action:@selector(clickOnBackBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBut];
    [backBut release];
    
    //添加按钮
    UIButton * addBut = [[UIButton alloc] initWithFrame:CGRectMake(256.0, 8.0, 58.0, 30.0)];
    [addBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [addBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [addBut setTitle:@"添加" forState:UIControlStateNormal];
    [addBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [addBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addBut addTarget:self action:@selector(clickOnAddBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:addBut];
    [addBut release];
    
}

-(void)clickOnBackBut{
    //曹工，在返回之前应该把目前的常用地址做一个保存
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 增加常用地址
-(void)clickOnAddBut{
    
    ChoseLocationVC * vc = [[ChoseLocationVC alloc] init];
    vc.choseLocationVCDelegate = self;
    [self.navigationController pushViewController:vc animated:TRUE];
}

#pragma mark - 获取现有的本地常用地址
-(void)getUsedAddressInfoArr{
    //曹工请实现这个功能，从本地数据库存取
    //目前使用假数据
    if (UsedAddressInfoArr) {
        [UsedAddressInfoArr removeAllObjects];
        UsedAddressInfoArr = nil;
    }
    
    UsedAddressInfoArr = [[NSMutableArray alloc] init];
    [UsedAddressInfoArr addObjectsFromArray:[[RunTimeShare sharedInstance] getusedAddrArray]];
}

#pragma mark - UITableView的协议处理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [UsedAddressInfoArr count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (vcDelegate && [vcDelegate respondsToSelector:@selector(didSelectAddress:)]) {
        NSDictionary * dict = [UsedAddressInfoArr objectAtIndex:indexPath.row];
        [vcDelegate didSelectAddress:dict];
        [self.navigationController popViewControllerAnimated:TRUE];
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle==UITableViewCellEditingStyleDelete) {
        NSDictionary * dict = [UsedAddressInfoArr objectAtIndex:[indexPath row]];
        [[RunTimeShare sharedInstance] removeUsedAddr:dict];
        [UsedAddressInfoArr removeObjectAtIndex:[indexPath row]];
        [tableView reloadData];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellIdentifier = @"addressCell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
        cell.userInteractionEnabled = YES;
    }
    for (UIView * subView in cell.subviews) {
        if (subView.tag>100) {
            [subView removeFromSuperview];
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary * addrDict = [UsedAddressInfoArr objectAtIndex:[indexPath row]];
    
    UILabel * addressLbl = [[UILabel alloc] initWithFrame:CGRectMake(5.0, 1.0, 268.0, 48.0)];
    addressLbl.text = [addrDict valueForKey:kAddrStr];
    addressLbl.backgroundColor = [UIColor clearColor];
    addressLbl.tag = 200;
    [cell addSubview:addressLbl];
    [addressLbl release];
    
    UIImageView * line = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 49.0, 320.0, 1.0)];
    line.image = [UIImage imageNamed:@"shezhi_lineacc.png"];
    line.tag = 200;
    [cell addSubview:line];
    [line release];
    
    return cell;
}

#pragma - mark 地址添加页面代理
-(void)didChoseLocation:(CLLocation*)loc strAddr:(NSString*)addr{
    [self.navigationController popViewControllerAnimated:TRUE];
    NSDictionary * dict = [RunTimeShare makeAddrDict:addr location:loc];
    [[RunTimeShare sharedInstance] addUsedAddr:dict];
    [self getUsedAddressInfoArr];
    [addressTable reloadData];
}

@end
