//
//  SettingVC.h
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-8.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SettingVCDelegate <NSObject>

-(void)SettingVCDidDismiss;
-(void)EnterSettingDetailVCWithTag:(int)curTag;
-(void)UserClickOnLogoutBut;

@end

@interface SettingVC : UIViewController{
    id <SettingVCDelegate> setDelegate;
}

@property (nonatomic,assign) id <SettingVCDelegate> setDelegate;

@end
