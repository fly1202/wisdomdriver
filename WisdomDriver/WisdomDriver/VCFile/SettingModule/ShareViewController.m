//
//  ShareViewController.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-8.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "ShareViewController.h"
#import <MessageUI/MessageUI.h>
#import "TCWBEngine.h"
#import "WeiboAccounts.h"
#import "WeiboSignIn.h"
#import "defines.h"
#import "SVProgressHUD.h"
#import "ComposeViewController.h"

@interface ShareViewController ()<MFMessageComposeViewControllerDelegate,WeiboSignInDelegate>{
    WeiboSignIn * _weiboSignIn;
    TCWBEngine *  weiboEngine;
}

@end

@implementation ShareViewController
@synthesize shareDelegate;
- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    [self addTopViewWithTitle:@"分享"];
    
    [self addShareButtonUI];
    
    _weiboSignIn = [[WeiboSignIn alloc] init];
    _weiboSignIn.delegate = self;
    
    // setup qq weibo engine
    weiboEngine = [[TCWBEngine alloc] initWithAppKey:WiressSDKDemoAppKey andSecret:WiressSDKDemoAppSecret andRedirectUrl:REDIRECTURI];
    [weiboEngine setRootViewController:self];
    //[engine setRedirectURI:@"http://www.ying7wang7.com"];
}

#pragma mark - 添加顶部条栏和文字,返回按钮
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //返回按钮
    UIButton * backBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 58.0, 30.0)];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [backBut setTitle:@"返回" forState:UIControlStateNormal];
    backBut.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBut addTarget:self action:@selector(clickOnBackBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBut];
    [backBut release];
}

-(void)clickOnBackBut{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 加载分享按钮UI布局
-(void)addShareButtonUI{
    NSArray * strArr = [[NSArray alloc] initWithObjects:@"腾讯微博分享",@"新浪微博分享",@"短信分享", nil];
    for (int i=0; i<[strArr count]; i++) {
        //底图
        UIImageView * bottomView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 55.0+55.0*i, 300.0, 45.0)];
        bottomView.image = [UIImage imageNamed:@"bg_customtable1.png"];
        bottomView.userInteractionEnabled = YES;
        [self.view addSubview:bottomView];
        [bottomView release];
        
        //图像
        UIImageView * weiboView = [[UIImageView alloc] initWithFrame:CGRectMake(8.0, 2.0, 40.0, 40.0)];
        weiboView.image = [UIImage imageNamed:[NSString stringWithFormat:@"weibo%i.png",i+1]];
        [bottomView addSubview:weiboView];
        [weiboView release];
        
        //箭头
        UIImageView * arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 15.0, 10.0, 14.0)];
        arrowView.image = [UIImage imageNamed:@"acc_jiantou.png"];
        [bottomView addSubview:arrowView];
        [arrowView release];
        
        //文字
        UILabel * txtLbl = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 0.0, 200.0, 45.0)];
        txtLbl.backgroundColor = [UIColor clearColor];
        txtLbl.text = [strArr objectAtIndex:i];
        [bottomView addSubview:txtLbl];
        [txtLbl release];
        
        //按钮
        UIButton * but = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 300.0, 45.0)];
        but.backgroundColor = [UIColor clearColor];
        but.tag = 200+i;
        [but addTarget:self action:@selector(clickOnShareBut:) forControlEvents:UIControlEventTouchUpInside];
        [bottomView addSubview:but];
        [but release];

    }
}

#pragma mark - 点击分享按钮实现分享
-(void)clickOnShareBut:(id)sender{
    //曹工，请实现下面的内容
    int curTag = ((UIButton *)sender).tag - 200;
    switch (curTag) {
        case 0:
        {
            //腾讯微博分享
            [self performSelector:@selector(shareToQQTW) onThread:[NSThread mainThread] withObject:nil waitUntilDone:FALSE];
        }
            break;
        case 1:
        {
            //新浪微博分享
            if ([[WeiboAccounts shared] accounts].count == 0) {
                [_weiboSignIn signInOnViewController:self];
            }else{
                [self shareToSinaTW];
            }
        }
            break;
        case 2:
        {
            //短信分享
            MFMessageComposeViewController * vc = [[[MFMessageComposeViewController alloc] init] autorelease];
            vc.messageComposeDelegate = self;
            vc.title = @"分享智驾通给好友";
            vc.body = kShareShortMsg;
            [self presentModalViewController:vc animated:TRUE];
        }
            break;
        default:
            break;
    }
}

#pragma mark - 短信分享协议
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    //[self dismissSuccessMsg:@"短信分享发送成功."];

    [controller dismissModalViewControllerAnimated:TRUE];
}

#pragma mark - 微博分享
- (void)finishedWithAuth:(WeiboAuthentication *)auth error:(NSError *)error
{
    if (error) {
        NSLog(@"failed to auth: %@", error);
    }
    else {
        NSLog(@"Success to auth: %@", auth.userId);
        [[WeiboAccounts shared] addAccountWithAuthentication:auth];
        [self shareToSinaTW];
    }
}

- (void)shareToSinaTW{
    ComposeViewController *composeViewController = [[[ComposeViewController alloc]initWithNibName:@"ComposeViewController" bundle:nil]autorelease];
    composeViewController.statusText = [NSString stringWithFormat:@"#智驾通# %@:", @"我发现了一个好的代驾应用@智驾通,朋友聚会可以开怀畅饮"];
    [self presentViewController:composeViewController animated:YES completion:nil];
}

- (void)shareToQQTW{
    // 分享(自带登录功能)
    [weiboEngine UIBroadCastMsgWithContent:[NSString stringWithFormat:@"我发现了一个好的代驾应用，智驾通 %@:", @"简单易用"]
                                       andImage:nil
                                    parReserved:nil
                                       delegate:self
                                    onPostStart:@selector(postStart)
                                  onPostSuccess:@selector(createSuccess:)
                                  onPostFailure:@selector(createFail:)];
}

#pragma mark -
#pragma mark - creatSuccessOrFail

- (void)postStart {
    NSLog(@"%s", __FUNCTION__);
    //    [self showAlertMessage:@"开始发送"];
}

- (void)createSuccess:(NSDictionary *)dict {
    NSLog(@"%s %@", __FUNCTION__,dict);
    if ([[dict objectForKey:@"ret"] intValue] == 0) {
        //[self showAlertMessage:@"发送成功！"];
    }else {
        //[self showAlertMessage:@"发送失败！"];
    }
}

- (void)createFail:(NSError *)error {
    NSLog(@"error is %@",error);
    //[self showAlertMessage:@"发送失败！"];
}

#pragma mark -
#pragma mark 显示提醒框

- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框

- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}
@end
