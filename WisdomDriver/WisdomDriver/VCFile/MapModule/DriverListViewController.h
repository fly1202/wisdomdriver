//
//  DriverListViewController.h
//  WisdomDriver
//
//  Created by fei on 13-7-7.
//  Copyright (c) 2013年 idriven. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WantDriverVC.h"
@class NearDriverViewController;

@interface DriverListViewController : UITableViewController

@property (unsafe_unretained, nonatomic)NearDriverViewController* mFatherVC;
@property (nonatomic,assign) id<WantDriverVCDelegate> wantDriverVCDelegate;

- (void)fillDriverList:(NSArray*)driverList;

@end
