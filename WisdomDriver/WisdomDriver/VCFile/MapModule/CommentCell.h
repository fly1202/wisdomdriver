//
//  CommentCell.h
//  WisdomDriver
//
//  Created by fei on 13-7-8.
//  Copyright (c) 2013年 idriven. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentCell : UITableViewCell

- (void)fillDataFromDictionary:(NSDictionary*)dic;

@end
