//
//  MapViewController.h
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-8.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WantDriverVC.h"
@protocol MapViewControllerDelegate <NSObject>

-(void)UserWantEnterStandardVC;
-(void)UserWantEnterSettingVC;
-(void)UserWantEnterSearchVC;
-(void)UserWantEnterWantVC:(NSDictionary*)addrDict;

@end

@interface MapViewController : UIViewController{
    id <MapViewControllerDelegate> mapVcDelegate;
}

@property (nonatomic,assign) id <MapViewControllerDelegate> mapVcDelegate;
@property (nonatomic,assign) id<WantDriverVCDelegate> wantDriverVCDelegate;

- (void)addDriverOnTheMap:(NSArray*)driverList;
- (void)clearForlogin;
- (void)loginSuccess;
@end
