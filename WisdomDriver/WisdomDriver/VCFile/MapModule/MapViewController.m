//
//  MapViewController.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-8.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "MapViewController.h"
#import "BMapKit.h"
#import "defines.h"
#import "RunTimeShare.h"
#import "ClientJSONObject.h"
#import "SVProgressHUD.h"
#import "MapSearchVC.h"
#import "DriverAnnotation.h"
#import "DriverAnnotationView.h"

#define UPDATE_DRIVER_INTERVAL 60.0


@interface MapViewController () <BMKMapViewDelegate,BMKSearchDelegate,UIAlertViewDelegate>
{
    BMKMapView *            mapView;
    BMKSearch *             mkSearch;
    CLLocation *            lastUserLocation;
    
    NSMutableArray *        annotations;
    NSMutableArray *        drivers;
    NSMutableArray *        adsArray;
    UIScrollView *          adsScrollView;
    UIButton *              adsCloseBtn;
    
    UILabel *               tintLb;
    //UILabel *               usrLocLb;
    
    NSTimer *               driverUpdateTimer;
}

@end

@implementation MapViewController

@synthesize mapVcDelegate;

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    self.view.backgroundColor = [UIColor orangeColor];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    mapView.showsUserLocation = TRUE;
    [mapView setDelegate:self];
    
    if (!annotations) {
        annotations = [[NSMutableArray alloc] init];
    } else {
        [annotations removeAllObjects];
    }
    //更新司机位置信息
    [self upadteDriverTimer];
    [driverUpdateTimer invalidate];
    // enable timer after each 60s seconds for scrolling.
    driverUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:UPDATE_DRIVER_INTERVAL
                                                         target:self
                                                       selector:@selector(upadteDriverTimer)
                                                       userInfo:nil
                                                        repeats:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    LogInfo(@"");
    [super viewWillAppear:animated];
    
    
}

- (void)viewWillDisappear:(BOOL)animated{
    LogInfo(@"");
    [super viewWillDisappear:animated];
    mapView.showsUserLocation = FALSE;
    [mapView setDelegate:nil];
    
    [driverUpdateTimer invalidate];
    driverUpdateTimer = nil;
}

-(void)loginSuccess{
    LogInfo(@"");
    [self addMapView];
    
    [self addAdsView];
    
    [driverUpdateTimer invalidate];
    // enable timer after each 60s seconds for scrolling.
    driverUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:UPDATE_DRIVER_INTERVAL
                                                         target:self
                                                       selector:@selector(upadteDriverTimer)
                                                       userInfo:nil
                                                        repeats:YES];
}






#pragma mark ------------add by caoyanbo------------

#pragma mark - 广告视图相关
-(void)addAdsView{
    NSArray * arr = [[ClientJSONObject clientJSONObject] getADListWithAgentID:[RunTimeShare getAgentID]];
    adsArray = [[NSMutableArray alloc] initWithArray:arr];
    if (adsArray.count == 0) {
        NSMutableDictionary * dict1 = [NSMutableDictionary dictionaryWithCapacity:4];
        [dict1 setValue:@"http://down.tutu001.com/d/file/20100320/3f91a1d378aef410902e7ad601_560.jpg" forKey:@"adImage"];
        [dict1 setValue:@"http://www.apple.com.cn" forKey:@"adUrl"];
        [adsArray addObject:dict1];
        
        NSMutableDictionary * dict2 = [NSMutableDictionary dictionaryWithCapacity:4];
        [dict2 setValue:@"http://down.tutu001.com/d/file/20100320/3f91a1d378aef410902e7ad601_560.jpg" forKey:@"adImage"];
        [dict2 setValue:@"http://www.baidu.com" forKey:@"adUrl"];
        [adsArray addObject:dict2];
        
        NSMutableDictionary * dict3 = [NSMutableDictionary dictionaryWithCapacity:4];
        [dict3 setValue:@"http://down.tutu001.com/d/file/20100320/3f91a1d378aef410902e7ad601_560.jpg" forKey:@"adImage"];
        [dict3 setValue:@"http://www.apple.com.cn" forKey:@"adUrl"];
        [adsArray addObject:dict3];
        
        NSMutableDictionary * dict4 = [NSMutableDictionary dictionaryWithCapacity:4];
        [dict4 setValue:@"http://down.tutu001.com/d/file/20100320/3f91a1d378aef410902e7ad601_560.jpg" forKey:@"adImage"];
        [dict4 setValue:@"http://www.baidu.com" forKey:@"adUrl"];
        [adsArray addObject:dict4];
    }
    int picCount = adsArray.count;
    
    UIScrollView * helperScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(-8.0, 0, 336.0, 50.0)];
    helperScroll.contentSize = CGSizeMake(336.0*picCount, 50.0);
    helperScroll.pagingEnabled = YES;
    helperScroll.showsHorizontalScrollIndicator = NO;
    for (int i=1; i<=picCount; i++) {
        
        NSDictionary * adDict = [adsArray objectAtIndex:i-1];
        NSString * strUrl = [adDict valueForKey:@"adImage"];
        UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strUrl]]];
        
        UIImageView * helpView = [[UIImageView alloc] initWithFrame:CGRectMake(336.0*(i-1)+8.0, 0.0, 320.0, 50.0)];
        helpView.image = img;
        helpView.userInteractionEnabled = TRUE;
        //helpView.image = [UIImage imageNamed:[NSString stringWithFormat:@"pic_iad.png"]];
        [helperScroll addSubview:helpView];
        [helpView release];
        
        UIButton * adBtn = [[UIButton alloc] initWithFrame:CGRectMake(0,0,320,50)];
        adBtn.backgroundColor = [UIColor clearColor];
        [adBtn addTarget:self action:@selector(adButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [helpView addSubview:adBtn];
        adBtn.tag = i-1;
        [adBtn release];
    }
    
    [self.view addSubview:helperScroll];
    adsScrollView = helperScroll;
    [helperScroll release];
    
    //close button
    UIImageView * imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30.0, 30.0)];
    [imgView setImage:[UIImage imageNamed:@"btn_close.png"]];
    
    UIButton * enterBut = [[UIButton alloc] initWithFrame:CGRectMake(288.0, 58.0, 30.0, 30.0)];
    enterBut.backgroundColor = [UIColor clearColor];
    [enterBut addTarget:self action:@selector(closeAdsView) forControlEvents:UIControlEventTouchUpInside];
    [enterBut addSubview:imgView];
    [self.view addSubview:enterBut];
    adsCloseBtn = enterBut;
    [enterBut release];
    [imgView release];
    
    // enable timer after each 2 seconds for scrolling.
    [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
}

-(void)adButtonClick:(UIButton*)btn{
    NSDictionary * adDict = [adsArray objectAtIndex:btn.tag];
    NSString * url = [adDict valueForKey:@"adUrl"];
    
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]])
        NSLog(@"%@%@",@"Failed to open url:",[url description]);
}

-(void)closeAdsView{
    [adsScrollView removeFromSuperview];
    [adsCloseBtn removeFromSuperview];
    adsCloseBtn = nil;
    adsScrollView = nil;
}

- (void)scrollingTimer {
    // access the scroll view with the tag
    UIScrollView *scrMain = adsScrollView;
    
    // get the current offset ( which page is being displayed )
    CGFloat contentOffset = scrMain.contentOffset.x;
    // calculate next page to display
    int nextPage = (int)(contentOffset/scrMain.frame.size.width) + 1;
    int totalPage = (int)(scrMain.contentSize.width/scrMain.frame.size.width);
    // if page is not 10, display it
    if( nextPage!=totalPage )  {
        [scrMain scrollRectToVisible:CGRectMake(nextPage*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
        // else start sliding form 1 :)
    } else {
        [scrMain scrollRectToVisible:CGRectMake(0, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
    }
}

#pragma mark - 司机刷新
- (void)upadteDriverTimer{
    LogInfo(@"");
    return;
    
    if (!drivers) {
        drivers = [[NSMutableArray alloc] init];
    }
    
    NSArray * arr = [[ClientJSONObject clientJSONObject]
                     getDriverListWithlatitude:lastUserLocation.coordinate.latitude
                     longitude:lastUserLocation.coordinate.longitude];
    if (arr && arr.count > 1) {
        [drivers removeAllObjects];
        [drivers addObjectsFromArray:arr];
        
        if (drivers.count > 0) {
            if (!annotations) {
                annotations = [[NSMutableArray alloc] init];
            }
            
            for (id obj in annotations) {
                if ([obj isKindOfClass:[DriverAnnotation class]]) {
                    [annotations removeObject:obj];
                }
            }
            
            for (int i = 0; i < drivers.count; i++) {
                DriverAnnotation * anno = [[DriverAnnotation alloc] initWithDriverDict:[drivers objectAtIndex:i]];
                [annotations addObject:anno];
            }
            [self performSelectorOnMainThread:@selector(addAnnotations:) withObject:annotations waitUntilDone:true];
        }
    }
}

#pragma mark - 添加地图视图
- (void)addMapView{
    
    if (mkSearch) {
        [mkSearch release];
        mkSearch = nil;
    }
    mkSearch = [[BMKSearch alloc] init];
	mkSearch.delegate = self;
    
    if (mapView) {
        return;
    }
    mapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
    mapView.backgroundColor  = [UIColor orangeColor];
    mapView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [mapView setMapType:BMKMapTypeStandard];
    
    //显示用户当前的坐标，打开地图有相应的提示
    mapView.showsUserLocation=YES;
    
    //设置地图的代理
    mapView.delegate=self;
    
    [self.view addSubview:mapView];
    
    //搜索按钮
    UIButton * schBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //[schBtn setImage:[UIImage imageNamed:@"soso_btn1.png"] forState:UIControlStateNormal];
    [schBtn setBackgroundImage:[UIImage imageNamed:@"soso_btn1.png"] forState:UIControlStateNormal];
    [schBtn setFrame:CGRectMake(255, 245.0, 60.0, 50.0)];
    [schBtn setBackgroundColor:[UIColor clearColor]];
    [schBtn addTarget:self action:@selector(searchBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view insertSubview:schBtn aboveSubview:mapView];
    
    //定位按钮
    UIButton * locateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [locateBtn setImage:[UIImage imageNamed:@"dingwei_btn1.png"] forState:UIControlStateNormal];
    [locateBtn setFrame:CGRectMake(255, 300.0, 60.0, 50.0)];
    [locateBtn setBackgroundColor:[UIColor clearColor]];
    [locateBtn addTarget:self action:@selector(showUserLocation) forControlEvents:UIControlEventTouchUpInside];
    [self.view insertSubview:locateBtn aboveSubview:mapView];
    
    //添加长按扑捉
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1.0; //user needs to press for 2 seconds
    [mapView addGestureRecognizer:lpgr];
    [lpgr release];
    
    UILabel * bgLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 381, 320, 30)];
    bgLb.backgroundColor = [[UIColor alloc] initWithRed:86.0/255.0 green:86/255.0 blue:86/255.0 alpha:0.5];
    [self.view addSubview:bgLb];
    [bgLb release];
    
    tintLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 310, 30)];
    tintLb.font = [UIFont systemFontOfSize:12.0];
    tintLb.textColor = [UIColor whiteColor];
    tintLb.text = @"长按地图以选取地址";
    tintLb.backgroundColor = [UIColor clearColor];
    
    [bgLb addSubview:tintLb];
    
}

#pragma mark - 长按处理
- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:mapView];
    CLLocationCoordinate2D touchMapCoordinate =
    [mapView convertPoint:touchPoint toCoordinateFromView:mapView];
    
    [self showWithStatusMsg:@"正在解析地址中"];
    //当另外的VC使用baiduMAP后需要重新设定delegate，否则会使用另外一个实例的delegate方法来响应
    mkSearch.delegate = self;
    mapView.delegate = self;
    [mkSearch reverseGeocode:touchMapCoordinate];
}

#pragma mark - 搜索按钮
- (void)searchBtnClicked{
    
    [mapVcDelegate UserWantEnterSearchVC];
}

#pragma mark - 搜索代理
-(void)didSelectAddr:(BMKPoiInfo*)addr{
    LogInfo(@"didSelectAddr:%@ location:%f %f",addr.address,addr.pt.latitude,addr.pt.longitude);
    mapView.delegate = self;
    mkSearch.delegate = self;
    
    if (annotations) {
        [mapView removeAnnotations:mapView.annotations];
        [annotations removeAllObjects];
    }else{
        annotations = [[NSMutableArray alloc] init];
    }
    
    //新地点
    BMKPointAnnotation* annotation = [[BMKPointAnnotation alloc]init];
    annotation.coordinate = addr.pt;
    annotation.title = addr.address;
    [annotations addObject:[annotation autorelease]];
    [self performSelectorOnMainThread:@selector(updateAddrAnnotation) withObject:nil waitUntilDone:true];
    
    CLLocationCoordinate2D startCoord = addr.pt;
    // the distance should be from driver to user
    BMKCoordinateRegion adjustedRegion = [mapView regionThatFits:BMKCoordinateRegionMakeWithDistance(startCoord, SPAN_DISTANCE_WIDTH, SPAN_DISTANCE_HEIGHT)];
    LogInfo(@"deltalong:%f deltalang:%f",adjustedRegion.span.longitudeDelta,adjustedRegion.span.latitudeDelta);
    [mapView setRegion:adjustedRegion animated:YES];
}

#pragma mark - 定位按钮
- (void)showUserLocation{
    NSLog(@"showUserLocation");
    //刷新位置后再设置地图位置
    mapView.showsUserLocation = TRUE;
    [lastUserLocation release];
    lastUserLocation = [[CLLocation alloc] initWithLatitude:mapView.userLocation.coordinate.latitude
                                                  longitude:mapView.userLocation.coordinate.longitude];
    [[RunTimeShare sharedInstance] setCurUserLocation:lastUserLocation];
    //[self moveToUserLocation];
    
    CLLocationCoordinate2D startCoord = mapView.userLocation.coordinate;
    // the distance should be from driver to user
    BMKCoordinateRegion adjustedRegion = [mapView regionThatFits:BMKCoordinateRegionMakeWithDistance(startCoord, 2*1000, 2*1000)];
    [mapView setRegion:adjustedRegion animated:YES];
    
    //[self showWithStatusMsg:@"正在解析地址"];
    [mkSearch reverseGeocode:lastUserLocation.coordinate];
}

- (void)choseLocForDrive:(id)sender {
    NSLog(@"choseLocForDrive: called");
    //MapAnnotation *annotation = [[self.map selectedAnnotations] objectAtIndex:0];
    //push driver detail controller
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"是否选择此地址为代驾地址？" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
    //alert.tag = versonAlertTag;
    [alert show];
    [alert release];
}

- (void)moveToUserLocation{
    CLLocationCoordinate2D startCoord = mapView.userLocation.coordinate;
    // the distance should be from driver to user
    BMKCoordinateRegion adjustedRegion = [mapView regionThatFits:BMKCoordinateRegionMakeWithDistance(startCoord, SPAN_DISTANCE_WIDTH, SPAN_DISTANCE_HEIGHT)];
    [mapView setRegion:adjustedRegion animated:YES];
}

#pragma mark - 有关提醒框的协议处理
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    //处理页面跳转
    if (buttonIndex == 0) {
        BMKPointAnnotation * anno = [annotations objectAtIndex:(annotations.count-1)];
        CLLocation * loc = [[CLLocation alloc] initWithLatitude:anno.coordinate.latitude longitude:anno.coordinate.longitude];
        NSDictionary * dict = [RunTimeShare makeAddrDict:anno.title location:loc];
        [mapVcDelegate UserWantEnterWantVC:dict];
    }
}

#pragma mark -
#pragma mark BMapViewDelegate implementation
- (BMKAnnotationView *)mapView:(BMKMapView *)mapViewObj viewForAnnotation:(id <BMKAnnotation>)annotation {
    NSLog(@"mapView:viewForAnnotation:");
    BMKAnnotationView * annotationView = nil;
    
    if ([annotation isKindOfClass:[BMKUserLocation class]]){
        return annotationView;
    }else if ([annotation isKindOfClass:[BMKPointAnnotation class]]) {
        BMKPinAnnotationView *pinAnnotationView = (BMKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"mapAnnotation"];
        
        if (pinAnnotationView) {
            [pinAnnotationView prepareForReuse];
            pinAnnotationView.annotation = annotation;
        } else {
            pinAnnotationView = [[[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"mapAnnotation"] autorelease];
        }
        
        pinAnnotationView.canShowCallout = YES;
        
        UIButton *disclosureButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        pinAnnotationView.rightCalloutAccessoryView = disclosureButton;
        [disclosureButton addTarget:self
                             action:@selector(choseLocForDrive:)
                   forControlEvents:UIControlEventTouchUpInside];
        annotationView = pinAnnotationView;
        
    } else if ([annotation isKindOfClass:[DriverAnnotation class]]) {
        DriverAnnotationView *driverView = (DriverAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"driverAnnotation"];
        
        if (driverView) {
            [driverView prepareForReuse];
            driverView.annotation = annotation;
        } else {
            driverView = [[[DriverAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"driverAnnotation"] autorelease];
        }
        
        driverView.canShowCallout = NO;
        
        annotationView = driverView;
    }
    
    return annotationView;
}

- (void)mapView:(BMKMapView *)amapView didAddAnnotationViews:(NSArray *)views{
    NSLog(@"didAddAnnotationViews");
//    for (id<BMKAnnotation> currentAnnotation in mapView.annotations) {
//        [mapView selectAnnotation:currentAnnotation animated:YES];
//    }
}

- (void)mapView:(BMKMapView *)mapView didSelectAnnotationView:(BMKAnnotationView *)view{
    NSLog(@"annotation touched: %@", view.annotation.title);
    DriverAnnotation *annot = (DriverAnnotation *)view.annotation;
    [_wantDriverVCDelegate enterDriverInfoVC:annot.driverDict];
}

- (void)mapView:(BMKMapView *)mapViewObj didUpdateUserLocation:(BMKUserLocation *)userLocation{
    //LogInfo(@"didUpdateUserLocation:%@",[userLocation description]);
    if (lastUserLocation == nil) {
        lastUserLocation = [[CLLocation alloc] initWithLatitude:mapView.userLocation.coordinate.latitude
                                                      longitude:mapView.userLocation.coordinate.longitude];
        [[RunTimeShare sharedInstance] setCurUserLocation:lastUserLocation];
        if ([SVProgressHUD isVisible]) {
            [self dismiss];
        }
        
        CLLocationCoordinate2D startCoord = mapView.userLocation.coordinate;
        // the distance should be from driver to user
        BMKCoordinateRegion adjustedRegion = [mapView regionThatFits:BMKCoordinateRegionMakeWithDistance(startCoord, SPAN_DISTANCE_WIDTH, SPAN_DISTANCE_HEIGHT)];
        [mapView setRegion:adjustedRegion animated:YES];
        
        [self showWithStatusMsg:@"正在解析地址"];
        //获取司机列表
        if (!drivers) {
            drivers = [[NSMutableArray alloc] init];
        }else{
            [drivers removeAllObjects];
        }
        
        //test coordinate,lon=120.221179&lat=30.209811
        for(int i = 5; i < 5;i++){
            
            NSMutableDictionary * dict = [NSMutableDictionary dictionary];
            [dict setValue:[NSString stringWithFormat:@"司机%d",i] forKey:kDriverName];
            [dict setValue:@"123456" forKey:kDriverTel];
            [dict setValue:(i%2 == 0) ? @"空闲" : @"忙" forKey:kDriverStatus];
            [dict setValue:[NSString stringWithFormat:@"22.63%d",i] forKey:kDriverLatitude];
            [dict setValue:[NSString stringWithFormat:@"114.07%d",i] forKey:kDriverLongitude];
            [drivers addObject:dict];
        }
        
        NSArray * arr = [[ClientJSONObject clientJSONObject]
                         getDriverListWithlatitude:lastUserLocation.coordinate.latitude
                         longitude:lastUserLocation.coordinate.longitude];
        if (arr && arr.count > 1) {
            [drivers addObjectsFromArray:arr];
        }
        
        if (drivers.count > 0) {
            if (!annotations) {
                annotations = [[NSMutableArray alloc] init];
            }
            for (int i = 0; i < drivers.count; i++) {
                DriverAnnotation * anno = [[DriverAnnotation alloc] initWithDriverDict:[drivers objectAtIndex:i]];
                [annotations addObject:anno];
            }
            [self performSelectorOnMainThread:@selector(addAnnotations:) withObject:annotations waitUntilDone:true];
        }
        
        dispatch_async (dispatch_get_main_queue(), ^{
            
            
        });
        
        //解析当前位置
        [mkSearch reverseGeocode:lastUserLocation.coordinate];
    }else{
        //停止更新用户位置
        //mapView.showsUserLocation = FALSE;
    }
}

- (void)addAnnotations:(NSArray *)annos{
    [mapView addAnnotations:annotations];
}

- (void)mapViewDidFinishLoadingMap:(BMKMapView *)mapView{
    NSLog(@"mapViewDidFinishLoadingMap:");
}

- (void)mapViewDidFailLoadingMap:(BMKMapView *)mapView withError:(NSError *)error{
    NSLog(@"mapViewDidFailLoadingMap:%@",[error description]);
}

#pragma - mark BMKSearchDelegate
- (void)onGetAddrResult:(BMKAddrInfo*)result errorCode:(int)error{
    
    if ([SVProgressHUD isVisible]) {
        [self dismiss];
    }
	// 在此处添加您对反地理编码结果的处理
    NSLog(@"onGetAddrResult:lat%f,long%f userLoc:lat%f,long%f",result.geoPt.latitude,result.geoPt.longitude,lastUserLocation.coordinate.latitude,lastUserLocation.coordinate.longitude);
    if (fabs(result.geoPt.latitude - lastUserLocation.coordinate.latitude) < 0.00005 &&
        fabs(result.geoPt.longitude == lastUserLocation.coordinate.longitude) < 0.00005) {
        //用户位置
        NSLog(@"用户位置解析成功");
        [RunTimeShare sharedInstance].curAddrInfo = result;
        [RunTimeShare sharedInstance].curLocationString =
        [NSString stringWithFormat:@"%@",result.strAddr];
        
        //usrLocLb.text = [NSString stringWithFormat:@"您的当前位置是：%@",result.strAddr];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotifyUpdateLocation object:nil];
    }else{
        //添加选择地址大头针
        NSLog(@"拾取位置解析成功");
        
        [mapView removeAnnotations:annotations];
        
        if (annotations) {
            [annotations removeAllObjects];
        }else{
            annotations = [[NSMutableArray alloc] init];
        }
        
        for (int i = 0; i < drivers.count; i++) {
            DriverAnnotation * anno = [[DriverAnnotation alloc] initWithDriverDict:[drivers objectAtIndex:i]];
            [annotations addObject:anno];
        }
        
        //新地点
        BMKPointAnnotation* annotation = [[BMKPointAnnotation alloc] init];
        annotation.coordinate = result.geoPt;
        annotation.title = result.strAddr;
        [annotations addObject:annotation];
        [self performSelectorOnMainThread:@selector(updateAddrAnnotation) withObject:nil waitUntilDone:true];
        [annotation release];
    }
}

-(void)updateAddrAnnotation{
    [mapView addAnnotations:annotations];
}

- (void)addDriverOnTheMap:(NSArray*)driverList{
    [self addMapView];
    
    
    for (int i = 0; i < driverList.count; i++) {
        DriverAnnotation * anno = [[DriverAnnotation alloc] initWithDriverDict:[driverList objectAtIndex:i]];
        //[annotations addObject:anno];
        [mapView addAnnotation:anno];
    }
    //[self addAnnotations:annotations];
//    [self performSelectorOnMainThread:@selector(addAnnotations:) withObject:annotations waitUntilDone:true];
}

#pragma mark - 清理页面为重新登陆做准备
-(void)clearForlogin{
    if (mapView && annotations) {
        [mapView removeAnnotations:annotations];
        [annotations removeAllObjects];
        [self showUserLocation];
    }
}

#pragma mark -
#pragma mark 显示提醒框
- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框
- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}
@end
