//
//  DriverInfoViewController.m
//  WisdomDriver
//
//  Created by lifei on 13-7-8.
//  Copyright (c) 2013年 idriven. All rights reserved.
//

#import "DriverInfoViewController.h"
#import "ClientJSONObject.h"
#import "DriverListCell.h"
#import "CommentCell.h"

@interface DriverInfoViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic)NSMutableArray*    mCommentList;


@property (retain, nonatomic) IBOutlet UIView *mViewHeader;
@property (retain, nonatomic) IBOutlet UITableView *mTableView;

@end

@implementation DriverInfoViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    DriverListCell *cell = [[DriverListCell alloc] init];
    cell.accessoryType = UITableViewCellAccessoryNone;
    [cell fillDataFromDictionary:_mdicDriver];
    [self.mViewHeader addSubview:cell];
    [self addTopViewWithTitle:@"司机信息"];
    
    CommentCell *cell1 = [[CommentCell alloc] init];
    self.mTableView.rowHeight = CGRectGetHeight(cell1.frame);
}

#pragma mark - 添加顶部条栏和文字
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //返回按钮
    UIButton * backBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 58.0, 30.0)];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [backBut setTitle:@"返回" forState:UIControlStateNormal];
    [backBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBut addTarget:self action:@selector(clickOnBackBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBut];
    [backBut release];
    
}

-(void)clickOnBackBut{    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 10;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"用户评价";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[CommentCell alloc] init];
    }
    // Configure the cell...
    //[cell fillDataFromDictionary:nil];
    return cell;
}

- (void)dealloc {
    [_mViewHeader release];
    [_mTableView release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setMViewHeader:nil];
    [self setMTableView:nil];
    [super viewDidUnload];
}
@end
