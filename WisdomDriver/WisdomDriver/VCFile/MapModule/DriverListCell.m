//
//  DriverListCell.m
//  WisdomDriver
//
//  Created by fei on 13-7-7.
//  Copyright (c) 2013年 idriven. All rights reserved.
//

#import "DriverListCell.h"

@implementation DriverListCell

- (id)init{
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    self = [nibs lastObject];
    if (self) {
        
    }
    return self;
}

- (void)fillDataFromDictionary:(NSDictionary*)dic{
    if (![dic isKindOfClass:[NSDictionary class]]) {
        return;
    }
    _mLabDriverName.text = [dic objectForKey:@"driverName"];
    _mLabDriverTelPhone.text = [dic objectForKey:@"driverTel"];
    _mLabAgentName.text = [dic objectForKey:@"agentName"];
    _mLabAgentTelphone.text = [dic objectForKey:@"agentTel"];
    _mLabStatus.text = [dic objectForKey:@"driverStatus"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_mLabDistance release];
    [_mLabAgentName release];
    [_mLabAgentTelphone release];
    [_mLabStatus release];
    [_mLabDriverName release];
    [super dealloc];
}
@end
