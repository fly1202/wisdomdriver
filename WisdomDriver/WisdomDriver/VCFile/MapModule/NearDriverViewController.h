//
//  NearDriverViewController.h
//  WisdomDriver
//
//  Created by fei on 13-7-7.
//  Copyright (c) 2013年 idriven. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WantDriverVC.h"
#import "MapViewController.h"
#import "DriverListViewController.h"

@interface NearDriverViewController : UIViewController<MapViewControllerDelegate,WantDriverVCDelegate>

@property(assign,nonatomic)NSInteger   mCurrentIndex;
@property (nonatomic,assign) id <MapViewControllerDelegate> mapVcDelegate;
@property(strong, nonatomic)MapViewController       *mMapVC;
@property(strong, nonatomic)DriverListViewController  *mDriversVC;
@property (nonatomic,assign) id<WantDriverVCDelegate> wantDriverVCDelegate;

@end
