//
//  UIImage+Stretchable.h
//  meiliyue
//
//  Created by sjxu on 12-11-23.
//  Copyright (c) 2012年 sjxu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Stretchable)

- (UIImage*) getStretchableImage;
+ (UIImage *) getImageFromBundle:(NSString *)strImgName;
@end
