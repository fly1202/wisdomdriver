//
//  DriverAnnotation.m
//  WisdomDriver
//
//  Created by  apple on 13-2-3.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "DriverAnnotation.h"
#import "BMapKit.h"
#import "defines.h"

@implementation DriverAnnotation {
}

@synthesize driverDict,coordinate;

-(id)initWithDriverDict:(NSDictionary*)dict{
    self = [super init];
    if (self) {
        self.driverDict = dict;
        self.coordinate = CLLocationCoordinate2DMake([[dict valueForKey:kDriverLatitude] doubleValue],
                                                     [[dict valueForKey:kDriverLongitude] doubleValue]);
        self.title = [dict valueForKey:kDriverName];
        self.subtitle = [dict valueForKey:kDriverTel];
        self.driverDict = dict;
    }
    return self;
}

@end
