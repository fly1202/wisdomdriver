//
//  NearDriverViewController.m
//  WisdomDriver
//
//  Created by fei on 13-7-7.
//  Copyright (c) 2013年 idriven. All rights reserved.
//

#import "NearDriverViewController.h"
#import "CustomSegmentView.h"
#import "DriverListViewController.h"
#import "MapViewController.h"
#import "ClientJSONObject.h"


#define COLOR(R,G,B)        [UIColor colorWithRed:R/255.0f green:G/255.0f blue:B/255.0f alpha:1]
#define kChildViewFrame  CGRectMake(0, 47, 320, 460)


@interface NearDriverViewController ()<CustomSegmentViewDelegate,MapViewControllerDelegate>
@property (strong, nonatomic)NSMutableArray*    mDriverList;

@property (assign,nonatomic)UIViewController         *mCurrentView;
@property (retain, nonatomic) IBOutlet CustomSegmentView *mCustomSegment;

@end

@implementation NearDriverViewController


-(void)viewDidAppear:(BOOL)animated{
    if (_mCurrentIndex == 1) {
        [_mMapVC viewDidAppear:animated];
    }else{
        [_mDriversVC viewDidAppear:animated];
    }
}
- (void)viewWillAppear:(BOOL)animated{
    if (_mCurrentIndex == 1) {
        [_mMapVC viewWillAppear:animated];
    }else{
        [_mDriversVC viewWillAppear:animated];
    }
}

- (void)getDriverList{
    NSArray *arry = [[ClientJSONObject clientJSONObject] getNearestDriverList:@"" Withlatitude:30 Withlongitude:120 withDriverNumber:-1 withDirverStatus:3];
    self.mDriverList =  [NSMutableArray arrayWithArray:arry];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getDriverList];
    if (self.mMapVC == nil) {
        self.mMapVC = [[MapViewController alloc] init];
        self.mMapVC.view.frame = kChildViewFrame;
        self.mMapVC.wantDriverVCDelegate = _wantDriverVCDelegate;
        [_mMapVC addDriverOnTheMap:_mDriverList];
    }
    if (self.mDriversVC == nil) {
        self.mDriversVC = [[DriverListViewController
                            alloc] init];
        self.mDriversVC.mFatherVC = self;
        NSLog(@"_wantDriverVCDelegate = %@",_wantDriverVCDelegate);
        self.mDriversVC.wantDriverVCDelegate = _wantDriverVCDelegate;
        [_mDriversVC fillDriverList:_mDriverList];
        self.mDriversVC.view.frame = kChildViewFrame;
        //self.mDriversVC.mMyBoxVC = self;
    }
    [self.view addSubview:_mMapVC.view];
    _mCurrentView = _mMapVC;
    _mCurrentIndex = 1;
    [self initNavSegment];
    
    [self addTopViewWithTitle:@"智代驾"];

    // Do any additional setup after loading the view from its nib.
}
#pragma mark - 添加顶部条栏和文字,收费标准,设置
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    CGRect nRect = _mCustomSegment.frame;
    nRect.origin.x = (320 - CGRectGetWidth(nRect)) / 2.0f;
    nRect.origin.y = (47 - CGRectGetHeight(nRect)) / 2.0f;
    _mCustomSegment.frame = nRect;
    
    [self.view addSubview:_mCustomSegment];
    
    //收费标准
    UIButton *standardBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 64.0, 30.0)];
    [standardBut setBackgroundImage:[UIImage imageNamed:@"btn_shoufeidefault1.png"] forState:UIControlStateNormal];
    [standardBut setBackgroundImage:[UIImage imageNamed:@"btn_shoufeiselected2.png"] forState:UIControlStateSelected];
    [standardBut setTitle:@"收费标准" forState:UIControlStateNormal];
    [standardBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [standardBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [standardBut addTarget:self action:@selector(clickOnStandardBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:standardBut];
    [standardBut release];
    
    //设置按钮
    UIButton * settingBut = [[UIButton alloc] initWithFrame:CGRectMake(268.0, 8.0, 46.0, 30.0)];
    [settingBut setBackgroundImage:[UIImage imageNamed:@"btn_settingup.png"] forState:UIControlStateNormal];
    [settingBut setBackgroundImage:[UIImage imageNamed:@"btn_settingdown.png"] forState:UIControlStateSelected];
    [settingBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [settingBut addTarget:self action:@selector(clickOnSettingBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:settingBut];
    [settingBut release];
}

- (void)initNavSegment{
    self.mCustomSegment.mDelegate = self;
    self.mCustomSegment.mNormalTextColor = [UIColor whiteColor];
    self.mCustomSegment.mHightlightedTextColor = [UIColor whiteColor];
    self.mCustomSegment.mNormalShadowColor = COLOR(19, 104, 139);
    self.mCustomSegment.mNormalShadowOffset = CGSizeMake(0 , -1);
    self.mCustomSegment.mSelectIndex = _mCurrentIndex;
}


#pragma mark - action

#pragma mark - 进入收费标准界面
-(void)clickOnStandardBut{
    [self.mapVcDelegate UserWantEnterStandardVC];
}

#pragma mark - 进入设置界面
-(void)clickOnSettingBut{
    [self.mapVcDelegate UserWantEnterSettingVC];
}

-(void)segmentSelected:(id)sender {
    CustomSegmentView *seg = (CustomSegmentView *)sender;
    if (_mCurrentIndex == seg.mSelectIndex) {
        //return;
    }
    
    
    switch (seg.mSelectIndex) {
        case 0:
            _mDriversVC.view.alpha = 0;
            [self changeChildViewController:_mDriversVC];
            
            break;
        case 1:
            _mMapVC.view.alpha = 0;
            [self changeChildViewController:_mMapVC];
            break;
        default:
            break;
    }
    _mCurrentIndex = seg.mSelectIndex;
    
}

- (void)changeChildViewController:(UIViewController *)childVC{
    [_mCurrentView.view removeFromSuperview];
    [self.view addSubview:childVC.view];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5f];
    [childVC.view setAlpha:1.0];
    [UIView commitAnimations];
    
    _mCurrentView = childVC;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
