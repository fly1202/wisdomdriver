//
//  CommentCell.m
//  WisdomDriver
//
//  Created by fei on 13-7-8.
//  Copyright (c) 2013年 idriven. All rights reserved.
//

#import "CommentCell.h"

@implementation CommentCell

- (id)init{
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    self = [nibs lastObject];
    if (self) {
        
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)fillDataFromDictionary:(NSDictionary*)dic{
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
