//
//  DriverAnnotationView.h
//  WisdomDriver
//
//  Created by  apple on 13-2-3.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "BMKAnnotationView.h"
#import "BMapKit.h"

@interface DriverAnnotationView : BMKAnnotationView
- (id)initWithAnnotation:(id<BMKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier;
@end
