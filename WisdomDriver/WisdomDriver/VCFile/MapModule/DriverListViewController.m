//
//  DriverListViewController.m
//  WisdomDriver
//
//  Created by fei on 13-7-7.
//  Copyright (c) 2013年 idriven. All rights reserved.
//

#import "DriverListViewController.h"
#import "DriverInfoViewController.h"
#import "NearDriverViewController.h"
#import "ClientJSONObject.h"
#import "DriverListCell.h"

@interface DriverListViewController ()

@property (strong, nonatomic)NSArray*    mDriverList;

@end

@implementation DriverListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    DriverListCell *cell = [[DriverListCell alloc] init];
    self.tableView.rowHeight = CGRectGetHeight(cell.frame);
    // Do any additional setup after loading the view from its nib.
}

- (void)fillDriverList:(NSArray*)driverList{
    self.mDriverList = driverList;
    [self.tableView reloadData];

}

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    // Return the number of rows in the section.
    return [self.mDriverList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    DriverListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[DriverListCell alloc] init];
    }
    // Configure the cell...
    [cell fillDataFromDictionary:[self.mDriverList objectAtIndex:indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dicDriver = [self.mDriverList objectAtIndex:indexPath.row];
    NSLog(@"%@",_wantDriverVCDelegate);
    [_wantDriverVCDelegate enterDriverInfoVC:dicDriver];
    //[self.mFatherVC.navigationController pushViewController:driverInfo animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
