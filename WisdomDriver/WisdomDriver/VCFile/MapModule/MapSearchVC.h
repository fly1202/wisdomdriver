//
//  MapSearchVC.h
//  WisdomDriver
//
//  Created by  apple on 13-1-27.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BMapKit.h"

@protocol MapSearchVCDelegate <NSObject>

-(void)didSelectAddr:(BMKPoiInfo*)addr;

@end

@interface MapSearchVC : UIViewController{
    id<MapSearchVCDelegate> vcDelegate;
}
@property (nonatomic, assign) id<MapSearchVCDelegate> vcDelegate;
@end
