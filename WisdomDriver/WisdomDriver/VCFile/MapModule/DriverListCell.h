//
//  DriverListCell.h
//  WisdomDriver
//
//  Created by fei on 13-7-7.
//  Copyright (c) 2013年 idriven. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DriverListCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *mLabDriverTelPhone;
@property (retain, nonatomic) IBOutlet UILabel *mLabDriverName;
@property (retain, nonatomic) IBOutlet UILabel *mLabDistance;
@property (retain, nonatomic) IBOutlet UILabel *mLabAgentName;
@property (retain, nonatomic) IBOutlet UILabel *mLabAgentTelphone;
@property (retain, nonatomic) IBOutlet UILabel *mLabStatus;


- (void)fillDataFromDictionary:(NSDictionary*)dic;

@end
