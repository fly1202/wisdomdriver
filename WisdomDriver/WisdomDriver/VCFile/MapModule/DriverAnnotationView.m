//
//  DriverAnnotationView.m
//  WisdomDriver
//
//  Created by  apple on 13-2-3.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "DriverAnnotationView.h"
#import "DriverAnnotation.h"
#import "defines.h"

@implementation DriverAnnotationView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithAnnotation:(id<BMKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        //司机状态（空闲，以确认，代驾中）
        //BOOL bFlag = [[[(DriverAnnotation*)annotation driverDict] valueForKey:kDriverStatus] isEqualToString:@"空闲"];
        NSString * status = [[(DriverAnnotation*)annotation driverDict] valueForKey:kDriverStatus];
        UIImage *blipImage = nil;
        if ([status isEqualToString:@"空闲"]) {
            blipImage = [UIImage imageNamed:@"acc_sijil.png"];
        }else if ([status isEqualToString:@"已确认"]){
            blipImage = [UIImage imageNamed:@"acc_sijijingxi.png"];
        }else{
            blipImage = [UIImage imageNamed:@"acc_sijilred.png"];
        }
        
        UIImageView * imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 55, 30)];
        imgView.image = blipImage;
        [self addSubview:imgView];
        [imgView release];
        
        CGRect frame = [self frame];
        frame.size =  CGSizeMake([blipImage size].width, [blipImage size].height);
        [self setFrame:frame];
        [self setCenterOffset:CGPointMake(0.0, -7.0)];
        //[self setImage:blipImage];
        
        UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 55, 24)];
        lb.backgroundColor = [UIColor clearColor];
        lb.textColor = [UIColor whiteColor];
        lb.textAlignment = UITextAlignmentCenter;
        lb.font = [UIFont systemFontOfSize:12.0];
        lb.text = [[(DriverAnnotation*)annotation driverDict] valueForKey:kDriverName];
        [imgView addSubview:lb];
    }
    return self;
}

@end
