//
//  StandardVC.h
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-8.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StandardVCDelegate <NSObject>

-(void)StandardVcDidDismiss;

@end

@interface StandardVC : UIViewController{
    id <StandardVCDelegate> standarVcDelegate;
}

@property (nonatomic,assign) id <StandardVCDelegate> standarVcDelegate;

@end
