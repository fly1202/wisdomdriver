//
//  MapSearchVC.m
//  WisdomDriver
//
//  Created by  apple on 13-1-27.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "MapSearchVC.h"
#import "SVProgressHUD.h"
#import "RunTimeShare.h"

#define kTableHeightSearch 150
#define kTableHeightResult 377

#define kTableTagSearch 100
#define kTableTagResult 101

@interface MapSearchVC ()<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,BMKSearchDelegate>{
    NSMutableArray *tableData;
    
    UITableView *theTableView;
    UISearchBar *theSearchBar;
}

@end

@implementation MapSearchVC
@synthesize vcDelegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    [self addTopViewWithTitle:@"地址搜索"];
    
    tableData = [[NSMutableArray alloc] init];
    
    [self addSearchBar];
    [self addTableView];
}

#pragma mark - 添加顶部条栏和文字,收费标准,设置
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //返回按钮
    UIButton * backBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 58.0, 30.0)];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [backBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [backBut setTitle:@"返回" forState:UIControlStateNormal];
    backBut.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBut addTarget:self action:@selector(clickOnBackBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBut];
    [backBut release];
}

-(void)clickOnBackBut{
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)viewDidAppear:(BOOL)animated {
    [theSearchBar becomeFirstResponder];
    [super viewDidAppear:animated];
}

-(void)addTableView{
    theTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 92, 320, kTableHeightSearch) style:UITableViewStylePlain];
    theTableView.delegate = self;
    theTableView.dataSource = self;
    theTableView.tag = kTableTagSearch;
    [self.view addSubview:theTableView];
}

-(void)addSearchBar{
    theSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 45, 320, 47)];
    theSearchBar.delegate = self;
    theSearchBar.showsCancelButton = YES;
    theSearchBar.barStyle = UIBarStyleBlackTranslucent;
    theSearchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    theSearchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
    theSearchBar.placeholder = @"请输入地名";
    theSearchBar.keyboardType =  UIKeyboardTypeDefault;
    //为UISearchBar添加背景图片
    UIView *segment = [theSearchBar.subviews objectAtIndex:0];
    UIImageView *bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Images/search_bar_bg.png"]];
    [segment addSubview: bgImage];
    //<---背景图片
    [self.view addSubview:theSearchBar];
    [theSearchBar becomeFirstResponder];
}

#pragma mark -
#pragma mark - UISearchBarDelegate
// called when keyboard search button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)thesearchBar
{
    NSLog(@"searchBarSearchButtonClicked");
    
    NSString * keyWord = [NSString stringWithString:thesearchBar.text];
    NSString * city = [RunTimeShare sharedInstance].curAddrInfo.addressComponent.city;
    BMKSearch * mkSearch = [[BMKSearch alloc] init];
    mkSearch.delegate = self;
    /*
    //中心位置的经纬度：东经104°21′15〃，北纬28°42′30〃  宝安 113.940334&lat=22.563140
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(22.563140,113.940334);
    [mkSearch poiMultiSearchNearBy:[keyWord componentsSeparatedByString:@" "]
                            center:center
                            radius:400*1000
                         pageIndex:0];
    */
    [mkSearch poiSearchInCity:city withKey:keyWord pageIndex:0];
    
    [thesearchBar resignFirstResponder];
    
    [[RunTimeShare sharedInstance] addSearchKey:theSearchBar.text];
    [self showWithStatusMsg:@"正在搜索中，请稍后..."];
    
}

// called when cancel button pressed
- (void)searchBarCancelButtonClicked:(UISearchBar *)thesearchBar{
    [self clickOnBackBut];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    theTableView.tag = kTableTagSearch;
    
    [tableData removeAllObjects];
    [tableData addObjectsFromArray:[[RunTimeShare sharedInstance] getSearchKeyArray]];
    
    CGRect frame = theTableView.frame;
    frame.size.height = kTableHeightSearch;
    theTableView.frame = frame;
    [theTableView reloadData];
    return TRUE;
}
#pragma - mark BMKSearchDelegate
- (void)onGetPoiResult:(NSArray*)poiResultList searchType:(int)type errorCode:(int)error{
    //BMKTypePoiList,BMKTypeAreaPoiList,BMKAreaMultiPoiList
    NSLog(@"onGetPoiResult:searchType:errorCode:  %d addr found",poiResultList.count);
    [self dismiss];
    
    [tableData removeAllObjects];
    if (error == BMKErrorOk) {
		BMKPoiResult* result = [poiResultList objectAtIndex:0];
		for (int i = 0; i < result.poiInfoList.count; i++) {
			BMKPoiInfo* poi = [result.poiInfoList objectAtIndex:i];
            [tableData addObject:poi];
		}
	}else{
        [self dismissErrorMsg:@"没有找到相关的地名，请重新搜索"];
    }
    
    theTableView.tag = kTableTagResult;
    
    CGRect frame = theTableView.frame;
    frame.size.height = kTableHeightResult;
    theTableView.frame = frame;
    
    [theTableView reloadData];
}

- (void)onGetAddrResult:(BMKAddrInfo*)result errorCode:(int)error{
    
}

#pragma mark tableView delegete
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 155.0;
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (theTableView.tag == kTableTagResult) {
        BMKPoiInfo* poi = [tableData objectAtIndex:indexPath.row];
        [vcDelegate didSelectAddr:poi];
        
        [self.navigationController popViewControllerAnimated:TRUE];
    }else if (theTableView.tag == kTableTagSearch){
        if (indexPath.row == tableData.count) {
            // clear search history
            [[RunTimeShare sharedInstance] removeSearchKeyArray];
            [tableData removeAllObjects];
            [theTableView reloadData];
        }else{
            theSearchBar.text = [tableData objectAtIndex:indexPath.row];
            [self searchBarSearchButtonClicked:theSearchBar];
        }
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    if (theTableView.tag == kTableTagResult){
        count = [tableData count];
    }else if (theTableView.tag == kTableTagSearch){
        //仅有历史记录时才添加清除按钮
        count = ([tableData count] > 0) ? [tableData count]+1 : 0;
    }
    return count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellIdentifier = @"contentCell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.userInteractionEnabled=YES;
    }
    
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    if (theTableView.tag == kTableTagResult) {
        BMKPoiInfo * info = [tableData objectAtIndex:indexPath.row];
        
        cell.textLabel.text = info.name;
        cell.detailTextLabel.text = info.address;
    }else if (theTableView.tag == kTableTagSearch){
        if (indexPath.row == tableData.count) {
            //删除按钮
            cell.textLabel.text = @"                清空历史";
            cell.detailTextLabel.text = nil;
        }else{
            cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
            cell.detailTextLabel.text = nil;
        }
    }
    
    return cell;
}

#pragma mark -
#pragma mark 显示提醒框

- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框

- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}
@end
