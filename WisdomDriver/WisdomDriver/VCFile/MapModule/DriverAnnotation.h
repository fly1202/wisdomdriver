//
//  DriverAnnotation.h
//  WisdomDriver
//
//  Created by  apple on 13-2-3.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BMapKit.h"

@interface DriverAnnotation : BMKShape{
    
    CLLocationCoordinate2D _coordinate;
    NSDictionary * _driverDict;
}

///该点的坐标
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, retain) NSDictionary * driverDict;

-(id)initWithDriverDict:(NSDictionary*)dict;
@end
