//
//  ChoseLocationVC.m
//  WisdomDriver
//
//  Created by  apple on 01-1-1.
//  Copyright (c) 2001年 JunYunKJ. All rights reserved.
//

#import "ChoseLocationVC.h"
#import "BMapKit.h"
#import "defines.h"
#import "RunTimeShare.h"
#import "SVProgressHUD.h"
#import "MapSearchVC.h"

@interface ChoseLocationVC ()<BMKMapViewDelegate,BMKSearchDelegate,UISearchBarDelegate,MapSearchVCDelegate>
{
    BMKMapView *            mapView;
    BMKSearch *             mkSearch;
    
    NSMutableArray *        annotations;
    
    UISearchBar *           searchBar;
    
    UILabel *               tintLb;
    BMKUserLocation *       userLocation;
    
    UILongPressGestureRecognizer * lpgr;
}
@end

@implementation ChoseLocationVC
@synthesize choseLocationVCDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    LogInfo(@"");
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self addTopViewWithTitle:@"地图选定"];
    [self addMapView];
    
    annotations = [[NSMutableArray alloc] init];
}

-(void)viewDidAppear:(BOOL)animated{
    
    LogInfo(@"");
    [super viewDidAppear:TRUE];
    
    mapView.delegate = self;
    
    //显示用户当前的坐标，打开地图有相应的提示
    mapView.showsUserLocation=YES;
    [self showWithStatusMsg:@"正在定位中"];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    LogInfo(@"");
    [super viewWillDisappear:TRUE];
    
    mapView.delegate = nil;
    
    //显示用户当前的坐标，打开地图有相应的提示
    mapView.showsUserLocation=NO;
}

#pragma mark - 添加顶部条栏和文字,收费标准,设置
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //取消
    UIButton *standardBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 8.0, 58.0, 30.0)];
    [standardBut setBackgroundImage:[UIImage imageNamed:@"navBarLeftButBottom.png"] forState:UIControlStateNormal];
    [standardBut setTitle:@"取消" forState:UIControlStateNormal];
    [standardBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [standardBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [standardBut addTarget:self action:@selector(clickOnCancelBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:standardBut];
    [standardBut release];
    
    //选定
    /*
    UIButton * settingBut = [[UIButton alloc] initWithFrame:CGRectMake(256.0, 8.0, 58.0, 30.0)];
    [settingBut setBackgroundImage:[UIImage imageNamed:@"navBarRightButBottom.png"] forState:UIControlStateNormal];
    [settingBut setTitle:@"选定" forState:UIControlStateNormal];
    [settingBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [settingBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [settingBut addTarget:self action:@selector(clickOnSettingBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:settingBut];
    [settingBut release];
     */
}

#pragma mark - 添加地图视图
- (void)addMapView{
    
    mkSearch = [[BMKSearch alloc] init];
	mkSearch.delegate = self;
    
    mapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, 45, 320, 460-45)];
    [mapView setMapType:BMKMapTypeStandard];
    
    //设置地图的代理
    mapView.delegate=self;
    
    [self.view addSubview:mapView];
    
    //搜索按钮
    UIButton * schBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //[schBtn setImage:[UIImage imageNamed:@"soso_btn1.png"] forState:UIControlStateNormal];
    [schBtn setBackgroundImage:[UIImage imageNamed:@"soso_btn1.png"] forState:UIControlStateNormal];
    [schBtn setFrame:CGRectMake(255, 245.0, 60.0, 50.0)];
    [schBtn setBackgroundColor:[UIColor clearColor]];
    [schBtn addTarget:self action:@selector(searchBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view insertSubview:schBtn aboveSubview:mapView];
    
    //定位按钮
    UIButton * locateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [locateBtn setImage:[UIImage imageNamed:@"dingwei_btn1.png"] forState:UIControlStateNormal];
    [locateBtn setFrame:CGRectMake(255, 300.0, 60.0, 50.0)];
    [locateBtn setBackgroundColor:[UIColor clearColor]];
    [locateBtn addTarget:self action:@selector(showUserLocation) forControlEvents:UIControlEventTouchUpInside];
    [self.view insertSubview:locateBtn aboveSubview:mapView];
    
    //添加长按扑捉
    lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1.0; //user needs to press for 2 seconds
    [mapView addGestureRecognizer:lpgr];
    [lpgr release];
    
    UILabel * bgLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 45, 320, 30)];
    bgLb.backgroundColor = [[UIColor alloc] initWithRed:86.0/255.0 green:86/255.0 blue:86/255.0 alpha:0.5];
    [self.view addSubview:bgLb];
    [bgLb release];
    
    tintLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 310, 30)];
    tintLb.font = [UIFont systemFontOfSize:12.0];
    tintLb.textColor = [UIColor whiteColor];
    tintLb.text = @"长按地图以选取地址";
    tintLb.backgroundColor = [UIColor clearColor];
    
    [bgLb addSubview:tintLb];
}

#pragma mark - 长按处理
- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:mapView];
    CLLocationCoordinate2D touchMapCoordinate =
    [mapView convertPoint:touchPoint toCoordinateFromView:mapView];
    
    [self showWithStatusMsg:@"正在解析地址中"];
    mkSearch.delegate = self;
    mapView.delegate = self;
    [mkSearch reverseGeocode:touchMapCoordinate];
}

#pragma mark - 取消
-(void)clickOnCancelBut{
    [mapView removeGestureRecognizer:lpgr];
    [self.navigationController popViewControllerAnimated:TRUE];
}

#pragma mark - 选定
-(void)clickOnSettingBut{
    
    if (annotations.count != 1) {
        [self dismissErrorMsg:@"未选择代驾地址"];
        return;
    }
    
    BMKPointAnnotation * anno = [annotations objectAtIndex:0];
    CLLocation * loc = [[CLLocation alloc] initWithLatitude:anno.coordinate.latitude
                                                  longitude:anno.coordinate.longitude];
    [self.choseLocationVCDelegate didChoseLocation:loc strAddr:anno.title];
    
    [self.navigationController popViewControllerAnimated:TRUE];
}

#pragma mark - 搜索按钮
- (void)searchBtnClicked{
    
    MapSearchVC * vc = [[MapSearchVC alloc] init];
    vc.vcDelegate = self;
    [self.navigationController pushViewController:vc animated:TRUE];
}

#pragma mark - 定位按钮
- (void)showUserLocation{
    NSLog(@"showUserLocation");
    [self moveToUserLocation];
}

#pragma mark - 搜索代理
-(void)didSelectAddr:(BMKPoiInfo*)addr{
    LogInfo(@"%@",addr.address);
    
    mapView.delegate = self;
    mkSearch.delegate = self;
    
    if (annotations) {
        [mapView removeAnnotations:annotations];
        [annotations removeAllObjects];
    }else{
        annotations = [[NSMutableArray alloc] init];
    }
    
    //新地点
    BMKPointAnnotation* annotation = [[BMKPointAnnotation alloc]init];
    annotation.coordinate = addr.pt;
    annotation.title = addr.address;
    [annotations addObject:annotation];
    [self performSelectorOnMainThread:@selector(updateAddrAnnotation) withObject:nil waitUntilDone:true];
    [annotation release];
    
    CLLocationCoordinate2D startCoord = addr.pt;
    // the distance should be from driver to user
    BMKCoordinateRegion adjustedRegion = [mapView regionThatFits:BMKCoordinateRegionMakeWithDistance(startCoord, SPAN_DISTANCE_WIDTH*0.6, SPAN_DISTANCE_HEIGHT*0.6)];
    [mapView setRegion:adjustedRegion animated:YES];
}

- (void)moveToUserLocation{
    CLLocationCoordinate2D startCoord = userLocation.coordinate;
    // the distance should be from driver to user
    BMKCoordinateRegion adjustedRegion = [mapView regionThatFits:BMKCoordinateRegionMakeWithDistance(startCoord, SPAN_DISTANCE_WIDTH, SPAN_DISTANCE_HEIGHT)];
    [mapView setRegion:adjustedRegion animated:YES];
}

#pragma - mark BMKSearchDelegate
- (void)onGetPoiResult:(NSArray*)poiResultList searchType:(int)type errorCode:(int)error{
    //BMKTypePoiList,BMKTypeAreaPoiList,BMKAreaMultiPoiList
    LogInfo(@" %d addr found",poiResultList.count);
}

- (void)onGetAddrResult:(BMKAddrInfo*)result errorCode:(int)error{
    if ([SVProgressHUD isVisible]) {
        [self dismiss];
    }
	// 在此处添加您对反地理编码结果的处理
    //添加选择地址大头针
    LogInfo(@"选择位置解析成功。。。");
    if (annotations) {
        [mapView removeAnnotations:annotations];
        [annotations removeAllObjects];
    }else{
        annotations = [[NSMutableArray alloc] init];
    }
    
    //新地点
    BMKPointAnnotation* annotation = [[BMKPointAnnotation alloc]init];
    annotation.coordinate = result.geoPt;
    annotation.title = result.strAddr;
    [annotations addObject:annotation];
    [self performSelectorOnMainThread:@selector(updateAddrAnnotation) withObject:nil waitUntilDone:true];
    [annotation release];
}

-(void)updateAddrAnnotation{
    [mapView addAnnotations:annotations];
}

#pragma mark -
#pragma mark BMapViewDelegate implementation
- (BMKAnnotationView *)mapView:(BMKMapView *)mapViewObj viewForAnnotation:(id <BMKAnnotation>)annotation {
    
    if ([annotation isKindOfClass:[BMKUserLocation class]])
        return nil;
    
    BMKPinAnnotationView *pinAnnotationView = (BMKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"mapAnnotation"];
    
    if (pinAnnotationView) {
        [pinAnnotationView prepareForReuse];
        pinAnnotationView.annotation = annotation;
    } else {
        pinAnnotationView = [[[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"mapAnnotation"] autorelease];
    }
    
    pinAnnotationView.canShowCallout = YES;
    
    UIButton *disclosureButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    pinAnnotationView.rightCalloutAccessoryView = disclosureButton;
    [disclosureButton addTarget:self
                         action:@selector(choseLocForDrive:)
               forControlEvents:UIControlEventTouchUpInside];
    
    return pinAnnotationView;
}

- (void)mapView:(BMKMapView *)mapView annotationView:(BMKAnnotationView *)view didChangeDragState:(BMKAnnotationViewDragState)newState
   fromOldState:(BMKAnnotationViewDragState)oldState{
    if (newState == BMKAnnotationViewDragStateEnding)
    {
        CLLocationCoordinate2D droppedAt = view.annotation.coordinate;
        LogInfo(@"dropped at %f,%f", droppedAt.latitude, droppedAt.longitude);
        
        //update the annotation
        //see if its an information annotation
        if ([view.annotation isKindOfClass:[BMKPointAnnotation class]]) {
            LogInfo(@"Info annotation updating..");
            //BMKPointAnnotation* userAnnotation = ((BMKPointAnnotation *)view.annotation);
        }
    }
}

- (void)mapView:(BMKMapView *)amapView didAddAnnotationViews:(NSArray *)views{
    LogInfo(@"called");
    
    for (id<BMKAnnotation> currentAnnotation in mapView.annotations) {
        [mapView selectAnnotation:currentAnnotation animated:YES];
    }
}

- (void)mapView:(BMKMapView *)mapView didSelectAnnotationView:(BMKAnnotationView *)view
{
    LogInfo(@"annotation touched: %@", view.annotation.title);
}

- (void)mapView:(BMKMapView *)mapViewObj didUpdateUserLocation:(BMKUserLocation *)uLocation{
    LogInfo(@"--");
    static float rate = 0.9;
    rate += 0.001;
    if (userLocation == nil) {
        userLocation = uLocation;
        
        CLLocationCoordinate2D startCoord = mapView.userLocation.coordinate;
        // the distance should be from driver to user
        BMKCoordinateRegion adjustedRegion = [mapView regionThatFits:BMKCoordinateRegionMakeWithDistance(startCoord, SPAN_DISTANCE_WIDTH*rate, SPAN_DISTANCE_HEIGHT*rate)];
        [mapView setRegion:adjustedRegion animated:YES];
    }
    [self dismiss];
}

- (void)mapViewDidFinishLoadingMap:(BMKMapView *)mapView{
    NSLog(@"mapViewDidFinishLoadingMap:");
}

- (void)mapViewDidFailLoadingMap:(BMKMapView *)mapView withError:(NSError *)error{
    NSLog(@"mapViewDidFailLoadingMap:%@",[error description]);
}

- (void)choseLocForDrive:(id)sender {
    NSLog(@"choseLocForDrive: called");
    //MapAnnotation *annotation = [[self.map selectedAnnotations] objectAtIndex:0];
    //push driver detail controller
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"是否选择此地址为常用地址？" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
    //alert.tag = versonAlertTag;
    [alert show];
    [alert release];
}

#pragma mark - 有关提醒框的协议处理
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    //处理页面跳转
    if (buttonIndex == 0) {
        BMKPointAnnotation * anno = [annotations objectAtIndex:0];
        CLLocation * loc = [[CLLocation alloc] initWithLatitude:anno.coordinate.latitude longitude:anno.coordinate.longitude];
        [self.choseLocationVCDelegate didChoseLocation:loc strAddr:anno.title];
        
        [self.navigationController popViewControllerAnimated:TRUE];
    }
}

#pragma mark -
#pragma mark 显示提醒框

- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框

- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}
@end
