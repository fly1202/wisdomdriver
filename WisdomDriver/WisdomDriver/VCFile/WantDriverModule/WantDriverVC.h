//
//  WantDriverVC.h
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-8.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WantDriverVCDelegate <NSObject>

- (void)enterDriverInfoVC:(NSDictionary*)dicDriver;
-(void)enterChoseLocationVC;
-(void)enterUsedAddressVC;
-(void)enterSearchVC;
-(void)enterMapVC;

@end

@interface WantDriverVC : UIViewController
{
    id<WantDriverVCDelegate> wantDriverVCDelegate;
}
@property (nonatomic,assign) id<WantDriverVCDelegate> wantDriverVCDelegate;
-(void)updateUserAddr;
-(void)setStartAddr:(NSDictionary*)dict;
-(void)clearForlogin;
@end
