//
//  WantDriverVC.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-8.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "WantDriverVC.h"
#import "RunTimeShare.h"
#import "ChoseLocationVC.h"
#import "MyDateTimePicker.h"
#import "ActionSheetPicker.h"
#import "ClientJSONObject.h"
#import "UsedAddressVC.h"
#import "defines.h"
#import "SVProgressHUD.h"
#import "MapSearchVC.h"

#define telFieldTag 110

@interface WantDriverVC ()<UITextFieldDelegate,
UIPickerViewDelegate,UIPickerViewDataSource,ChoseLocationVCDelegate,UsedAddressVCDelegate,MapSearchVCDelegate>{
    UIView * bottomView;
    
    NSMutableArray * messageLblArr;
    
    UITextField * nameField;
    UITextField * telField;
    MyDateTimePicker * datePicker;
    
    NSMutableArray * countArray;
    
    NSDictionary * startAddrDict;
    NSDictionary * endAddrDict;
    
    BOOL bStartAddr;
}

@end

@implementation WantDriverVC
@synthesize wantDriverVCDelegate;

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 460.0)];
    //bottomView.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0];
    //[bottomView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [bottomView addSubview:bgImgView];
    [self.view addSubview:bottomView];
    [bottomView release];
    
    [self addTopViewWithTitle:@"我要代驾"];
    
    [self addUIToBottomView];
    
    countArray = [[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10", nil];
}

#pragma mark - 添加顶部条栏和文字
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
}

#pragma mark - 各种UI布局
-(void)addUIToBottomView{
    
    messageLblArr = [[NSMutableArray alloc] init];
    
    UIImageView * bottomView1 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 50.0, 300.0, 180.0)];
    bottomView1.image = [UIImage imageNamed:@"bg_customtable4.png"];
    bottomView1.userInteractionEnabled = YES;
    [bottomView addSubview:bottomView1];
    [bottomView1 release];

    NSArray * strArr = [[NSArray alloc] initWithObjects:@"代驾位置:",@"结束位置:",@"代驾时间:",@"所需司机:", nil];
    for (int i=0; i<[strArr count]; i++) {
        UILabel * strLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 45.0*i, 70.0, 45.0)];
        strLbl.textAlignment = UITextAlignmentRight;
        strLbl.backgroundColor = [UIColor clearColor];
        strLbl.text = [strArr objectAtIndex:i];
        strLbl.font = [UIFont systemFontOfSize:16.0];
        [bottomView1 addSubview:strLbl];
        [strLbl release];
        
        UIButton * messageBut = [[UIButton alloc] initWithFrame:CGRectMake(70.0, 45.0*i, 140.0, 45.0)];
        messageBut.backgroundColor = [UIColor clearColor];
        messageBut.tag = 100+i;
        [messageBut addTarget:self action:@selector(clickOnMessageBut:) forControlEvents:UIControlEventTouchUpInside];
        [bottomView1 addSubview:messageBut];
        [messageBut release];
        
        UILabel * messageLbl = [[UILabel alloc] initWithFrame:CGRectMake(83.0, 45.0*i, 230.0, 45.0)];
        messageLbl.backgroundColor = [UIColor clearColor];
        messageLbl.textAlignment = UITextAlignmentLeft;
        //messageLbl.text = @"显示位置信息";
        messageLbl.textColor = [UIColor grayColor];
        messageLbl.font = [UIFont systemFontOfSize:16];
        messageLbl.lineBreakMode = UILineBreakModeCharacterWrap;
        messageLbl.numberOfLines = 0;
        [bottomView1 addSubview:messageLbl];
        [messageLblArr addObject:messageLbl];
        [messageLbl release];
        
        //在代驾位置和结束位置添加的是标签和按钮
        if (i==0||i==1) {
            UILabel * messageLbl = [messageLblArr objectAtIndex:i];
            messageLbl.text = (i==0)? [RunTimeShare sharedInstance].curLocationString : @"显示位置信息";
            if (i == 0) {
                startAddrDict = [RunTimeShare makeAddrDict:messageLbl.text location:[RunTimeShare sharedInstance].curUserLocation];
            }
            messageLbl.font = [UIFont systemFontOfSize:12];
            messageLbl.frame = CGRectMake(83.0, 45.0*i, 133.0, 45.0);
            
            //地图按钮
            UIButton * mapBut = [[UIButton alloc] initWithFrame:CGRectMake(215.0, 1.0+45.0*i, 30.0, 44.0)];
            [mapBut setImage:[UIImage imageNamed:@"mapred.png"] forState:UIControlStateNormal];
            mapBut.tag = 100+i;
            [mapBut addTarget:self action:@selector(clickOnMapBut:) forControlEvents:UIControlEventTouchUpInside];
            [bottomView1 addSubview:mapBut];
            [mapBut release];
            
            //常用地址按钮
            UIButton * usedBut = [[UIButton alloc] initWithFrame:CGRectMake(260.0, 12.0+45.0*i, 34.0, 22.0)];
            [usedBut setImage:[UIImage imageNamed:@"oldDress.png"] forState:UIControlStateNormal];
            usedBut.tag = 100+i;
            [usedBut addTarget:self action:@selector(clickOnUsedBut:) forControlEvents:UIControlEventTouchUpInside];
            [bottomView1 addSubview:usedBut];
            [usedBut release];
        }
        // default value
        if (i==2) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mmaaa"];
            messageLbl.text = [NSString stringWithFormat:@"%@",
                          [dateFormatter stringFromDate:[NSDate date]]];
            [dateFormatter release];
        }
        if (i==3) {
            messageLbl.text = @"1";
        }
    }
    
    
    UIImageView * bottomView2 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 240.0, 300.0, 90.0)];
    bottomView2.image = [UIImage imageNamed:@"bg_customtable2.png"];
    bottomView2.userInteractionEnabled = YES;
    [bottomView addSubview:bottomView2];
    [bottomView2 release];
    
    NSArray * strArr1 = [[NSArray alloc] initWithObjects:@"联系人",@"电话",nil];
    for (int index=0; index<[strArr1 count]; index++) {
        UILabel * strLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 45.0*index, 75.0, 45.0)];
        strLbl.textAlignment = UITextAlignmentRight;
        strLbl.backgroundColor = [UIColor clearColor];
        strLbl.text = [strArr1 objectAtIndex:index];
        strLbl.font = [UIFont systemFontOfSize:16.0];
        [bottomView2 addSubview:strLbl];
        [strLbl release];
        
        UITextField * txtField = [[UITextField alloc] initWithFrame:CGRectMake(80, 45.0*index, 260, 45.0)];
        txtField.backgroundColor = [UIColor clearColor];
        txtField.delegate = self;
        txtField.textColor = [UIColor grayColor];
        txtField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
        if (index == 0) {
            nameField = txtField;
        }else{
            telField = txtField;
            telField.tag = telFieldTag;
        }
        [bottomView2 addSubview:txtField];
        [txtField release];
        
        if (index == 0) {
            //联系人
            txtField.text = [RunTimeShare getNickName];
        }else{
            txtField.text = [RunTimeShare getUserTel];
        }
    }
    
    //下单按钮
    UIImageView * bottomView3 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 340.0, 300.0, 45.0)];
    bottomView3.image = [UIImage imageNamed:@"btn_xiadandefault1.png"];
    bottomView3.userInteractionEnabled = YES;
    [self.view addSubview:bottomView3];
    [bottomView3 release];
    
    UIButton * button = [[UIButton alloc] init];
    button.frame = bottomView3.frame;
    button.backgroundColor = [UIColor clearColor];
    [button setImage:[UIImage imageNamed:@"btn_xiadandefault1.png"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"btn_xiadanselected2.png"] forState:UIControlStateSelected];
    [button addTarget:self action:@selector(submitBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    [button release];
    
    UILabel * btnLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 45)];
    btnLb.backgroundColor = [UIColor clearColor];
    btnLb.textColor = [UIColor whiteColor];
    btnLb.text = @"下  单";
    btnLb.font = [UIFont systemFontOfSize:18.0];
    btnLb.textAlignment = UITextAlignmentCenter;
    [button addSubview:btnLb];
    [btnLb release];
}

-(void)updateUserAddr{
    UILabel * messageLbl = [messageLblArr objectAtIndex:0];
    messageLbl.text = [RunTimeShare sharedInstance].curLocationString;
    startAddrDict = [RunTimeShare makeAddrDict:messageLbl.text
                                      location:[RunTimeShare sharedInstance].curUserLocation];
}

-(void)setStartAddr:(NSDictionary*)dict{
    UILabel * messageLbl = [messageLblArr objectAtIndex:0];
    messageLbl.text = [dict valueForKey:kAddrStr];
    startAddrDict = dict;
}

#pragma mark - 司机数量选择代理
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    return [countArray count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [countArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    NSLog(@"Selected Color: %@. Index of selected color: %i", [countArray objectAtIndex:row], row);
    UILabel * lb = [messageLblArr objectAtIndex:3];
    lb.text = [countArray objectAtIndex:row];
}

#pragma mark - 下单按钮
-(void)submitBtnClick{
    NSLog(@"submitBtnClick");
    if ([((UILabel*)[messageLblArr objectAtIndex:0]).text length] == 0 ||
        [((UILabel*)[messageLblArr objectAtIndex:1]).text length] == 0) {
        [self dismissErrorMsg:@"代驾位置未选择"];
        return;
    }
    
    /*
    if ([((UILabel*)[messageLblArr objectAtIndex:1]).text isEqualToString:@"显示位置信息"]) {
        [self dismissErrorMsg:@"结束位置未选择"];
        return;
    }
     */
    
    if ([((UILabel*)[messageLblArr objectAtIndex:2]).text length] == 0 ||
        [((UILabel*)[messageLblArr objectAtIndex:3]).text length] == 0) {
        [self dismissErrorMsg:@"请选择代驾时间及代驾司机数量后再提交"];
        return;
    }
    
    if ([nameField.text length] == 0 || [telField.text length] == 0) {
        [self dismissErrorMsg:@"请填写联系人信息后再提交"];
        return;
    }
    
    [self showWithStatusMsg:@"正在提交订单"];
    [self performSelector:@selector(submitOrder) withObject:nil afterDelay:0.5];
}

-(void)submitOrder{
    NSString * start = ((UILabel*)[messageLblArr objectAtIndex:0]).text;
    NSString * end = ((UILabel*)[messageLblArr objectAtIndex:1]).text;
    NSString * time = ((UILabel*)[messageLblArr objectAtIndex:2]).text;
    NSString * count = ((UILabel*)[messageLblArr objectAtIndex:3]).text;
    NSString * name = nameField.text;
    NSString * tel = [telField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString * userTel = [RunTimeShare getUserTel];
    NSString * clientID = [[RunTimeShare getUserInfo] valueForKey:kUserInfoClientID];
    double lat = [[startAddrDict valueForKey:kAddrLatitude] doubleValue];
    double lon = [[startAddrDict valueForKey:kAddrLogitude] doubleValue];
    double acc = [[startAddrDict valueForKey:kAddrAccuracy] doubleValue];
    
    if (!end || [end isEqualToString:@"显示位置信息"]) {
        end = @"暂未指定";
    }
    
    //提交订单
    NSDictionary * dict = [[ClientJSONObject clientJSONObject] saveOrderWithTel:userTel clientID:clientID contactName:name contactTel:tel start:start end:end time:time driverNum:count lon:lon lat:lat accuracy:acc];
    if ([[dict valueForKey:@"result"] integerValue] == 1) {
        //success
        [self dismissSuccessMsg:@"下单成功"];
        
        [self clearForlogin];
        [self.wantDriverVCDelegate enterMapVC];
    }else{
        [self dismissErrorMsg:@"订单提交失败"];
    }
}

#pragma mark - 获取位置信息
-(void)clickOnMessageBut:(id)sender{
    
    NSLog(@"clickOnMessageBut");
    UIButton* btn = (UIButton*)sender;
    int index = btn.tag - 100;
    if (index == 0 || index == 1) {
        bStartAddr = (index == 0);
        [wantDriverVCDelegate enterSearchVC];
        /*
        UILabel * lb = [messageLblArr objectAtIndex:index];
        lb.text = [RunTimeShare sharedInstance].curLocationString;
         */
    }
    else if (index == 2){
        //选择代驾时间
        datePicker = [[MyDateTimePicker alloc] initWithFrame:CGRectMake(0, 460-MyDateTimePickerHeight-49, 325, MyDateTimePickerHeight)];
        [datePicker setMode:UIDatePickerModeDateAndTime];
        [datePicker setHidden:NO animated:TRUE];
        datePicker.picker.minimumDate = [NSDate date];
        [datePicker addTargetForDoneButton:self action:@selector(changeDateInLabel)];
        [self.view addSubview:datePicker];
        [datePicker release];
    }
    else if (index == 3){
        //选择司机数量
        [ActionSheetStringPicker showPickerWithTitle:@"选择司机数量" rows:countArray initialSelection:0 target:self successAction:@selector(countWasSelected:element:) cancelAction:@selector(actionPickerCancelled:) origin:sender];
    }
}

#pragma mark - 地址搜索代理
-(void)didSelectAddr:(BMKPoiInfo*)addr{
    UILabel * lb = [messageLblArr objectAtIndex:(bStartAddr ? 0 : 1)];
    lb.text = addr.address;
    CLLocation * loc = [[CLLocation alloc] initWithLatitude:addr.pt.latitude longitude:addr.pt.longitude];
    if (bStartAddr) {
        startAddrDict = [RunTimeShare makeAddrDict:addr.address location:loc];
    }else{
        endAddrDict = [RunTimeShare makeAddrDict:addr.address location:loc];
    }
    [self.navigationController popViewControllerAnimated:TRUE];
}

#pragma mark - Implementation

- (void)countWasSelected:(NSNumber *)selectedIndex element:(id)element {
    int index = [selectedIndex intValue];
    UILabel * lb = [messageLblArr objectAtIndex:3];
    lb.text = [countArray objectAtIndex:index];
}

- (void)actionPickerCancelled:(id)sender {
    NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

#pragma mark - 日期选择
-(void)changeDateInLabel{
    UILabel * label = [messageLblArr objectAtIndex:2];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mmaaa"];
	label.text = [NSString stringWithFormat:@"%@",
                  [dateFormatter stringFromDate:datePicker.picker.date]];
	[dateFormatter release];
    
    [datePicker setHidden:TRUE animated:TRUE];
}

#pragma mark - 从地图上获取位置
-(void)clickOnMapBut:(id)sender{
    NSLog(@"clickOnMapBut");
    if (((UIButton*)sender).tag-100 == 0) {
        bStartAddr = TRUE;
    }else{
        bStartAddr = FALSE;
    }
    [self.wantDriverVCDelegate enterChoseLocationVC];
}

#pragma mark - 从常用地址中选择地址
-(void)clickOnUsedBut:(id)sender{
    NSLog(@"clickOnUsedBut");
    if (((UIButton*)sender).tag-100 == 0) {
        bStartAddr = TRUE;
    }else{
        bStartAddr = FALSE;
    }
    [self.wantDriverVCDelegate enterUsedAddressVC];
}

#pragma mark - 地图位置选择delegate
-(void)didChoseLocation:(CLLocation*)loc strAddr:(NSString*)addr{
    NSLog(@"didChoseLocation:");
    int index = bStartAddr ? 0 : 1;
    if (bStartAddr) {
        startAddrDict = [RunTimeShare makeAddrDict:addr location:loc];
    }else{
        endAddrDict = [RunTimeShare makeAddrDict:addr location:loc];
    }
    UILabel * lb = [messageLblArr objectAtIndex:index];
    lb.text = addr;
}

#pragma mark - 常用地址选择delegate
-(void)didSelectAddress:(NSDictionary *)addrDict{
    NSLog(@"didSelectAddress:");
    int index = bStartAddr ? 0 : 1;
    if (bStartAddr) {
        startAddrDict = addrDict;
    }else{
        endAddrDict = addrDict;
    }
    UILabel * lb = [messageLblArr objectAtIndex:index];
    lb.text = [addrDict valueForKey:kAddrStr];
}

#pragma mark - 清理页面重新登陆
-(void)clearForlogin{
    [self updateUserAddr];
    for (int i = 0; i <messageLblArr.count; i++) {
        UILabel * messageLbl = [messageLblArr objectAtIndex:i];
        if (i == 0) {
            startAddrDict = [RunTimeShare makeAddrDict:messageLbl.text location:[RunTimeShare sharedInstance].curUserLocation];
            messageLbl.text = (i==0)? [RunTimeShare sharedInstance].curLocationString : @"显示位置信息";
        }else if (i == 1){
            messageLbl.text = (i==0)? [RunTimeShare sharedInstance].curLocationString : @"显示位置信息";
        }else if (i==2) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mmaaa"];
            messageLbl.text = [NSString stringWithFormat:@"%@",
                               [dateFormatter stringFromDate:[NSDate date]]];
            [dateFormatter release];
        }else if (i==3) {
            messageLbl.text = @"1";
        }
    }
    
    nameField.text = [RunTimeShare getNickName];
    telField.text = [RunTimeShare getUserTel];
}

#pragma mark - 输入框UITextField的协议处理

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
}

//按下Done按钮的调用方法，我们让键盘消失
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSCharacterSet *cs;
    if(textField.tag == telFieldTag)
    {
        cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        BOOL basicTest = [string isEqualToString:filtered];
        if(!basicTest)
        {
            [self dismissErrorMsg:@"请输入数字"];
            return NO;
        }
    }
    
    //其他的类型不需要检测，直接写入
    return YES;
}

#pragma mark -
#pragma mark 显示提醒框

- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框

- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}
@end
