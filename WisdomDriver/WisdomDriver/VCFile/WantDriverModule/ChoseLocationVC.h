//
//  ChoseLocationVC.h
//  WisdomDriver
//
//  Created by  apple on 01-1-1.
//  Copyright (c) 2001年 JunYunKJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BMapKit.h"

@protocol ChoseLocationVCDelegate <NSObject>

-(void)didChoseLocation:(CLLocation*)loc strAddr:(NSString*)addr;

@end

@interface ChoseLocationVC : UIViewController
{
    id<ChoseLocationVCDelegate> choseLocationVCDelegate;
}
@property (nonatomic,assign) id<ChoseLocationVCDelegate> choseLocationVCDelegate;
@end
