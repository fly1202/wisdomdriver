//
//  HelperVC.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-6.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//
//帮助提示，在应用第一次在用户手机运行时会显示，过后不会再显示

#import "HelperVC.h"
#import "RunTimeShare.h"

@interface HelperVC (){

}

-(void)LoadScrollView;

@end

@implementation HelperVC

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self LoadScrollView];
}

#pragma mark - 加载帮助图片到滑动页面上
-(void)LoadScrollView{
    //帮助图片的数量，从数字1开始
    int picCount = 6;
    UIScrollView * helperScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(-8.0, 0.0, 336.0, 460.0)];
    helperScroll.contentSize = CGSizeMake(336.0*picCount, 460.0);
    helperScroll.pagingEnabled = YES;
    helperScroll.showsHorizontalScrollIndicator = NO;
    for (int i=1; i<=picCount; i++) {
        UIImageView * helpView = [[UIImageView alloc] initWithFrame:CGRectMake(336.0*(i-1)+8.0, 0.0, 320.0, 460.0)];
        helpView.image = [UIImage imageNamed:[NSString stringWithFormat:@"helpInfo%i.png",i]];
        [helperScroll addSubview:helpView];
        [helpView release];
    }
    
    //在最后一张帮助图片上加载一个按钮
    //需要根据不同的图片上面按钮位置不一样而进行布局
    UIImageView * imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 65.0, 45.0)];
    [imgView setImage:[UIImage imageNamed:@"btn_go.png"]];
    
    UIButton * enterBut = [[UIButton alloc] initWithFrame:CGRectMake(336.0*(picCount-1)+265.0, 380.0, 65.0, 45.0)];
    enterBut.backgroundColor = [UIColor clearColor];
    [enterBut addTarget:self action:@selector(GotoEnterApp) forControlEvents:UIControlEventTouchUpInside];
    [enterBut addSubview:imgView];
    [helperScroll addSubview:enterBut];
    [enterBut release];
    [imgView release];
    
    [self.view addSubview:helperScroll];
    [helperScroll release];
}

#pragma mark - 进入应用主界面
-(void)GotoEnterApp{
    //修改为不再是第一次运行
    [RunTimeShare setFirstTimeRun:NO];
    
    MainTabVC * mainVC = [[MainTabVC alloc] init];
    [self.navigationController pushViewController:mainVC animated:YES];
}

@end
