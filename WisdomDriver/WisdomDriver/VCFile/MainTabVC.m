//
//  MainTabVC.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-7.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//


#import "MainTabVC.h"
#import "defines.h"
#import "RunTimeShare.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "CustomBadge.h"
#import "MapSearchVC.h"
#import "NearDriverViewController.h"
#import "DriverInfoViewController.h"
#import "SBJson.h"
#import "NSData+Base64.h"


static int curTag = 0;

@interface MainTabVC ()<ABPeoplePickerNavigationControllerDelegate,ChoseLocationVCDelegate>{
    LoginMainVC * loginVC;
    NearDriverViewController * mapVC;
    WantDriverVC * wantVC;
    OrderManageVC * orderManageVC;
    CustomBadge * customBadge;
    
    StandardVC * standVC;
    SettingVC  * settingVC;
        
    //底部的Tabar
    UIImageView * myTabarView;
    
    //底部Tabar按钮选择状态底图
    UIImageView * SelBottomView;
    
    //底部按钮图像数组
    NSArray * tabarButImageNameArr;
    NSMutableArray * tabarButViewArr;
    
    //显示位置的提示UITextView
    UITextView  * PositionTxt;
    
    //装载电话下单ui的UIView
    UIView * phoneOderView;
    NSString * newVerUrl;
}

-(void)addTabarView;
-(void)addTopViewWithTitle:(NSString *)title;

@end

@implementation MainTabVC

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0];
    
    
    //加载各个UIViewController
    [self addApplicationViewController];
    
    //加载底部Tabar
    [self addTabarView];
    
    //默认下是地图界面首先出现
    [self.view insertSubview:mapVC.view belowSubview:myTabarView];
    
    //如果还没有自动登录，那么需要先进入登录界面处理
    BOOL isLogin = [RunTimeShare isAutoLogin];
    if (!isLogin) {
        loginVC = [[LoginMainVC alloc] init];
        loginVC.view.frame = CGRectMake(0.0, 0.0, 320.0, 460.0);
        loginVC.loginDelegate = self;
        [self.view addSubview:loginVC.view];
        [loginVC.view release];
    }
    //-------add by caoyanbo---------
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLocationNotify:) name:kNotifyUpdateLocation object:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    LogInfo(@"");
    switch (curTag) {
        case 0:
        {
            [mapVC viewDidAppear:animated];
        }
            break;
        case 1:
        {
            //点击了我要代驾按钮
            [wantVC viewDidAppear:animated];
        }
            break;
        case 2:
        {
            //[phoneOderView viewWillAppear:animated];
        }
            break;
        case 3:
        {
            //点击了订单结算按钮
            [orderManageVC viewDidAppear:animated];
        }
            break;
        default:
            break;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    LogInfo(@"");
    switch (curTag) {
        case 0:
        {
            [mapVC viewWillAppear:animated];
        }
            break;
        case 1:
        {
            //点击了我要代驾按钮
            [wantVC viewWillAppear:animated];
        }
            break;
        case 2:
        {
            //[phoneOderView viewWillAppear:animated];
        }
            break;
        case 3:
        {
            //点击了订单结算按钮
            [orderManageVC viewWillAppear:animated];
        }
            break;
        default:
            break;
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    LogInfo(@"");
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    LogInfo(@"");
}

#pragma mark - 进行新版本检测
-(void)VersionDetection{
    //曹工，进行新版本检测
    NSString * ver = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    /*
     NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
     CFShow(infoDic);
     
     NSString *appVersion = [infoDic objectForKey:@"CFBundleVersion"];
     */
    [self onCheckVersion:ver];
}

#pragma mark - 版本监测
-(void)onCheckVersion:(NSString *)currentVersion
{
    NSString *URL = APPURL;
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:URL]];
    [request setHTTPMethod:@"POST"];
    NSHTTPURLResponse *urlResponse = nil;
    NSError *error = nil;
    NSData *recervedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
    
    NSString *results = [[NSString alloc] initWithBytes:[recervedData bytes] length:[recervedData length] encoding:NSUTF8StringEncoding];
    SBJsonParser *jsonParser = [[SBJsonParser alloc] init];
    NSDictionary *dic = [[[jsonParser objectWithString:results] copy] autorelease];
    
    NSArray *infoArray = [dic objectForKey:@"results"];
    if ([infoArray count]) {
        NSDictionary *releaseInfo = [infoArray objectAtIndex:0];
        NSString *lastVersion = [releaseInfo objectForKey:@"version"];
        
        if (![lastVersion isEqualToString:currentVersion]) {
            newVerUrl = [releaseInfo objectForKey:@"trackVireUrl"];
            LogInfo(@"new version found:%@",newVerUrl);
            UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"更新" message:@"有新的版本更新，是否前往更新？" delegate:self cancelButtonTitle:@"更新" otherButtonTitles:@"关闭", nil] autorelease];
            [alert show];
        }
    }
}

#pragma mark - 有关提醒框的协议处理
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        //处理去下载新版本的事情
        NSString *iTunesLink = APP_UPDATE_URL;
        LogInfo(@"new version iTunesLink:%@",APP_UPDATE_URL);
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
    }
}

#pragma mark - 加载各个UIViewController
-(void)addApplicationViewController{
    CGRect vcRect = CGRectMake(0.0, 0.0, 320.0, 460.0);
    
    //加载地图控制器界面
    mapVC = [[NearDriverViewController alloc] init];
    mapVC.wantDriverVCDelegate = self;
    mapVC.view.frame = vcRect;
    mapVC.mapVcDelegate = self;
    [self.view addSubview:mapVC.view];
    [mapVC.view release];
    
    //加载我要代驾控制器界面
    wantVC = [[WantDriverVC alloc] init];
    wantVC.wantDriverVCDelegate = self;
    wantVC.view.frame = vcRect;
    [self.view addSubview:wantVC.view];
    [wantVC.view release];
    
    //加载订单管理控制器界面
    orderManageVC = [[OrderManageVC alloc] init];
    orderManageVC.vcDelegate = self;
    orderManageVC.view.frame = vcRect;
    [self.view addSubview:orderManageVC.view];
    [orderManageVC.view release];
    
    //电话下单的ui处理
    [self addPhoneOrderView];
}

#pragma mark - 底部TabarView的处理
-(void)addTabarView{
    //底部按钮名称
    tabarButImageNameArr = [[NSArray alloc] initWithObjects:@"mapBut",@"wantDriver",@"phoneBut",@"jiezhangBut",nil];
    
    myTabarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 411.0, 320.0, 49.0)];
    myTabarView.image = [UIImage imageNamed:@"tabarView.png"];
    myTabarView.userInteractionEnabled = YES;
    [self.view addSubview:myTabarView];
    [myTabarView release];
    
    //底部按钮选中时显示的底图，旁边带有两条分割线
    SelBottomView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 80.0, 49.0)];
    SelBottomView.image = [UIImage imageNamed:@"tabarSelected.png"];
    [myTabarView addSubview:SelBottomView];
    [SelBottomView release];
    
    UIImageView * leftLine = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 1.0, 49.0)];
    leftLine.image = [UIImage imageNamed:@"dibuline.png"];
    [SelBottomView addSubview:leftLine];
    [leftLine release];
    
    UIImageView * rightLine = [[UIImageView alloc] initWithFrame:CGRectMake(79.0, 0.0, 1.0, 49.0)];
    rightLine.image = [UIImage imageNamed:@"dibuline.png"];
    [SelBottomView addSubview:rightLine];
    [rightLine release];

    NSArray * titleArr = [[NSArray alloc] initWithObjects:@"地图",@"我要代驾",@"电话下单",@"订单结算",nil];
    tabarButViewArr = [[NSMutableArray alloc] init];
    for (int i=0; i<[titleArr count]; i++) {
        NSString * imageName = [NSString stringWithFormat:@"%@.png",[tabarButImageNameArr objectAtIndex:i]];
        UIImageView * butView = [[UIImageView alloc] initWithFrame:CGRectMake(80.0*i+25.0, 0.0, 30.0, 30.0)];
        butView.image = [UIImage imageNamed:imageName];
        
        [tabarButViewArr addObject:butView];
        [myTabarView addSubview:butView];
        
        UILabel * nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(80.0*i, 28.0, 80.0, 19.0)];
        nameLbl.text = [titleArr objectAtIndex:i];
        nameLbl.textColor = [UIColor whiteColor];
        nameLbl.textAlignment = UITextAlignmentCenter;
        nameLbl.backgroundColor = [UIColor clearColor];
        nameLbl.font = [UIFont systemFontOfSize:12.0];
        [myTabarView addSubview:nameLbl];
        [nameLbl release];
        
        //按钮
        UIButton * tabarBut = [[UIButton alloc] initWithFrame:CGRectMake(80.0*i, 0.0, 80.0, 49.0)];
        tabarBut.backgroundColor = [UIColor clearColor];
        tabarBut.tag = 1000+i;
        [tabarBut addTarget:self action:@selector(clickOnTabarBut:) forControlEvents:UIControlEventTouchUpInside];
        [myTabarView addSubview:tabarBut];
        [tabarBut release];
    
        //默认下显示地图控制器
        if (i==0) {
            NSString * name = [NSString stringWithFormat:@"%@Sel.png",[tabarButImageNameArr objectAtIndex:i]];
            butView.image = [UIImage imageNamed:name];
        }
    }
}

-(void)clickOnTabarBut:(id)sender{
    int butTag = ((UIButton *)sender).tag-1000;
    if (curTag==butTag) {
        return;
    }
    
    curTag = butTag;
    switch (butTag) {
        case 0:
        {
            //点击了地图按钮
            [self.view insertSubview:mapVC.view belowSubview:myTabarView];
            [mapVC viewDidAppear:TRUE];
        }
            break;
        case 1:
        {
            //点击了我要代驾按钮
            [self.view insertSubview:wantVC.view belowSubview:myTabarView];
        }
            break;
        case 2:
        {
            //点击了电话下单按钮
            [self.view insertSubview:phoneOderView belowSubview:myTabarView];
        }
            break;
        case 3:
        {
            //点击了订单结算按钮
            [self.view insertSubview:orderManageVC.view belowSubview:myTabarView];
        }
            break;
        default:
            break;
    }
    
    //修改底部按钮下面UIImageView图像
    for (int i=0; i<[tabarButViewArr count]; i++) {
        UIImageView * curView = (UIImageView *)[tabarButViewArr objectAtIndex:i];
        NSString * imageName = [NSString stringWithFormat:@"%@.png",[tabarButImageNameArr objectAtIndex:i]];
        curView.image = [UIImage imageNamed:imageName];
        
        //如果是当前的按钮被选中,则替换成选中的图像
        if (curTag==i) {
            NSString * imageName = [NSString stringWithFormat:@"%@Sel.png",[tabarButImageNameArr objectAtIndex:i]];
            curView.image = [UIImage imageNamed:imageName];
        }
    }
    
    //移动底部按钮的底图
    [SelBottomView setFrame:CGRectMake(80.0*curTag, 0.0, 80.0, 49.0)];
}

#pragma mark - 加载电话下单的ui处理
-(void)addPhoneOrderView{
    phoneOderView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 460.0)];
    //phoneOderView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [phoneOderView addSubview:bgImgView];
    [self.view addSubview:phoneOderView];
    [phoneOderView release];
    
    [self addTopViewWithTitle:@"电话下单"];
    
    //文字处理
    UITextView * topTxt = [[UITextView alloc] initWithFrame:CGRectMake(2.0, 46.0, 314.0, 100.0)];
    topTxt.text = @"尊敬的客户您好,欢迎使用我们的代驾客户端电话预约服务。\n\n全国服务电话:";
    topTxt.userInteractionEnabled = NO;
    topTxt.font = [UIFont systemFontOfSize:15.0];
    topTxt.backgroundColor = [UIColor clearColor];
    [phoneOderView addSubview:topTxt];
    [topTxt release];
    
    UITextView * bottomTxt = [[UITextView alloc] initWithFrame:CGRectMake(2.0, 265.0, 314.0, 130.0)];
    bottomTxt.text = @"拨打前请确认:\n1.您要代驾的时间\n2.您要代驾的开始和结束的地址\n3.电话下单成功后,我们的代驾司机会直接跟您取得联系,同时会有预约成功短信下发";
    bottomTxt.userInteractionEnabled = NO;
    bottomTxt.font = [UIFont systemFontOfSize:15.0];
    bottomTxt.backgroundColor = [UIColor clearColor];
    [phoneOderView addSubview:bottomTxt];
    [bottomTxt release];
    
    //电话按钮
    UIButton * phoneBut = [[UIButton alloc] initWithFrame:CGRectMake(45.0, 150.0, 230.0, 49.0)];
    [phoneBut setImage:[UIImage imageNamed:@"phoneCall.png"] forState:UIControlStateNormal];
    [phoneBut addTarget:self action:@selector(callPhoneToOrder) forControlEvents:UIControlEventTouchUpInside];
    [phoneOderView addSubview:phoneBut];
    [phoneBut release];
    
    UILabel * phoneLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 230.0, 49.0)];
    phoneLbl.backgroundColor = [UIColor clearColor];
    phoneLbl.textColor = [UIColor whiteColor];
    phoneLbl.textAlignment = UITextAlignmentCenter;
    phoneLbl.font = [UIFont systemFontOfSize:20.0];
    phoneLbl.text = kPhoneOrderService;
    [phoneBut addSubview:phoneLbl];
    [phoneLbl release];
    
    //位置提示
    PositionTxt = [[UITextView alloc] initWithFrame:CGRectMake(0.0, 201.0, 320.0, 75.0)];
    PositionTxt.backgroundColor = [UIColor clearColor];
    PositionTxt.text = [NSString stringWithFormat:@"您当前位置:%@",[self getCurPosition]];
    PositionTxt.font = [UIFont systemFontOfSize:18.0];
    PositionTxt.userInteractionEnabled = NO;
    PositionTxt.textColor = [UIColor colorWithRed:86.0/255.0 green:86.0/255.0 blue:86.0/255.0 alpha:1.0];
    [phoneOderView addSubview:PositionTxt];
    [PositionTxt release];
}

//添加顶部条栏和文字
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [phoneOderView addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [phoneOderView addSubview:topLbl];
    [topLbl release];
}

#pragma mark - 电话下单
-(void)callPhoneToOrder{
    NSArray * tempArr = [kPhoneOrderService componentsSeparatedByString:@"-"];
    NSString * realPhone = [tempArr componentsJoinedByString:@""];
    NSString *phone = [NSString stringWithFormat:@"tel://%@",realPhone];
    UIWebView*callWebview =[[UIWebView alloc] init];
    NSURL *telURL =[NSURL URLWithString:phone];
    [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
    [self.view addSubview:callWebview];
    [callWebview release];
}

#pragma mark - 获取当前位置
-(NSString *)getCurPosition{
    return [RunTimeShare sharedInstance].curLocationString;
}

#pragma mark - 处理登录界面的协议

//登录成功
-(void)loginDidSuccess{
    [loginVC.view removeFromSuperview];
    loginVC = nil;
    
    [mapVC.mMapVC clearForlogin];
    [wantVC clearForlogin];
    [[RunTimeShare sharedInstance] clearForLogin];
    
    [mapVC.mMapVC loginSuccess];
    
    NSArray * arr = [[ClientJSONObject clientJSONObject] getOrderListWithTel:[RunTimeShare getUserTel]];
    if (arr && arr.count > 0) {
        if (customBadge) {
            [customBadge removeFromSuperview];
            customBadge = nil;
        }
        customBadge = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%d",arr.count]
                                          withStringColor:[UIColor whiteColor]
                                           withInsetColor:[UIColor redColor]
                                           withBadgeFrame:YES
                                      withBadgeFrameColor:[UIColor whiteColor]
                                                withScale:1.0
                                              withShining:YES];
       
        customBadge.frame = CGRectMake(280, 400, customBadge.frame.size.width, customBadge.frame.size.height);
        [self.view insertSubview:customBadge aboveSubview:myTabarView];
        [orderManageVC addBadge:arr.count];
        [orderManageVC addQRImage];
    }
    
    //登陆成功后检测版本更新
    [self VersionDetection];
}

//进入注册界面
-(void)enterRegisterVC{
    RegisterVC * registerVC = [[RegisterVC alloc] init];
    registerVC.registerVCDelegate = self;
    [self.navigationController pushViewController:registerVC animated:YES];
}

//进入找回密码界面
-(void)enterForgetPassWordVC{
    ForgetPasswordVC * forgetVC = [[ForgetPasswordVC alloc] init];
    [self.navigationController pushViewController:forgetVC animated:YES];
}

#pragma mark - 处理注册界面的协议

//注册成功，则将登录界面去掉
-(void)didRegisterSubmitSuccess{
    [loginVC.view removeFromSuperview];
    loginVC = nil;
}

#pragma mark - 有关地图VC界面的协议

//进入到收费标准的界面
-(void)UserWantEnterStandardVC{
    if (standVC) {
        standVC = nil;
    }
    standVC = [[StandardVC alloc] init];
    standVC.view.alpha = 0.0;
    standVC.view.frame = CGRectMake(0.0, 0.0, 320.0, 460.0);
    standVC.standarVcDelegate = self;
    [self.view addSubview:standVC.view];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5f];
    [standVC.view setAlpha:1.0];
    [UIView commitAnimations];
}

//进入到设置的界面
-(void)UserWantEnterSettingVC{
    if (settingVC) {
        settingVC = nil;
    }
    settingVC = [[SettingVC alloc] init];
    settingVC.view.alpha = 0.0;
    settingVC.view.frame = CGRectMake(0.0, 0.0, 320.0, 460.0);
    settingVC.setDelegate = self;
    [self.view addSubview:settingVC.view];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5f];
    [settingVC.view setAlpha:1.0];
    [UIView commitAnimations];

}

-(void)UserWantEnterSearchVC{
    MapSearchVC * vc = [[MapSearchVC alloc] init];
    vc.vcDelegate = mapVC.mMapVC;
    [self.navigationController pushViewController:vc animated:TRUE];
}

-(void)UserWantEnterWantVC:(NSDictionary*)addrDict{
    [wantVC setStartAddr:addrDict];
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = 1000+1;
    [self clickOnTabarBut:btn];
}

#pragma mark - 收费标准界面的协议
-(void)StandardVcDidDismiss{
    if (standVC) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5f];
        [standVC.view setAlpha:0.0];
        [UIView commitAnimations];
    }
}

#pragma mark - 设置界面的协议
-(void)SettingVCDidDismiss{
    if (settingVC) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5f];
        [settingVC.view setAlpha:0.0];
        [UIView commitAnimations];
    }
}

//在设置界面中退出登录
-(void)UserClickOnLogoutBut{
    if (loginVC) {
        loginVC = nil;
    }
    loginVC = [[LoginMainVC alloc] init];
    loginVC.view.frame = CGRectMake(0.0, 0.0, 320.0, 460.0);
    loginVC.loginDelegate = self;
    [self.view addSubview:loginVC.view];
    [loginVC.view release];
    
}

//在设置界面中进入不同的VC
-(void)EnterSettingDetailVCWithTag:(int)curTag{
    switch (curTag) {
        case 0:
        {
           //进入个人设置界面
            PersonSettingVC * personVC = [[PersonSettingVC alloc] init];
            [self.navigationController pushViewController:personVC animated:YES];
        }
            break;
        case 1:
        {
            //进入系统设置界面
            SystemSettingVC * systemVC = [[SystemSettingVC alloc] init];
            [self.navigationController pushViewController:systemVC animated:YES];
        }
            break;
        case 2:
        {
            //进入系统消息界面
            SystemMessageVC * messageVC = [[SystemMessageVC alloc] init];
            [self.navigationController pushViewController:messageVC animated:YES];
        }
            break;
        case 3:
        {
            //进入分享界面
            ShareViewController * shareVC = [[ShareViewController alloc] init];
            shareVC.shareDelegate = self;
            [self.navigationController pushViewController:shareVC animated:YES];
        }
            break;
        default:
            break;
    }
}

#pragma mark - 下单页面协议处理
-(void)enterChoseLocationVC{
    ChoseLocationVC * vc = [[ChoseLocationVC alloc] init];
    vc.choseLocationVCDelegate = wantVC;
    [self.navigationController pushViewController:[vc autorelease] animated:TRUE];
}

-(void)enterUsedAddressVC{
    UsedAddressVC * vc = [[UsedAddressVC alloc] init];
    vc.vcDelegate = wantVC;
    [self.navigationController pushViewController:[vc autorelease] animated:TRUE];
}

-(void)enterSearchVC{
    MapSearchVC * vc = [[MapSearchVC alloc] init];
    vc.vcDelegate = wantVC;
    [self.navigationController pushViewController:vc animated:TRUE];
}

- (void)enterDriverInfoVC:(NSDictionary*)dicDriver{
    DriverInfoViewController *infoVC = [[DriverInfoViewController alloc] init];
    infoVC.mdicDriver = dicDriver;
    [self.navigationController pushViewController:infoVC animated:YES];
}

-(void)enterMapVC{
    [mapVC.mMapVC clearForlogin];
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = 1000+0;
    [self clickOnTabarBut:btn];
}
#pragma mark - 分享代理
-(void)enterShortMsgVC{
    
}

#pragma mark - 订单管理协议
-(void)enterReserveListVC{
    [customBadge removeFromSuperview];
    customBadge = nil;
    ReserveListVC * vc = [[ReserveListVC alloc] init];
    [self.navigationController pushViewController:vc animated:TRUE];
}

-(void)enterConsumeListVC{
    ConsumeListVC * vc = [[ConsumeListVC alloc] init];
    [self.navigationController pushViewController:vc animated:TRUE];
}

-(void)enterChoseGuestVC{
    LogInfo(@"");
    ABPeoplePickerNavigationController * picker = nil;
    
    if(!picker){
        picker = [[ABPeoplePickerNavigationController alloc] init];
        // place the delegate of the picker to the controll
        picker.peoplePickerDelegate = self;
    }
    // showing the picker
    [self presentModalViewController:picker animated:YES];
    [picker release];
}

-(void)enterSendMailVC:(NSString *)base64Img{
    // send mail
    if ([MFMailComposeViewController canSendMail]) {
        //Create a string with HTML formatting for the email body
        NSMutableString *emailBody = [[NSMutableString alloc] initWithString:@"<html><body>"];
        //Add some text to it however you want
        [emailBody appendString:@"<p>请客二维码图片：</p>"];
        //Pick an image to insert
        
        //Don't forget the "<b>" tags are required, the "<p>" tags are optional
        [emailBody appendString:[NSString stringWithFormat:@"<p><b><img src='data:image/png;base64,%@'></b></p>",base64Img]];
        //You could repeat here with more text or images, otherwise
        //close the HTML formatting
        [emailBody appendString:@"</body></html>"];
        //NSLog(@"%@",emailBody);
        
        //Create the mail composer window
        MFMailComposeViewController *emailDialog = [[MFMailComposeViewController alloc] init];
        emailDialog.mailComposeDelegate = orderManageVC;
        [emailDialog setSubject:@"智驾通——请客二维码"];
        [emailDialog setMessageBody:[emailBody autorelease] isHTML:YES];
        
        [self presentModalViewController:[emailDialog autorelease] animated:YES];
        LogInfo(@"Email VC show up");
    } else {
        NSLog(@"Device is unable to send email in its current state.");
    }
}


#pragma mark - 通讯录协议
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    // assigning control back to the main controller
    [peoplePicker dismissModalViewControllerAnimated:YES];
}

- (BOOL)peoplePickerNavigationController: (ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    
    [peoplePicker dismissModalViewControllerAnimated:NO];
    
    if (property == kABPersonPhoneProperty) {
        
        ABMutableMultiValueRef phoneMulti = ABRecordCopyValue(person, property);
        int index = ABMultiValueGetIndexForIdentifier(phoneMulti,identifier);
        NSString *phone = (NSString*)ABMultiValueCopyValueAtIndex(phoneMulti, index);
        //do something
        
        //[self sendQRImgToGuest:phone];
        [self performSelectorOnMainThread:@selector(sendQRImgToGuest:) withObject:phone waitUntilDone:TRUE];
        [phone release];
    }
    return NO;
}

-(void)sendQRImgToGuest:(NSString*)guestPhone{
    LogInfo(@"guest phone:%@",guestPhone);
    NSString * tel = [RunTimeShare getUserTel];
    NSDictionary * dict = [[ClientJSONObject clientJSONObject] getGuestDCodeWithTel:tel guestTel:guestPhone];
    if ([[dict valueForKey:@"dimensionalCode"] integerValue] != -2) {
        
        
        //generate QRCode image and send
        UIImage * qrImg = [RunTimeShare generateQRImageWithStr:[dict valueForKey:@"dimensionalCode"] size:250];
        
        //Convert the image into data
        NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(qrImg)];
        //Create a base64 string representation of the data using NSData+Base64
        NSString *base64String = [imageData base64EncodedString];
        
        [self enterSendMailVC:base64String];
    }
}


#pragma mark - add by caoyanbo
- (void)updateLocationNotify:(NSNotification *)note
{
    NSLog(@"Received notification: %@", note);
    PositionTxt.text = [NSString stringWithFormat:@"您当前位置:%@",[self getCurPosition]];
    [wantVC updateUserAddr];
}
@end
