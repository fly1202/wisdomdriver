//
//  AgentListVC.m
//  WisdomDriver
//
//  Created by  apple on 13-1-13.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "AgentListVC.h"
#import "ClientJSONObject.h"
#import "AgentInfoVC.h"

@interface AgentListVC () <UITableViewDataSource,UITableViewDelegate,AgentInfoVCDelegate>
{
    UITableView * agentTable;
    NSMutableArray * agentArray;
}
@end

@implementation AgentListVC
@synthesize agentListVCDelegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //self.view.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0];
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    //顶部条栏和文字处理，取消按钮,提交按钮
    [self addTopViewWithTitle:@"代理商列表"];
    
    //代理商列表
    [self addAgentTable];
    
    [self loadAgentList];
}

#pragma mark - 添加顶部条栏和文字,取消按钮,提交按钮
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //取消按钮
    UIButton *cancelBut = [[UIButton alloc] initWithFrame:CGRectMake(3.0, 8.0, 60.0, 30.0)];
    [cancelBut setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"navBarRightButBottom.png"] forState:UIControlStateNormal];
    [cancelBut addTarget:self action:@selector(clickOnCancelBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelBut];
    [cancelBut release];
}

-(void)addAgentTable{
    agentTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 46, 320, 460-46) style:UITableViewStylePlain];
    agentTable.dataSource = self;
    agentTable.delegate = self;
    [self.view addSubview:agentTable];
}

-(void)loadAgentList{
    NSString * cityCode = @"0571";
    NSArray * array = [[ClientJSONObject clientJSONObject] getAgentListWithCityCode:cityCode];
    if (agentArray == nil) {
        agentArray = [[NSMutableArray alloc] init];
    }
    [agentArray removeAllObjects];
    [agentArray addObjectsFromArray:array];
    [agentTable reloadData];
}

#pragma mark tableView delegete
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 155.0;
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //进入代理商详情页面
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary* agentDict = [agentArray objectAtIndex:indexPath.row];
    AgentInfoVC * agentVC = [[AgentInfoVC alloc] initWithBasicDict:agentDict];
    agentVC.agentInfoVCDelegate = self;
    [self.navigationController pushViewController:agentVC animated:YES];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [agentArray count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * cellIdentifier=@"contentCell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.userInteractionEnabled=YES;
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle=UITableViewCellSelectionStyleGray;
    
    NSDictionary *dictionary=[agentArray objectAtIndex:indexPath.row];
    
    //{"agentID":"12345","agentName":"代理商2","agentAddress":"杭州","agentLogo":" images /b.jpg"}
    
    NSString *url=[NSString stringWithFormat:@"%@",[dictionary objectForKey:@"agentLogo"]];
    cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
    cell.textLabel.text = [dictionary objectForKey:@"agentName"];
    cell.detailTextLabel.text = [dictionary objectForKey:@"agentAddress"];
    
    return cell;
}

#pragma mark - AgentInfoVCDelegate
-(void)didSetDefaultAgent:(NSDictionary *)agent{
    
    if (agentListVCDelegate != nil && [agentListVCDelegate respondsToSelector:@selector(didSelectAgent:)]) {
        [agentListVCDelegate didSelectAgent:agent];
    }
    
    //[self.navigationController popViewControllerAnimated:TRUE];
}

//取消注册
-(void)clickOnCancelBut{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
