//
//  LoginMainVC.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-7.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "LoginMainVC.h"
#import "ClientJSONObject.h"
#import <QuartzCore/QuartzCore.h>
#import "SVProgressHUD.h"
#import "RunTimeShare.h"

#define nameFieldTag  100
#define pdFieldTag    200

@interface LoginMainVC (){
    UITextField *  nameField;
    UITextField *  pdField;
    
    //记住密码
    UIImageView * recordPdView;
    BOOL isRecordPd;
    
    //自动登录
    UIImageView * autoView;
    BOOL isAutoLogin;
}

-(void)addTopViewWithTitle:(NSString *)title;
-(void)addLoginTextField;
-(void)loginSetting;
-(void)addLoginAndRegisterButView;

@end

@implementation LoginMainVC
@synthesize loginDelegate;

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //self.view.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0];
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    //顶部条栏和文字处理
    [self addTopViewWithTitle:@"登录"];
    
    //登录名字和密码输入框
    [self addLoginTextField];
    
    //登录的纪录设置
    [self loginSetting];
    
    //登录注册按钮设置
    [self addLoginAndRegisterButView];
    
    //判断是否自动登陆
    if (isAutoLogin) {
        [self clickOnLoginBut];
    }
}

#pragma mark - 添加顶部条栏和文字
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
}

#pragma mark - 添加登录名字和密码输入框
-(void)addLoginTextField{
    //登录时的名称
    UIImageView * bottomView1 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 55.0, 300.0, 45.0)];
    [bottomView1 setImage:[UIImage imageNamed:@"bg_customtable1.png"]];
    bottomView1.userInteractionEnabled = TRUE;
    
    nameField = [[UITextField alloc] initWithFrame:CGRectMake(10.0, 0.0, 290.0, 45.0)];
    nameField.placeholder = @"请输入手机号";
    nameField.delegate = self;
    nameField.borderStyle = UITextBorderStyleNone;
    nameField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    nameField.tag = nameFieldTag;
    [bottomView1 addSubview:nameField];
    [self.view addSubview:bottomView1];
    [nameField release];
    [bottomView1 release];
    
    //登录时的密码
    UIImageView * bottomView2 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 115.0, 300.0, 45.0)];
    [bottomView2 setImage:[UIImage imageNamed:@"bg_customtable1.png"]];
    bottomView2.userInteractionEnabled = TRUE;
    
    pdField = [[UITextField alloc] initWithFrame:CGRectMake(10.0, 0.0, 290.0, 45.0)];
    pdField.placeholder = @"请输入密码";
    pdField.delegate = self;
    pdField.borderStyle = UITextBorderStyleNone;
    pdField.secureTextEntry = YES;
    pdField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    [bottomView2 addSubview:pdField];
    [self.view addSubview:bottomView2];
    pdField.tag = pdFieldTag;
    [pdField release];
    
    //是否已经记住了密码
    isRecordPd = [RunTimeShare isLoginRecord];
    if (isRecordPd) {
        nameField.text = (NSString *)[RunTimeShare getUserTel];
        pdField.text = (NSString *)[RunTimeShare getUserPassword];
    }
}

#pragma mark - 输入框UITextField的协议处理

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
}

//按下Done按钮的调用方法，我们让键盘消失
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSCharacterSet *cs;
    if(textField.tag == pdFieldTag)
    {
        cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        BOOL basicTest = [string isEqualToString:filtered];
        if(!basicTest)
        {
            [self dismissErrorMsg:@"请输入数字或者字母"];
            return NO;
        }
    }
    
    //其他的类型不需要检测，直接写入
    return YES;
}

#pragma mark - 登录的纪录设置
-(void)loginSetting{
    //是否已经记住了密码
    isRecordPd = [RunTimeShare isLoginRecord];
    //记住密码
    recordPdView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 170.0, 40.0, 44.0)];
    recordPdView.image = [UIImage imageNamed:@"acc_btnyuandefault1.png"];
    if (isRecordPd) {
        recordPdView.image = [UIImage imageNamed:@"acc_btnyuanselect2.png"];
    }
    [self.view addSubview:recordPdView];
    [recordPdView release];
    
    UILabel * recordLbl = [[UILabel alloc] initWithFrame:CGRectMake(40.0, 172.0, 70.0, 40.0)];
    recordLbl.text = @"记住密码";
    recordLbl.backgroundColor = [UIColor clearColor];
    recordLbl.textAlignment = UITextAlignmentCenter;
    [self.view addSubview:recordLbl];
    [recordLbl release];
    
    UIButton * recordBut = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 163.0, 50.0, 50.0)];
    recordBut.backgroundColor = [UIColor clearColor];
    [recordBut addTarget:self action:@selector(clickOnRecordBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:recordBut];
    [recordBut release];
    
    //是否自动登录
    isAutoLogin = [RunTimeShare isAutoLogin];
    
    //自动登录
    autoView = [[UIImageView alloc] initWithFrame:CGRectMake(110.0, 170.0, 40.0, 44.0)];
    autoView.image = [UIImage imageNamed:@"acc_btnyuandefault1.png"];
    if (isAutoLogin) {
        autoView.image = [UIImage imageNamed:@"acc_btnyuanselect2.png"];
    }
    [self.view addSubview:autoView];
    [autoView release];
    
    UILabel * autoLbl = [[UILabel alloc] initWithFrame:CGRectMake(150.0, 172.0, 70.0, 40.0)];
    autoLbl.text = @"自动登录";
    autoLbl.backgroundColor = [UIColor clearColor];
    autoLbl.textAlignment = UITextAlignmentCenter;
    [self.view addSubview:autoLbl];
    [autoLbl release];
    
    UIButton * autoBut = [[UIButton alloc] initWithFrame:CGRectMake(108.0, 163.0, 50.0, 50.0)];
    autoBut.backgroundColor = [UIColor clearColor];
    [autoBut addTarget:self action:@selector(clickOnAutoBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:autoBut];
    [autoBut release];
    
    //忘记密码
    UILabel * forgetLbl = [[UILabel alloc] initWithFrame:CGRectMake(240.0, 176.0, 70.0, 30.0)];
    forgetLbl.text = @"忘记密码";
    forgetLbl.backgroundColor = [UIColor clearColor];
    forgetLbl.textAlignment = UITextAlignmentCenter;
    [self.view addSubview:forgetLbl];
    [forgetLbl release];
    
    CALayer* layer = [forgetLbl layer];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.borderColor = [UIColor darkGrayColor].CGColor;
    bottomBorder.borderWidth = 1;
    bottomBorder.frame = CGRectMake(-1, layer.frame.size.height-1, layer.frame.size.width, 1);
    [bottomBorder setBorderColor:[UIColor blackColor].CGColor];
    [layer addSublayer:bottomBorder];
    
    UIButton * forgetBut = [[UIButton alloc] initWithFrame:CGRectMake(230.0, 163.0, 90.0, 50.0)];
    forgetBut.backgroundColor = [UIColor clearColor];
    [forgetBut addTarget:self action:@selector(enterForgetPD) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:forgetBut];
    [forgetBut release];
}

//是否记住密码
-(void)clickOnRecordBut{
    isRecordPd = !isRecordPd;
    recordPdView.image = [UIImage imageNamed:@"acc_btnyuandefault1.png"];
    if (isRecordPd) {
        recordPdView.image = [UIImage imageNamed:@"acc_btnyuanselect2.png"];
    }
}

//是否自动登录
-(void)clickOnAutoBut{
    isAutoLogin = !isAutoLogin;
    autoView.image = [UIImage imageNamed:@"acc_btnyuandefault1.png"];
    if (isAutoLogin) {
        autoView.image = [UIImage imageNamed:@"acc_btnyuanselect2.png"];
        isRecordPd = TRUE;
        recordPdView.image = [UIImage imageNamed:@"acc_btnyuanselect2.png"];
    }
}

//进入忘记密码界面
-(void)enterForgetPD{
    [self.loginDelegate enterForgetPassWordVC];
}

#pragma mark - 登录，注册，直接电话
-(void)addLoginAndRegisterButView{
    //登录按钮
    UIButton * loginBut = [[UIButton alloc] initWithFrame:CGRectMake(10.0, 230.0, 300.0, 44.0)];
    [loginBut setImage:[UIImage imageNamed:@"btn_loadingdefault1.png"] forState:UIControlStateNormal];
    [loginBut setImage:[UIImage imageNamed:@"btn_loadingselected2.png"] forState:UIControlStateSelected];
    [loginBut addTarget:self action:@selector(clickOnLoginBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginBut];
    [loginBut release];
    
    UILabel * loginLbl = [[UILabel alloc] initWithFrame:CGRectMake(110.0, 230.0, 100.0, 44.0)];
    loginLbl.text = @"登录";
    loginLbl.backgroundColor = [UIColor clearColor];
    loginLbl.textAlignment = UITextAlignmentCenter;
    loginLbl.font = [UIFont systemFontOfSize:18.0];
    loginLbl.textColor = [UIColor whiteColor];
    [self.view addSubview:loginLbl];
    [loginLbl release];
    
    //注册按钮
    UIButton * registerBut = [[UIButton alloc] initWithFrame:CGRectMake(10.0, 285.0, 300.0, 44.0)];
    [registerBut setImage:[UIImage imageNamed:@"btn_zhucedefault2.png"] forState:UIControlStateNormal];
    [registerBut setImage:[UIImage imageNamed:@"acc_btnyuanselected2.png"] forState:UIControlStateSelected];
    [registerBut addTarget:self action:@selector(clickOnRegisterBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:registerBut];
    [registerBut release];
    
    UILabel * registerLbl = [[UILabel alloc] initWithFrame:CGRectMake(110.0, 285.0, 100.0, 44.0)];
    registerLbl.text = @"注册";
    registerLbl.backgroundColor = [UIColor clearColor];
    registerLbl.textAlignment = UITextAlignmentCenter;
    registerLbl.font = [UIFont systemFontOfSize:18.0];
    [self.view addSubview:registerLbl];
    [registerLbl release];
    
    //预约按钮
    UIButton * orderBut = [[UIButton alloc] initWithFrame:CGRectMake(10.0, 340.0, 300.0, 44.0)];
    [orderBut setImage:[UIImage imageNamed:@"btn_zhucedefault2.png"] forState:UIControlStateNormal];
    [orderBut setImage:[UIImage imageNamed:@"acc_btnyuanselected2.png"] forState:UIControlStateSelected];
    [orderBut addTarget:self action:@selector(clickOnOrderBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:orderBut];
    [orderBut release];
    
    UILabel * orderLbl = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 340.0, 200.0, 44.0)];
    orderLbl.text = @"直接电话预约";
    orderLbl.backgroundColor = [UIColor clearColor];
    orderLbl.textAlignment = UITextAlignmentCenter;
    orderLbl.font = [UIFont systemFontOfSize:18.0];
    [self.view addSubview:orderLbl];
    [orderLbl release];
}

//点击登录
-(void)clickOnLoginBut{
    //登录流程如下：提交登录信息，根据返回的值判断是否登录成功。如果成功，则纪录用户设置并退出登录界面；如果不成功，则提示说明不成功
    
    //目前没有接上接口去登录，暂时放在这里纪录
    [RunTimeShare setIsLoginRecord:isRecordPd];
    [RunTimeShare setIsAutoLogin:isAutoLogin];
    
    if ([nameField.text length] == 0 || [pdField.text length] == 0) {
        [self dismissErrorMsg:@"请输入手机号和密码后再登陆"];
        return;
    }
    
    [self showWithStatusMsg:@"正在登录，请稍后..."];
    [self performSelector:@selector(login) withObject:nil afterDelay:0.5];
}

- (void)login{
    
    NSDictionary * dict = [[ClientJSONObject clientJSONObject] loginWithTel:nameField.text pwd:pdField.text];
    //并假设已经登录成功，协议通知退出登录界面
    //[self.loginDelegate loginDidSuccess];
    
    if ([[dict valueForKey:@"result"] integerValue] == 1) {
        
        [RunTimeShare setUserTel:nameField.text];
        
        if (isRecordPd) {
            [RunTimeShare setUserPassword:pdField.text];
        }else{
            [RunTimeShare setUserPassword:@""];
        }
        
        NSDictionary * personDict = [[ClientJSONObject clientJSONObject] getUserInfoWithTel:nameField.text];
        [RunTimeShare setUserInfo:personDict];
        
        [self dismiss];
        //并假设已经登录成功，协议通知退出登录界面
        [self.loginDelegate loginDidSuccess];
    }else if([[dict valueForKey:@"result"] integerValue] == 0){
        [self dismissErrorMsg:@"用户名密码错误"];
    }else{
        [self dismissErrorMsg:@"登录失败，请重新登录！"];
    }
}

//点击注册
-(void)clickOnRegisterBut{
    [self.loginDelegate enterRegisterVC];
}

//点击电话预约
-(void)clickOnOrderBut{
    //目前使用的是君韵科技公司的电话
    NSString *phone = @"tel://4001120120";
    UIWebView*callWebview =[[UIWebView alloc] init];
    NSURL *telURL =[NSURL URLWithString:phone];
    [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
    [self.view addSubview:callWebview];
    [callWebview release];
}

#pragma - mark 键盘隐藏
-(void)touchesEnded: (NSSet *)touches withEvent: (UIEvent *) event {
    NSLog(@"touchesEnded:withEvent:");
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([nameField isFirstResponder] && [touch view] != nameField) {
        [nameField resignFirstResponder];
    }else if ([pdField isFirstResponder] && [touch view] != pdField) {
        [pdField resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

#pragma mark -
#pragma mark 显示提醒框

- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框

- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}
@end
