//
//  ViewAgreementVC.m
//  WisdomDriver
//
//  Created by  apple on 13-1-29.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "ViewAgreementVC.h"
#import "defines.h"

@interface ViewAgreementVC ()

@end

@implementation ViewAgreementVC

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    //顶部条栏和文字处理，取消按钮,提交按钮
    [self addTopViewWithTitle:@"代驾协议"];
    [self addAgreementCtrl];
}

#pragma mark - 添加顶部条栏和文字,取消按钮,提交按钮
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //取消按钮
    UIButton *cancelBut = [[UIButton alloc] initWithFrame:CGRectMake(3.0, 8.0, 60.0, 30.0)];
    [cancelBut setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"navBarRightButBottom.png"] forState:UIControlStateNormal];
    [cancelBut addTarget:self action:@selector(clickOnCancelBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelBut];
    [cancelBut release];
}
//取消注册
-(void)clickOnCancelBut{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addAgreementCtrl{
    // Do any additional setup after loading the view from its nib.
    NSString *htmlFile = [[NSBundle mainBundle]
                          pathForResource:[kAgreementText stringByDeletingPathExtension]
                          ofType:@"html"
                          inDirectory:nil];
    NSLog(@"html file path:%@", htmlFile);
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile
                                                     encoding:NSUTF8StringEncoding
                                                        error:nil];
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    
    UIWebView * webView = [[UIWebView alloc] initWithFrame:CGRectMake(0.0, 46.0, 320.0, 413.0)];
    webView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:webView];
    [webView release];
    
    [webView loadHTMLString:htmlString baseURL:baseURL];
}

@end
