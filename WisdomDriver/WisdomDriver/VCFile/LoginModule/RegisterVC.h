//
//  RegisterVC.h
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-7.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RegisterVCDelegate <NSObject>

-(void)didRegisterSubmitSuccess;

@end

@interface RegisterVC : UIViewController{
    id <RegisterVCDelegate> registerVCDelegate;
}

@property (nonatomic,assign) id <RegisterVCDelegate> registerVCDelegate;

@end
