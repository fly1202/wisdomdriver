//
//  LoginMainVC.h
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-7.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoginMainVCDelegate <NSObject>

//登录成功
-(void)loginDidSuccess;

//进入注册界面
-(void)enterRegisterVC;

//进入找回密码界面
-(void)enterForgetPassWordVC;

@end

@interface LoginMainVC : UIViewController<UITextFieldDelegate>{
    id <LoginMainVCDelegate> loginDelegate;
}

@property (nonatomic,assign) id <LoginMainVCDelegate> loginDelegate;

@end
