//
//  AgentInfoVC.m
//  WisdomDriver
//
//  Created by  apple on 13-1-13.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "AgentInfoVC.h"
#import "ClientJSONObject.h"
#import "RunTimeShare.h"
#import "SVProgressHUD.h"
#import "defines.h"
#import "StandardVC.h"
#import <QuartzCore/QuartzCore.h>

@interface AgentInfoVC ()<StandardVCDelegate>
{
    NSMutableDictionary * agentDict;
}
@end

@implementation AgentInfoVC

@synthesize agentInfoVCDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithBasicDict:(NSDictionary*)dict{
    self = [super init];
    agentDict = [[NSMutableDictionary alloc] initWithDictionary:dict];
    return  self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    //顶部条栏和文字处理，取消按钮,提交按钮
    [self addTopViewWithTitle:@"代理商详情"];
    
    NSDictionary * dict = [[ClientJSONObject clientJSONObject]
                           getAgentBasicInfoWithID:[agentDict valueForKey:@"agentID"]];
    for(NSString *aKey in dict) {
        [agentDict setValue:[dict valueForKey:aKey] forKey:aKey];
    }
    [self addInfoCtrl];
    //{"agentLogo":"/a.jpg","agentName":"代理商1","agentAddress":"杭州","ratestandard":"收费标准","introduction":"代理商1是一家……的公司"}
}

#pragma mark - 添加顶部条栏和文字,取消按钮,提交按钮
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //取消按钮
    UIButton *cancelBut = [[UIButton alloc] initWithFrame:CGRectMake(3.0, 8.0, 60.0, 30.0)];
    [cancelBut setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"navBarRightButBottom.png"] forState:UIControlStateNormal];
    [cancelBut addTarget:self action:@selector(clickOnCancelBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelBut];
    [cancelBut release];
}

-(void)addInfoCtrl{
    
    UIImageView * bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 46, 320, 60)];
    [bgView setImage:[UIImage imageNamed:@"bg_customtable5.png"]];
    //bgView.backgroundColor = [UIColor grayColor];
    [self.view addSubview:bgView];
    [bgView release];
    
    // logo
    UIImageView * logoView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 40, 40)];
    NSString *url=[NSString stringWithFormat:@"%@",[agentDict objectForKey:@"agentLogo"]];
    logoView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
    //logoView.image = [UIImage imageNamed:@"icon.png"];
    [bgView addSubview:logoView];
    [logoView release];
    // Get the Layer of any view
    CALayer * l = [logoView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:10.0];
    
    UILabel * nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 10, 250, 20)];
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.text = [agentDict objectForKey:@"agentName"];
    nameLabel.font = [UIFont systemFontOfSize:14.0];
    [bgView addSubview:nameLabel];
    [nameLabel release];
    
    UILabel * addrLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 30, 250, 20)];
    addrLabel.backgroundColor = [UIColor clearColor];
    addrLabel.text = [NSString stringWithFormat:@"地址：%@",[agentDict objectForKey:@"agentAddress"]];
    addrLabel.font = [UIFont systemFontOfSize:14.0];
    [bgView addSubview:addrLabel];
    [addrLabel release];
    
    //设置代理按钮
    UIButton * setAgentBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, 120, 300, 45)];
    [setAgentBtn setImage:[UIImage imageNamed:@"morenshangjia_btndefault1.png"] forState:UIControlStateNormal];
    [setAgentBtn setImage:[UIImage imageNamed:@"morenshangjia_btnselected2.png"] forState:UIControlStateSelected];
    [setAgentBtn addTarget:self action:@selector(setAgentBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:setAgentBtn];
    [setAgentBtn release];
    
    UILabel * btnLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 7, 300, 30)];
    btnLbl.backgroundColor = [UIColor clearColor];
    btnLbl.textColor = [UIColor whiteColor];
    btnLbl.textAlignment = UITextAlignmentCenter;
    btnLbl.text = @"设为默认服务商家";
    btnLbl.font = [UIFont systemFontOfSize:18.0];
    [setAgentBtn addSubview:btnLbl];
    [btnLbl release];
    
    //加载收费标准按钮
    UIImageView * bottomView2 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 185.0, 300.0, 45.0)];
    bottomView2.image = [UIImage imageNamed:@"bg_customtable1.png"];
    bottomView2.userInteractionEnabled = YES;
    [self.view addSubview:bottomView2];
    [bottomView2 release];
    
    UILabel * txtLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, 110.0, 45.0)];
    txtLbl.backgroundColor = [UIColor clearColor];
    txtLbl.text = @"收费标准";
    [bottomView2 addSubview:txtLbl];
    [txtLbl release];
    
    UIImageView * arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 15.0, 10.0, 14.0)];
    arrowView.image = [UIImage imageNamed:@"acc_jiantou.png"];
    [bottomView2 addSubview:arrowView];
    [arrowView release];
    
    UIButton * rateBut = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 300.0, 45.0)];
    rateBut.backgroundColor = [UIColor clearColor];
    rateBut.tag = 1002;
    [rateBut addTarget:self action:@selector(clickOnRateBut) forControlEvents:UIControlEventTouchUpInside];
    [bottomView2 addSubview:rateBut];
    [rateBut release];
    
    //简介title
    UIImageView * introImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 250, 320, 30)];
    [introImgView setImage:[UIImage imageNamed:@"hengtiaolang_tltle.png"]];
    [self.view addSubview:introImgView];
    [introImgView release];
    
    UILabel * titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 5.0, 110.0, 20.0)];
    titleLbl.backgroundColor = [UIColor clearColor];
    titleLbl.text = @"简介：";
    titleLbl.textColor = [UIColor whiteColor];
    titleLbl.font = [UIFont systemFontOfSize:15.0];
    [introImgView addSubview:titleLbl];
    [titleLbl release];
    
    //简介内容
    UITextView * textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 280, 320, 460-280)];
    textView.editable = FALSE;
    textView.textColor = [UIColor colorWithRed:86.0/255.0 green:86.0/255.0 blue:86.0/255.0 alpha:1.0];
    textView.text = [agentDict valueForKey:@"introduction"];
    //textView.text = @"代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍代理商介绍";
    textView.editable = FALSE;
    textView.font = [UIFont systemFontOfSize:15.0];
    [self.view addSubview:textView];
    [textView release];
}

#pragma - mark 设置代理
-(void)setAgentBtnClick{
    
    NSString * agentID = [agentDict valueForKey:@"agentID"];
    NSString * tel = [RunTimeShare getUserTel];
    
    [self showWithStatusMsg:@"正在修改代理商"];
    NSDictionary * dict = [[ClientJSONObject clientJSONObject] changeAgentWithTel:tel agentID:agentID];
    if ([[dict valueForKey:@"result"] integerValue] == 1) {
        [self dismissSuccessMsg:@"修改代理商成功"];
        [RunTimeShare setAgentID:agentID];
        if (agentInfoVCDelegate != nil && [agentInfoVCDelegate respondsToSelector:@selector(didSetDefaultAgent:)]) {
            [agentInfoVCDelegate didSetDefaultAgent:agentDict];
        }
    }else{
        [self dismissErrorMsg:@"修改代理商失败，请重试"];
    }
}

#pragma - mark 收费标准
-(void)clickOnRateBut{
    StandardVC * vc = [[StandardVC alloc] init];
    vc.standarVcDelegate = self;
    [self.navigationController pushViewController:vc animated:TRUE];
}

#pragma - mark 返回
-(void)clickOnCancelBut{
    [self.navigationController popViewControllerAnimated:TRUE];
}

-(void)StandardVcDidDismiss{
    [self.navigationController popViewControllerAnimated:TRUE];
}

#pragma mark -
#pragma mark 显示提醒框

- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框

- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}

@end
