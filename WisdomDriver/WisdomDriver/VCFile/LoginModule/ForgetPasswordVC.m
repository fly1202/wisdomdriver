//
//  ForgetPasswordVC.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-7.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "ForgetPasswordVC.h"
#import "ClientJSONObject.h"
#import "SVProgressHUD.h"

#define nameFieldTag        100
#define pdFieldTag          200
#define pdAgainFieldTag     300
#define telFieldTag         400
#define vCodeFieldTag       500

@interface ForgetPasswordVC (){
    
    UITextField *  pdField;
    UITextField *  pdAgainField;
    UITextField *  vCodeField;
    UITextField *  telField;
    NSString *      sessionID;
    BOOL            isGetVCode;
}

@end

@implementation ForgetPasswordVC

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        isGetVCode = FALSE;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //self.view.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0];
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    //顶部条栏和文字处理，取消按钮
    [self addTopViewWithTitle:@"找回密码"];

    [self addContentControl];
}

#pragma mark - 添加顶部条栏和文字,取消按钮
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //取消按钮
    UIButton *cancelBut = [[UIButton alloc] initWithFrame:CGRectMake(3.0, 8.0, 58.0, 30.0)];
    [cancelBut setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"btn_duankdefault1.png"] forState:UIControlStateNormal];
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"btn_duankselected2.png"] forState:UIControlStateSelected];
    [cancelBut addTarget:self action:@selector(clickOnCancelBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelBut];
    [cancelBut release];
}

-(void)clickOnCancelBut{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - ------add by caoyanbo-------
#pragma mark - 添加注册使用的各个控件
-(void)addContentControl{
    //手机号
    UIImageView * bottomView1 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 55.0, 300.0, 45.0)];
    [bottomView1 setImage:[UIImage imageNamed:@"bg_customtable1.png"]];
    bottomView1.userInteractionEnabled = TRUE;
    
    telField = [[UITextField alloc] initWithFrame:CGRectMake(10.0, 0.0, 290.0, 45.0)];
    telField.placeholder = @"请输入手机号码";
    telField.delegate = self;
    telField.borderStyle = UITextBorderStyleNone;
    //telField.keyboardType = UIKeyboardTypePhonePad;
    telField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    [bottomView1 addSubview:telField];
    [self.view addSubview:bottomView1];
    telField.tag = telFieldTag;
    [telField release];
    
    //验证码
    UIImageView * bottomView2 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 115.0, 300.0, 45.0)];
    [bottomView2 setImage:[UIImage imageNamed:@"bg_customtable1.png"]];
    bottomView2.userInteractionEnabled = TRUE;
    vCodeField = [[UITextField alloc] initWithFrame:CGRectMake(10.0, 0.0, 290.0, 45.0)];
    vCodeField.placeholder = @"请输入验证码";
    vCodeField.delegate = self;
    vCodeField.borderStyle = UITextBorderStyleNone;
    vCodeField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    [bottomView2 addSubview:vCodeField];
    [self.view addSubview:bottomView2];
    vCodeField.tag = vCodeFieldTag;
    [vCodeField release];
    
    //验证码获取按钮
    UIButton * getvCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [getvCodeBtn addTarget:self action:@selector(getvCodeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [getvCodeBtn setFrame:CGRectMake(220, 123, 80, 30)];
    [getvCodeBtn setImage:[UIImage imageNamed:@"yanzhengma_btn.png"] forState:UIControlStateNormal];
    [getvCodeBtn setImage:[UIImage imageNamed:@"yanzhengma_btn2.png"] forState:UIControlStateSelected];
    [self.view addSubview:getvCodeBtn];
    
    UILabel * getvCodeLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 30)];
    getvCodeLb.backgroundColor = [UIColor clearColor];
    getvCodeLb.text = @"获取验证码";
    getvCodeLb.textColor = [UIColor whiteColor];
    getvCodeLb.font = [UIFont systemFontOfSize:12];
    getvCodeLb.textAlignment = UITextAlignmentCenter;
    [getvCodeBtn addSubview:getvCodeLb];
    [getvCodeLb release];
    
    //输入新密码
    UIImageView * bottomView0 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 175.0, 300.0, 45.0)];
    bottomView0.image = [UIImage imageNamed:@"bg_customtable1.png"];
    bottomView0.userInteractionEnabled = YES;
    [self.view addSubview:bottomView0];
    [bottomView0 release];
    
    UILabel * pdLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 90, 45)];
    pdLb.backgroundColor = [UIColor clearColor];
    pdLb.textAlignment = UITextAlignmentRight;
    pdLb.text = @"请输新密码:";
    [bottomView0 addSubview:pdLb];
    [pdLb release];
    
    pdField = [[UITextField alloc] initWithFrame:CGRectMake(100.0, 0.0, 260.0, 45.0)];
    //pdField.placeholder = @"请输新密码";
    pdField.delegate = self;
    pdField.secureTextEntry = YES;
    pdField.borderStyle = UITextBorderStyleNone;
    pdField.backgroundColor = [UIColor clearColor];
    pdField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    [bottomView0 addSubview:pdField];
    pdField.tag = pdFieldTag;
    [pdField release];
    
    //重复密码
    UIImageView * bottomView3 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 235.0, 300.0, 45.0)];
    bottomView3.image = [UIImage imageNamed:@"bg_customtable1.png"];
    bottomView3.userInteractionEnabled = YES;
    [self.view addSubview:bottomView3];
    [bottomView3 release];
    
    UILabel * pdAgainLb = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 90, 45)];
    pdAgainLb.backgroundColor = [UIColor clearColor];
    pdAgainLb.textAlignment = UITextAlignmentRight;
    pdAgainLb.text = @"重复密码：";
    [bottomView3 addSubview:pdAgainLb];
    [pdAgainLb release];
    
    pdAgainField = [[UITextField alloc] initWithFrame:CGRectMake(100.0, 0.0, 260.0, 45.0)];
    //pdField.placeholder = @"请输新密码";
    pdAgainField.delegate = self;
    pdAgainField.secureTextEntry = YES;
    pdAgainField.borderStyle = UITextBorderStyleNone;
    pdAgainField.backgroundColor = [UIColor clearColor];
    pdAgainField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    [bottomView3 addSubview:pdAgainField];
    pdAgainField.tag = pdAgainFieldTag;
    [pdAgainField release];

    //提交按钮
    UIImageView * bottomView4 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 300.0, 300.0, 45.0)];
    bottomView4.image = [UIImage imageNamed:@"blueBut.png"];
    bottomView4.userInteractionEnabled = YES;
    [self.view addSubview:bottomView4];
    [bottomView4 release];
    
    UIButton * button = [[UIButton alloc] init];
    button.frame = bottomView4.frame;
    button.backgroundColor = [UIColor clearColor];
    [button addTarget:self action:@selector(submitBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    [button release];
    
    UILabel * btnLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 45)];
    btnLb.backgroundColor = [UIColor clearColor];
    btnLb.textColor = [UIColor whiteColor];
    btnLb.text = @"提  交";
    btnLb.textAlignment = UITextAlignmentCenter;
    [button addSubview:btnLb];
    [btnLb release];
}

//提交修改
- (void)submitBtnClick{
    NSLog(@"submitBtnClick");
    if ([[pdField text] length] < 6 ||
        [[pdAgainField text] length] > 12) {
        
        [self dismissErrorMsg:@"请输入6-12位新密码."];
        return;
    }
    
    if (telField.text.length == 0 ||
        vCodeField.text.length == 0 ||
        pdField.text.length == 0 ||
        pdAgainField.text.length == 0) {
        [self dismissErrorMsg:@"请填写完整数据后再提交"];
        return ;
    }
    
    [self showWithStatusMsg:@"正在修改密码，请稍后..."];
    [self performSelector:@selector(resetPwd) withObject:nil afterDelay:0.5];
}

-(void)resetPwd{
    NSString * tel=telField.text,*captcha=vCodeField.text,*pwd=pdField.text;
    
    NSDictionary * dict = [[ClientJSONObject clientJSONObject] forgetPwdWithTel:tel captcha:captcha pwd:pwd sessionID:sessionID];
    [self dismiss];
    
    if ([[dict valueForKey:@"result"] integerValue] == 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }else if([[dict valueForKey:@"result"] integerValue] == 0){
        [self dismissErrorMsg:@"密码修改失败，请重新修改"];
    }else if([[dict valueForKey:@"result"] integerValue] == 4){
        [self dismissErrorMsg:@"验证码错误"];
    }else if([[dict valueForKey:@"result"] integerValue] == 3){
        [self dismissErrorMsg:@"验证码超时"];
    }else if([[dict valueForKey:@"result"] integerValue] == 6){
        [self dismissErrorMsg:@"手机号码与发送验证码的手机不匹配"];
    }else if([[dict valueForKey:@"result"] integerValue] == 8){
        [self dismissErrorMsg:@"请先获取手机验证码"];
    }
}

//获取验证码
-(void)getvCodeBtnClick{
    
    if (isGetVCode) {
        [self dismissErrorMsg:@"验证码已下发，请查收。"];
        return;
    }
    
    if (telField.text.length != 11) {
        [self dismissErrorMsg:@"请输入11位手机号码."];
        return ;
    }
    
    [self showWithStatusMsg:@"正在获取验证码，请稍后..."];
    [self performSelector:@selector(getCode) withObject:nil afterDelay:0.5];
}

-(void)getCode{
    NSString * tel = telField.text;
    NSDictionary * dict = [[ClientJSONObject clientJSONObject] getCaptcha:tel];
    [self dismiss];
    
    if ([[dict valueForKey:@"result"] integerValue] == 1) {
        //获取验证码成果
        sessionID = [[NSString alloc] initWithString:[dict valueForKey:@"sessionID"]];
        
        // enable timer after each 2 seconds for scrolling.
        [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(checkVerifyCodeButton) userInfo:nil repeats:NO];
        isGetVCode = TRUE;
    }else{
        [self dismissErrorMsg:@"获取验证码失败，请重新获取."];
    }
}

-(void)checkVerifyCodeButton{
    isGetVCode = FALSE;
}
#pragma mark - 输入框UITextField的协议处理

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
}

//按下Done按钮的调用方法，我们让键盘消失
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSCharacterSet *cs;
    if(textField.tag == pdFieldTag)
    {
        cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        BOOL basicTest = [string isEqualToString:filtered];
        if(!basicTest)
        {
            [self dismissErrorMsg:@"请输入数字或者字母."];
            return NO;
        }
    }
    
    //其他的类型不需要检测，直接写入
    return YES;
}

#pragma - mark 键盘隐藏
-(void)touchesEnded: (NSSet *)touches withEvent: (UIEvent *) event {
    NSLog(@"touchesEnded:withEvent:");
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([pdField isFirstResponder] && [touch view] != pdField) {
        [pdField resignFirstResponder];
    }else if ([pdAgainField isFirstResponder] && [touch view] != pdAgainField) {
        [pdAgainField resignFirstResponder];
    }else if ([telField isFirstResponder] && [touch view] != telField) {
        [telField resignFirstResponder];
    }else if ([vCodeField isFirstResponder] && [touch view] != vCodeField) {
        [vCodeField resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

#pragma mark -
#pragma mark 显示提醒框

- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框

- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}
@end
