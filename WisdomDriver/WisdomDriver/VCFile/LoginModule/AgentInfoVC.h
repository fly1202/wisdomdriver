//
//  AgentInfoVC.h
//  WisdomDriver
//
//  Created by  apple on 13-1-13.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AgentInfoVCDelegate <NSObject>

-(void)didSetDefaultAgent:(NSDictionary*)agent;

@end

@interface AgentInfoVC : UIViewController{
    id <AgentInfoVCDelegate> agentInfoVCDelegate;
}
@property (nonatomic,assign) id <AgentInfoVCDelegate> agentInfoVCDelegate;

-(id)initWithBasicDict:(NSDictionary*)dict;
@end
