//
//  AgentListVC.h
//  WisdomDriver
//
//  Created by  apple on 13-1-13.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AgentListVCDelegate <NSObject>

-(void)didSelectAgent:(NSDictionary*)agent;

@end

@interface AgentListVC : UIViewController{
    id <AgentListVCDelegate> agentListVCDelegate;
}

@property (nonatomic,assign) id <AgentListVCDelegate> agentListVCDelegate;

@end
