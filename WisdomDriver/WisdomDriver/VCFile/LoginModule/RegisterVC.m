//
//  RegisterVC.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-7.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "RegisterVC.h"
#import "ClientJSONObject.h"
#import "RunTimeShare.h"
#import "AgentListVC.h"
#import "defines.h"
#import "SVProgressHUD.h"
#import <QuartzCore/QuartzCore.h>
#import "ViewAgreementVC.h"

#define nameFieldTag        100
#define pdFieldTag          200
#define pdAgainFieldTag     300
#define telFieldTag         400
#define vCodeFieldTag       500

@interface RegisterVC () <UITextFieldDelegate,AgentListVCDelegate>{
    
    UITextField *  nameField;
    UITextField *  pdField;
    UITextField *  pdAgainField;
    UITextField *  vCodeField;
    UITextField *  telField;
    NSString *     sessionID;
    UIButton *     getvCodeBtn;
    UIButton *     agentButton;
    UIImageView *  agreeView;
    UILabel *      agentLabel;
    BOOL            isAgree;
    BOOL            isGetVCode;
    NSMutableDictionary * agentDict;
}

@end

@implementation RegisterVC
@synthesize registerVCDelegate;

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        isGetVCode = FALSE;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_640x960.png"]]];
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    //顶部条栏和文字处理，取消按钮,提交按钮
    [self addTopViewWithTitle:@"注册"];
    
    [self addRegContentControl];
}

#pragma mark - 添加顶部条栏和文字,取消按钮,提交按钮
-(void)addTopViewWithTitle:(NSString *)title{
    //顶部条栏
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    [topBarView release];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 47.0)];
    topLbl.textAlignment = UITextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    [topLbl release];
    
    //取消按钮
    UIButton *cancelBut = [[UIButton alloc] initWithFrame:CGRectMake(3.0, 8.0, 60.0, 30.0)];
    [cancelBut setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"navBarRightButBottom.png"] forState:UIControlStateNormal];
    [cancelBut addTarget:self action:@selector(clickOnCancelBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelBut];
    [cancelBut release];
    
    //提交按钮
    UIButton * submitBut = [[UIButton alloc] initWithFrame:CGRectMake(255.0, 8.0, 60.0, 30.0)];
    [submitBut setTitle:@"提交" forState:UIControlStateNormal];
    [submitBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [submitBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitBut setBackgroundImage:[UIImage imageNamed:@"navBarRightButBottom.png"] forState:UIControlStateNormal];
    [submitBut addTarget:self action:@selector(clickOnSubmitBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:submitBut];
    [submitBut release];
}

#pragma mark - ------add by caoyanbo-------
#pragma mark - 添加注册使用的各个控件
-(void)addRegContentControl{
    //手机号
    UIImageView * bottomView1 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 55.0, 300.0, 45.0)];
    [bottomView1 setImage:[UIImage imageNamed:@"bg_customtable1.png"]];
    bottomView1.userInteractionEnabled = TRUE;
    
    telField = [[UITextField alloc] initWithFrame:CGRectMake(10.0, 0.0, 290.0, 45.0)];
    telField.placeholder = @"请输入手机号码";
    telField.delegate = self;
    telField.borderStyle = UITextBorderStyleNone;
    //telField.keyboardType = UIKeyboardTypePhonePad;
    telField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    [bottomView1 addSubview:telField];
    [self.view addSubview:bottomView1];
    telField.tag = telFieldTag;
    [telField release];
    
    //验证码
    UIImageView * bottomView2 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 115.0, 300.0, 45.0)];
    [bottomView2 setImage:[UIImage imageNamed:@"bg_customtable1.png"]];
    bottomView2.userInteractionEnabled = TRUE;
    vCodeField = [[UITextField alloc] initWithFrame:CGRectMake(10.0, 0.0, 290.0, 45.0)];
    vCodeField.placeholder = @"请输入验证码";
    vCodeField.delegate = self;
    vCodeField.borderStyle = UITextBorderStyleNone;
    vCodeField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    [bottomView2 addSubview:vCodeField];
    [self.view addSubview:bottomView2];
    vCodeField.tag = vCodeFieldTag;
    [vCodeField release];
    
    //验证码获取按钮
    getvCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [getvCodeBtn addTarget:self action:@selector(getvCodeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [getvCodeBtn setFrame:CGRectMake(220, 123, 80, 30)];
    [getvCodeBtn setImage:[UIImage imageNamed:@"yanzhengma_btn.png"] forState:UIControlStateNormal];
    [getvCodeBtn setImage:[UIImage imageNamed:@"yanzhengma_btn2.png"] forState:UIControlStateSelected];
    [self.view addSubview:getvCodeBtn];
    
    UILabel * getvCodeLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 30)];
    getvCodeLb.backgroundColor = [UIColor clearColor];
    getvCodeLb.text = @"获取验证码";
    getvCodeLb.textColor = [UIColor whiteColor];
    getvCodeLb.font = [UIFont systemFontOfSize:12];
    getvCodeLb.textAlignment = UITextAlignmentCenter;
    [getvCodeBtn addSubview:getvCodeLb];
    [getvCodeLb release];
    
    //加载姓名和密码
    UIImageView * bottomView3 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 180.0, 300.0, 135.0)];
    bottomView3.image = [UIImage imageNamed:@"bg_customtable3.png"];
    bottomView3.userInteractionEnabled = YES;
    [self.view addSubview:bottomView3];
    [bottomView3 release];
    
    /*
    UIImageView * line1View = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 45.0, 300.0, 1.0)];
    line1View.image = [UIImage imageNamed:@"shezhi_lineacc.png"];
    [bottomView1 addSubview:line1View];
    [line1View release];
    UIImageView * line2View = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 90.0, 300.0, 1.0)];
    line2View.image = [UIImage imageNamed:@"shezhi_lineacc.png"];
    [bottomView1 addSubview:line2View];
    [line2View release];
    */
    
    NSArray * strArr = [[NSArray alloc] initWithObjects:@"姓名：",@"请输入密码：",@"重复密码：", nil];
    for (int i=0; i<[strArr count]; i++) {
        UILabel * txtLbl = [[UILabel alloc] initWithFrame:CGRectMake(5.0, 45.0*i, 110.0, 45.0)];
        txtLbl.backgroundColor = [UIColor clearColor];
        txtLbl.textAlignment = NSTextAlignmentRight;
        txtLbl.text = [strArr objectAtIndex:i];
        [bottomView3 addSubview:txtLbl];
        [txtLbl release];
        
        UITextField * txtField = [[UITextField alloc] initWithFrame:CGRectMake(130.0, 45.0*i, 180.0, 45.0)];
        txtField.delegate = self;
        txtField.backgroundColor = [UIColor clearColor];
        //txtField.borderStyle = UITextBorderStyleRoundedRect;
        txtField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
        [bottomView3 addSubview:txtField];
        if (i == 0) {
            txtField.tag = nameFieldTag;
            nameField = txtField;
        }else if (i == 1){
            txtField.secureTextEntry = YES;
            txtField.tag = pdFieldTag;
            pdField = txtField;
        }else{
            txtField.secureTextEntry = YES;
            txtField.tag = pdAgainFieldTag;
            pdAgainField = txtField;
        }
        [txtField release];
    }
    
    //加载代理商按钮
    UIImageView * bottomView4 = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 330.0, 300.0, 45.0)];
    bottomView4.image = [UIImage imageNamed:@"bg_customtable1.png"];
    bottomView4.userInteractionEnabled = YES;
    [self.view addSubview:bottomView4];
    [bottomView4 release];
    
    UILabel * txtLbl = [[UILabel alloc] initWithFrame:CGRectMake(5.0, 0.0, 110.0, 45.0)];
    txtLbl.backgroundColor = [UIColor clearColor];
    txtLbl.text = @"选择代理商：";
    //txtLbl.textColor = [UIColor colorWithRed:86.0 green:86.0 blue:86.0 alpha:0.6];
    txtLbl.textAlignment = NSTextAlignmentRight;
    [bottomView4 addSubview:txtLbl];
    [txtLbl release];
    
    agentLabel = [[UILabel alloc] initWithFrame:CGRectMake(115.0, 0.0, 150.0, 45.0)];
    agentLabel.backgroundColor = [UIColor clearColor];
    agentLabel.text = @"";
    agentLabel.textAlignment = NSTextAlignmentRight;
    [bottomView4 addSubview:agentLabel];
    [agentLabel release];
    
    UIImageView * arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(280.0, 15.0, 10.0, 14.0)];
    arrowView.image = [UIImage imageNamed:@"acc_jiantou.png"];
    [bottomView4 addSubview:arrowView];
    [arrowView release];
    
    UIButton * messageBut = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 300.0, 45.0)];
    messageBut.backgroundColor = [UIColor clearColor];
    messageBut.tag = 1002;
    [messageBut addTarget:self action:@selector(clickOnAgentBut) forControlEvents:UIControlEventTouchUpInside];
    [bottomView4 addSubview:messageBut];
    [messageBut release];
    
    //注册协议
    agreeView = [[UIImageView alloc] initWithFrame:CGRectMake(30.0, 385.0, 40.0, 44.0)];
    agreeView.image = [UIImage imageNamed:@"acc_btnyuandefault1.png"];
    if (isAgree) {
        agreeView.image = [UIImage imageNamed:@"acc_btnyuanselect2.png"];
    }
    [self.view addSubview:agreeView];
    [agreeView release];
    
    UILabel * agreeLbl = [[UILabel alloc] initWithFrame:CGRectMake(80.0, 390.0, 200.0, 30.0)];
    agreeLbl.text = @"同意《7智驾通代驾协议》";
    agreeLbl.backgroundColor = [UIColor clearColor];
    agreeLbl.textAlignment = UITextAlignmentCenter;
    [self.view addSubview:agreeLbl];
    [agreeLbl release];
    
    CALayer* layer = [agreeLbl layer];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.borderColor = [UIColor darkGrayColor].CGColor;
    bottomBorder.borderWidth = 1;
    bottomBorder.frame = CGRectMake(-1, layer.frame.size.height-1, layer.frame.size.width, 1);
    [bottomBorder setBorderColor:[UIColor blackColor].CGColor];
    [layer addSublayer:bottomBorder];
    
    UIButton * agreeBut = [[UIButton alloc] initWithFrame:CGRectMake(80.0, 385.0, 210.0, 40.0)];
    agreeBut.backgroundColor = [UIColor clearColor];
    [agreeBut addTarget:self action:@selector(enterAgreeVC) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:agreeBut];
    [agreeBut release];
    
    UIButton * recordBut = [[UIButton alloc] initWithFrame:CGRectMake(30.0, 385.0, 40.0, 44.0)];
    recordBut.backgroundColor = [UIColor clearColor];
    [recordBut addTarget:self action:@selector(clickOnAgreeBut) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:recordBut];
    [recordBut release];
}

#pragma mark - 查看代驾协议
-(void)enterAgreeVC{
    ViewAgreementVC * vc = [[ViewAgreementVC alloc] init];
    [self.navigationController pushViewController:vc animated:TRUE];
}

#pragma mark - 输入框UITextField的协议处理

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
}

//按下Done按钮的调用方法，我们让键盘消失
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSCharacterSet *cs;
    if(textField.tag == pdFieldTag)
    {
        cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        BOOL basicTest = [string isEqualToString:filtered];
        if(!basicTest)
        {
            [self dismissErrorMsg:@"请输入数字或者字母"];
            return NO;
        }
    }
    
    //其他的类型不需要检测，直接写入
    return YES;
}

#pragma mark - 代理商delegate
-(void)didSelectAgent:(NSDictionary *)agent{
    agentLabel.text = [agent valueForKey:@"agentName"];
    
    [self.navigationController popToViewController:self animated:TRUE];
}

#pragma mark - Control Event
-(void)clickOnAgentBut{
    AgentListVC * agListVC = [[AgentListVC alloc] init];
    agListVC.agentListVCDelegate = self;
    [self.navigationController pushViewController:agListVC animated:TRUE];
}

//是否同意协议
-(void)clickOnAgreeBut{
    isAgree = !isAgree;
    agreeView.image = [UIImage imageNamed:@"acc_btnyuandefault1.png"];
    if (isAgree) {
        agreeView.image = [UIImage imageNamed:@"acc_btnyuanselect2.png"];
    }
}

//获取验证码
-(void)getvCodeBtnClick{
    if (isGetVCode) {
        [self dismissErrorMsg:@"验证码已下发，请查收。"];
        return;
    }
    
    if (telField.text.length != 11) {
        [self dismissErrorMsg:@"请输入11位手机号码"];
        return ;
    }
    
    [self showWithStatusMsg:@"正在获取验证码，请稍后..."];
    [self performSelector:@selector(getCode) withObject:nil afterDelay:0.2];
}

-(void)getCode{
    NSString * tel = telField.text;
    NSDictionary * dict = [[ClientJSONObject clientJSONObject] getCaptcha:tel];
    [self dismiss];
    
    if ([[dict valueForKey:@"result"] integerValue] == 1) {
        //获取验证码成果
        sessionID = [[NSString alloc] initWithString:[dict valueForKey:@"sessionID"]];
        // enable timer after each 2 seconds for scrolling.
        [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(checkVerifyCodeButton) userInfo:nil repeats:NO];
        isGetVCode = TRUE;
    }else{
        [self dismissErrorMsg:@"服务器无响应，请重新获取验证码"];
    }
}

-(void)checkVerifyCodeButton{
    isGetVCode = FALSE;
}

//取消注册
-(void)clickOnCancelBut{
    [self.navigationController popViewControllerAnimated:YES];
}

//提交注册
-(void)clickOnSubmitBut{
    if (telField.text.length == 0){
        [self dismissErrorMsg:@"用户名未输入"];
        return ;
    }
    if (vCodeField.text.length == 0){
        [self dismissErrorMsg:@"验证码未输入"];
        return ;
    }
    if (pdField.text.length == 0 ||
        pdAgainField.text.length == 0){
        [self dismissErrorMsg:@"密码未输入"];
        return ;
    }
    if (pdField.text.length < 6 || pdField.text.length > 12 ||
        pdAgainField.text.length < 6 || pdAgainField.text.length > 12){
        [self dismissErrorMsg:@"请输入6-12位长度密码"];
        return ;
    }
    if (![pdField.text isEqualToString:pdAgainField.text]){
        [self dismissErrorMsg:@"密码不一致"];
        return ;
    }
    if (agentLabel.text.length == 0) {
        [self dismissErrorMsg:@"代理商未选择，请选择默认代理商"];
        return ;
    }
    if (!isAgree) {
        [self dismissErrorMsg:@"请同意《7智驾通代驾协议》"];
        return ;
    }
    
    [self showWithStatusMsg:@"正在注册，请稍后..."];
    [self performSelector:@selector(registerUser) withObject:nil afterDelay:0.2];
}

-(void)registerUser{
    NSString * tel=telField.text,*captcha=vCodeField.text,
    *name=nameField.text,*pwd=pdField.text,
    *agentID=[RunTimeShare getAgentID];
    
    NSDictionary * dict = [[ClientJSONObject clientJSONObject] regWithTel:tel captcha:captcha name:name pwd:pwd agentID:agentID sessionID:sessionID];
    [self dismiss];
    
    if ([[dict valueForKey:@"result"] integerValue] == 1) {
        
        //记录代理商ID\用户昵称
        [RunTimeShare setAgentID:agentID];
        [RunTimeShare setNickName:name];
        
        //先假设注册都成功
        [self.registerVCDelegate didRegisterSubmitSuccess];
        [self.navigationController popViewControllerAnimated:YES];
    }else if([[dict valueForKey:@"result"] integerValue] == 0){
        [self dismissErrorMsg:@"服务器未响应，请重新提交注册信息"];
    }else if([[dict valueForKey:@"result"] integerValue] == 2){
        [self dismissErrorMsg:kSOAPError2];
    }else if([[dict valueForKey:@"result"] integerValue] == 3){
        [self dismissErrorMsg:kSOAPError3];
    }else if([[dict valueForKey:@"result"] integerValue] == 4){
        [self dismissErrorMsg:kSOAPError4];
    }else if([[dict valueForKey:@"result"] integerValue] == 5){
        [self dismissErrorMsg:kSOAPError5];
    }else if([[dict valueForKey:@"result"] integerValue] == 6){
        [self dismissErrorMsg:kSOAPError6];
    }else if([[dict valueForKey:@"result"] integerValue] == 7){
        [self dismissErrorMsg:kSOAPError7];
    }else if([[dict valueForKey:@"result"] integerValue] == 8){
        [self dismissErrorMsg:kSOAPError8];
    }else if([[dict valueForKey:@"result"] integerValue] == 9){
        [self dismissErrorMsg:kSOAPError9];
    }
}

#pragma - mark 键盘隐藏
-(void)touchesEnded: (NSSet *)touches withEvent: (UIEvent *) event {
    NSLog(@"touchesEnded:withEvent:");
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([nameField isFirstResponder] && [touch view] != nameField) {
        [nameField resignFirstResponder];
    }else if ([pdField isFirstResponder] && [touch view] != pdField) {
        [pdField resignFirstResponder];
    }else if ([pdAgainField isFirstResponder] && [touch view] != pdAgainField) {
        [pdAgainField resignFirstResponder];
    }else if ([telField isFirstResponder] && [touch view] != telField) {
        [telField resignFirstResponder];
    }else if ([vCodeField isFirstResponder] && [touch view] != vCodeField) {
        [vCodeField resignFirstResponder];
    }
    
    [super touchesBegan:touches withEvent:event];
}

#pragma mark -
#pragma mark 显示提醒框

- (void)showWithStatusMsg:(NSString*)msg {
    if (msg && msg.length > 0) {
        [SVProgressHUD showWithStatus:msg];
    }else{
        [SVProgressHUD show];
    }
}

#pragma mark -
#pragma mark 隐藏提醒框

- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}
@end
