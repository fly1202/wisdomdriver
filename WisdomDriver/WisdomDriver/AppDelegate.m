//
//  AppDelegate.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-6.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "AppDelegate.h"
#import "HelperVC.h"
#import "MainTabVC.h"
#import "defines.h"
#import "RunTimeShare.h"

@implementation AppDelegate

- (void)dealloc
{
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //初始化有关需要本地化的信息
    [self initLocalInfo];
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    
    //先判断是否为第一次运行应用
    if ([RunTimeShare isFirstTimeRun]) {
        HelperVC * helpVC = [[HelperVC alloc] init];
        UINavigationController * rootNav = [[UINavigationController alloc] initWithRootViewController:helpVC];
        [rootNav setNavigationBarHidden:YES];
        [self.window addSubview:rootNav.view];
    }
    //如果不是第一次运行，则直接进入
    else{
        MainTabVC * mainVC = [[MainTabVC alloc] init];
        UINavigationController * rootNav = [[UINavigationController alloc] initWithRootViewController:mainVC];
        [rootNav setNavigationBarHidden:YES];
        [self.window addSubview:rootNav.view];
    }
    
    // 要使用百度地图，请先启动BaiduMapManager
	_mapManager = [[BMKMapManager alloc]init];
    // 如果要关注网络及授权验证事件，请设定generalDelegate参数
	BOOL ret = [_mapManager start:MAP_KEY generalDelegate:nil];
	if (!ret) {
		NSLog(@"manager start failed!");
	}
    
    self.window.backgroundColor = [UIColor blackColor];
    [self.window makeKeyAndVisible];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - 初始化有关需要本地化的信息

- (void)initLocalInfo{
    /*
    //判断是否刚下载进行第一次运行本应用，数据为BOOL类型的NSNumber
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"FirstTimeRun"]==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:@"FirstTimeRun"];
    }
    
    //判断是否已经进行登录密码纪录,数据为BOOL类型的NSNumber
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"IsLoginRecord"]==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:@"IsLoginRecord"];
    }
    
    //判断是否为自动登录，数据为BOOL类型的NSNumber
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"IsAutoLogin"]==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:@"IsAutoLogin"];
    }
    
    //用户登录的名称，数据为String类型
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"UserName"]==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"UserName"];
    }
    
    //用户登录的密码，数据为String类型
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"PassWord"]==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"PassWord"];
    }

    //是否震动提醒，数据为BOOL类型的NSNumber
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isVibrationAlert"]==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:@"isVibrationAlert"];
    }
    
    //是否声音提醒，数据为BOOL类型的NSNumber
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isSoundAlert"]==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:@"isSoundAlert"];
    }

     */
}

@end
