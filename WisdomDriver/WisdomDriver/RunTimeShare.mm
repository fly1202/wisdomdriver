//
//  RunTimeShare.m
//  WisdomDriver
//
//  Created by  apple on 13-1-13.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "RunTimeShare.h"
#import "defines.h"
#import "QREncoder.h"

static RunTimeShare * instance;

@implementation RunTimeShare
@synthesize curLocationString;
@synthesize curAddrInfo;
@synthesize curCityCode;
@synthesize curUserLocation;
@synthesize userTelphone;
@synthesize userPwd;
@synthesize searchKeyArray;

-(id)init
{
    self=[super init];
    if (self) {
        usedAddrArray = [[NSMutableArray alloc] init];
    }
    return self;
}

+(RunTimeShare *)sharedInstance
{
    if (!instance) {
        instance=[[RunTimeShare alloc] init];
        //[instance readUsedAddress];
    }
    
    return instance;
}

#pragma - mark 保存数据
-(void)saveData{
    [self saveUsedAddress];
}

#pragma - mark 清理数据
-(void)clearForLogin{
    if (usedAddrArray) {
        [usedAddrArray removeAllObjects];
    }
    if (searchKeyArray) {
        [searchKeyArray removeAllObjects];
    }
}

#pragma - mark 搜索历史
-(NSArray*)getSearchKeyArray{
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * path = [NSString stringWithFormat:@"%@/searchKey%@.plist",docDir,[RunTimeShare getUserTel]];
    if (searchKeyArray == nil) {
        searchKeyArray = [[NSMutableArray alloc] init];
    }else{
        [searchKeyArray removeAllObjects];
    }
    
    NSArray * arr = [NSArray arrayWithContentsOfFile:path];
    [searchKeyArray addObjectsFromArray:arr];
    
    return searchKeyArray;
}
-(void)addSearchKey:(NSString*)key{
    if (searchKeyArray == nil) {
        searchKeyArray = [[NSMutableArray alloc] init];
    }
    for (NSString * str in searchKeyArray) {
        if ([key isEqualToString:str]) {
            return;
        }
    }
    [searchKeyArray addObject:key];
    [self saveSearchKeyArray];
}

-(void)saveSearchKeyArray{
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * path = [NSString stringWithFormat:@"%@/searchKey%@.plist",docDir,[RunTimeShare getUserTel]];
    
    if (searchKeyArray.count > 0) {
        [searchKeyArray writeToFile:path atomically:TRUE];
    }
}

-(void)removeSearchKeyArray{
    if (searchKeyArray) {
        [searchKeyArray removeAllObjects];
    }
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * path = [NSString stringWithFormat:@"%@/searchKey%@.plist",docDir,[RunTimeShare getUserTel]];
    
    NSError * error;
    [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
}

#pragma - mark 读取常用地址
-(NSArray*)getusedAddrArray{
    if (usedAddrArray == nil || usedAddrArray.count == 0) {
        [self readUsedAddress];
    }
    return usedAddrArray;;
}

-(void)readUsedAddress{
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * path = [NSString stringWithFormat:@"%@/usedAddr%@.plist",docDir,[RunTimeShare getUserTel]];
    if (usedAddrArray == nil) {
        usedAddrArray = [[NSMutableArray alloc] init];
    }
    
    NSArray * arr = [NSArray arrayWithContentsOfFile:path];
    [usedAddrArray addObjectsFromArray:arr];
}

#pragma - mark 保存常用地址
-(void)saveUsedAddress{
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * path = [NSString stringWithFormat:@"%@/usedAddr%@.plist",docDir,[RunTimeShare getUserTel]];
    
    if (usedAddrArray.count > 0) {
        [usedAddrArray writeToFile:path atomically:TRUE];
    }
}

#pragma - mark 添加常用地址
-(void)addUsedAddr:(NSDictionary*)addrDict{
    [usedAddrArray addObject:addrDict];
    [self saveUsedAddress];
}

#pragma - mark 删除常用地址
-(void)removeUsedAddr:(NSDictionary*)addrDict{
    for (NSDictionary * dict in usedAddrArray) {
        if ([[dict valueForKey:kAddrStr] isEqualToString:[addrDict valueForKey:kAddrStr]]) {
            [usedAddrArray removeObject:dict];
            break;
        }
    }
    [self saveUsedAddress];
}

#pragma - mark 生成常用地址
+(NSDictionary*)makeAddrDict:(NSString*)addrName location:(CLLocation*)loc{
    NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
    [dict setValue:addrName forKey:kAddrStr];
    [dict setValue:[NSNumber numberWithDouble:loc.coordinate.latitude] forKey:kAddrLatitude];
    [dict setValue:[NSNumber numberWithDouble:loc.coordinate.longitude] forKey:kAddrLogitude];
    [dict setValue:[NSNumber numberWithDouble:loc.verticalAccuracy] forKey:kAddrAccuracy];
    return dict;
}

#pragma - mark 获取代理商
+(NSString*)getAgentID{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kDefaultAgentID]==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kDefaultAgentID];
        return @"";
    }
    return [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultAgentID];
}

#pragma - mark 设置代理商
+(void)setAgentID:(NSString*)agentID{
    [[NSUserDefaults standardUserDefaults] setValue:agentID forKey:kDefaultAgentID];
}

#pragma - mark 获取用户电话号码
+(NSString*)getUserTel{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserTel]==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kDefaultUserTel];
    }
    return [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultUserTel];
}

#pragma - mark 设置用户电话号码
+(void)setUserTel:(NSString*)tel{
    [[NSUserDefaults standardUserDefaults] setValue:tel forKey:kDefaultUserTel];
}

#pragma - mark 用户密码
//获取用户密码
+(NSString*)getUserPassword{
    //用户登录的密码，数据为String类型
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kDefaultPassword]==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kDefaultPassword];
        return @"";
    }
    return [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultPassword];
}
//设置用户密码
+(void)setUserPassword:(NSString*)pwd{
    //用户登录的密码，数据为String类型
    [[NSUserDefaults standardUserDefaults] setObject:pwd forKey:kDefaultPassword];
}

#pragma - mark 用户昵称
//获取用户昵称
+(NSString*)getNickName{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kDefaultNickName]==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kDefaultNickName];
        return @"";
    }
    return [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultNickName];
}
//设置用户昵称
+(void)setNickName:(NSString*)name{
    [[NSUserDefaults standardUserDefaults] setObject:name forKey:kDefaultNickName];
}

#pragma - mark 首次运行
//是否首次运行
+(BOOL)isFirstTimeRun{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kDefaultFirstTimeRun]==nil) {
        return TRUE;
    }
    return FALSE;
}
+(void)setFirstTimeRun:(BOOL)flag{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:kDefaultFirstTimeRun];
}

#pragma - mark 自动登录
//设置自动登陆
+(BOOL)isAutoLogin{
    //判断是否为自动登录，数据为BOOL类型的NSNumber
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kDefaultIsAutoLogin]==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:kDefaultIsAutoLogin];
    }
    return [[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultIsAutoLogin] boolValue];
}
+(void)setIsAutoLogin:(BOOL)isAutoLogin{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:isAutoLogin]
                                              forKey:kDefaultIsAutoLogin];
}

#pragma - mark 记住密码
//设置记住密码
+(BOOL)isLoginRecord{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kDefaultIsLoginRecord]==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO]
                                                  forKey:kDefaultIsLoginRecord];
    }
    return [[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultIsLoginRecord] boolValue];
}
+(void)setIsLoginRecord:(BOOL)isLoginRecord{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:isLoginRecord]
                                              forKey:kDefaultIsLoginRecord];
}

#pragma - mark 震动提示
//震动提示
+(BOOL)isVibrationAlert{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kDefaultIsVibrationAlert]==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO]
                                                  forKey:kDefaultIsVibrationAlert];
    }
    return [[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultIsVibrationAlert] boolValue];
}
+(void)setIsVibrationAlert:(BOOL)isVibrationAlert{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:isVibrationAlert]
                                              forKey:kDefaultIsVibrationAlert];
}

#pragma - mark 声音提示
//声音提示
+(BOOL)isSoundAlert{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kDefaultIsSoundAlert]==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO]
                                                  forKey:kDefaultIsSoundAlert];
    }
    return [[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultIsSoundAlert] boolValue];
}
+(void)setSoundAlert:(BOOL)isSoundAlert{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:isSoundAlert]
                                              forKey:kDefaultIsSoundAlert];
}

#pragma mark - 二维码
//二维码
+(UIImage*)getQRCodeImage{
    UIImage * codeImage;
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	// If you go to the folder below, you will find those pictures
	NSLog(@"%@",docDir);
	NSString *pngFilePath = [NSString stringWithFormat:@"%@/%@%@",docDir,[RunTimeShare getUserTel],kQRCodeImageName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:pngFilePath]) {
        NSLog(@"QRCode image is not exist!!!");
        return nil;
    }else{
        NSData * data = [NSData dataWithContentsOfFile:pngFilePath];
        codeImage = [UIImage imageWithData:data];
    }
    return codeImage;
}
+(void)setQRCodeImage:(UIImage*)img{
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)
                        objectAtIndex:0];
	// If you go to the folder below, you will find those pictures
	NSLog(@"%@",docDir);
	NSString *pngFilePath = [NSString stringWithFormat:@"%@/%@%@",docDir,[RunTimeShare getUserTel],kQRCodeImageName];
    
    NSData *data1 = [NSData dataWithData:UIImagePNGRepresentation(img)];
    [data1 writeToFile:pngFilePath atomically:YES];
}

+(UIImage*)generateQRImageWithStr:(NSString*)aVeryLongURL size:(int)qrcodeImageDimension{
    //the qrcode is square. now we make it 250 pixels wide
    //int qrcodeImageDimension = 250;
    
    //first encode the string into a matrix of bools, TRUE for black dot and FALSE for white. Let the encoder decide the error correction level and version
    DataMatrix* qrMatrix = [QREncoder encodeWithECLevel:QR_ECLEVEL_AUTO version:QR_VERSION_AUTO string:aVeryLongURL];
    
    //then render the matrix
    UIImage* qrcodeImage = [QREncoder renderDataMatrix:qrMatrix imageDimension:qrcodeImageDimension];
    return qrcodeImage;
}

#pragma mark - 用户信息
//用户信息
+(NSDictionary*)getUserInfo{
    if([[NSUserDefaults standardUserDefaults] objectForKey:kUserInfoSex] == nil){
        [[NSUserDefaults standardUserDefaults] setObject:kUserInfoSexMale forKey:kUserInfoSex];
    }
    
    NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
    [dict setValue:[RunTimeShare getAgentID] forKey:kUserInfoAgentID];
    [dict setValue:[RunTimeShare getNickName] forKey:kUserInfoNickName];
    [dict setValue:[[NSUserDefaults standardUserDefaults] objectForKey:kUserInfoSex]
            forKey:kUserInfoSex];
    [dict setValue:[[NSUserDefaults standardUserDefaults] objectForKey:kUserInfoClientID]
            forKey:kUserInfoClientID];
    
    UIImage * headImage;
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	// If you go to the folder below, you will find those pictures
	NSLog(@"%@",docDir);
	NSString *pngFilePath = [NSString stringWithFormat:@"%@/clientLogo.jpg",docDir];
    if (![[NSFileManager defaultManager] fileExistsAtPath:pngFilePath]) {
        headImage = [UIImage imageNamed:@"tempHead.png"];
    }else{
        NSData * data = [NSData dataWithContentsOfFile:pngFilePath];
        headImage = [UIImage imageWithData:data];
    }
    [dict setValue:headImage forKey:kUserInfoClientLogo];
    
    return dict;
}
+(void)setUserInfo:(NSDictionary*)dict{
    
    UIImage * headImage = nil;
    id obj = [dict valueForKey:kUserInfoClientLogo];
    if ([obj isKindOfClass:[NSString class]]) {
        NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:obj]];
        if (data == nil) {
            headImage = [UIImage imageNamed:@"tempHead.png"];
        }else{
            headImage = [UIImage imageWithData:data];
        }
    }else if ([obj isKindOfClass:[UIImage class]]){
        headImage = obj;
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        // If you go to the folder below, you will find those pictures
        NSLog(@"%@",docDir);
        NSString *pngFilePath = [NSString stringWithFormat:@"%@/clientLogo.jpg",docDir];
        NSData *data1 = [NSData dataWithData:UIImagePNGRepresentation(headImage)];
        [data1 writeToFile:pngFilePath atomically:YES];
    }
    
    [RunTimeShare setAgentID:[dict valueForKey:kUserInfoAgentID]];
    [RunTimeShare setNickName:[dict valueForKey:kUserInfoNickName]];
    [[NSUserDefaults standardUserDefaults] setValue:[dict valueForKey:kUserInfoSex] forKey:kUserInfoSex];
    [[NSUserDefaults standardUserDefaults] setValue:[dict valueForKey:kUserInfoClientID] forKey:kUserInfoClientID];
}
@end
