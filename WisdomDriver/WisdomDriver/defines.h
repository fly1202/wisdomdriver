//
//  defines.h
//  WisdomDriver
//
//  Created by  apple on 13-1-12.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * HTTP_PREFIX;

extern NSString * kAgreementText;

extern NSString * APPURL;
extern NSString * APP_UPDATE_URL;
extern NSString * MAP_KEY;
extern NSString * kNotifyUpdateLocation;
extern NSString * kDefaultAgentID;
extern NSString * kDefaultUserTel;
extern NSString * kDefaultPassword;
extern NSString * kDefaultNickName;
extern NSString * kDefaultFirstTimeRun;
extern NSString * kDefaultIsAutoLogin;
extern NSString * kDefaultIsLoginRecord;
extern NSString * kDefaultIsVibrationAlert;
extern NSString * kDefaultIsSoundAlert;

extern NSString * kUserInfoClientID;
extern NSString * kUserInfoClientLogo;
extern NSString * kUserInfoNickName;
extern NSString * kUserInfoSex;
extern NSString * kUserInfoSexMale;
extern NSString * kUserInfoSexFemale;
extern NSString * kUserInfoAgentID;

extern NSString * kQRCodeImageName;
extern NSString * kQRCodeDimensionalCode;

extern NSString * kAddrStr;
extern NSString * kAddrLatitude;
extern NSString * kAddrLogitude;
extern NSString * kAddrAccuracy;

extern NSString * kDriverName;
extern NSString * kDriverTel;
extern NSString * kDriverAgentName;
extern NSString * kDriverAgentTel;
extern NSString * kDriverLongitude;
extern NSString * kDriverLatitude;
extern NSString * kDriverStatus;

extern NSString * kReserveNumber;
extern NSString * kReserveStartAddr;
extern NSString * kReserveEndAddr;
extern NSString * kReserveDriveTime;
extern NSString * kReserveDriverCount;
extern NSString * kReserveDriverName;
extern NSString * kReserveDriverTel;
extern NSString * kReserveRateStandard;

extern NSString * kShareShortMsg;
extern NSString * kShareSinaWeibo;
extern NSString * kShareQQWeibo;

extern NSString * kPhoneCustomService;
extern NSString * kPhoneOrderService;

extern NSString * kMemCardNumer;
extern NSString * kMemCardCreateTime;
extern NSString * kMemCardType;
extern NSString * kMemCardCash;
extern NSString * kMemCardGiftCash;
extern NSString * kMemCardConsumeCount;

extern NSString * kConsumeOrderID;
extern NSString * kConsumeGuestTel;
extern NSString * kConsumeStartAddr;
extern NSString * kConsumeEndAddr;
extern NSString * kConsumeDaijiaTime;
extern NSString * kConsumeEndTime;
extern NSString * kConsumeDriverNum;
extern NSString * kConsumeDriverName;
extern NSString * kConsumeAgentName;
extern NSString * kConsumeTotalPrice;

extern NSString * kSOAPError0;
extern NSString * kSOAPError1;
extern NSString * kSOAPError2;
extern NSString * kSOAPError3;
extern NSString * kSOAPError4;
extern NSString * kSOAPError5;
extern NSString * kSOAPError6;
extern NSString * kSOAPError7;
extern NSString * kSOAPError8;
extern NSString * kSOAPError9;

#define UPDATE_DRIVER_DISTANCE 50.0
#define SPAN_DISTANCE_WIDTH 5*1000
#define SPAN_DISTANCE_HEIGHT 5*1000

#define _LOG_DEBUG_ 1
#define _LOG_INFO_  0
#define _LOG_ERROR_ 0

#if _LOG_ERROR_
#define LogDebug(fmt, ...) ;
#define LogInfo(fmt, ...) ;
#define LogError(fmt, ...) NSLog((@"%s [ERROR] " fmt), __FUNCTION__, ##__VA_ARGS__)
#elif _LOG_INFO_
#define LogDebug(fmt, ...) ;
#define LogInfo(fmt, ...) NSLog((@"%s [INFO] " fmt), __FUNCTION__, ##__VA_ARGS__)
#define LogError(fmt, ...) NSLog((@"%s [ERROR] " fmt), __FUNCTION__, ##__VA_ARGS__)
#elif _LOG_DEBUG_
#define LogDebug(fmt, ...) NSLog((@"%s [DEBUG] " fmt), __FUNCTION__, ##__VA_ARGS__)
#define LogInfo(fmt, ...) NSLog((@"%s [INFO] " fmt), __FUNCTION__, ##__VA_ARGS__)
#define LogError(fmt, ...) NSLog((@"%s [ERROR] " fmt), __FUNCTION__, ##__VA_ARGS__)
#else
#define LogDebug(fmt, ...) ;
#define LogInfo(fmt, ...) ;
#define LogError(fmt, ...) ;
#endif
