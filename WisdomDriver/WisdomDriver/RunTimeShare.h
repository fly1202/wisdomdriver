//
//  RunTimeShare.h
//  WisdomDriver
//
//  Created by  apple on 13-1-13.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BMapKit.h"

@interface RunTimeShare : NSObject
{
    CLLocation * curUserLocation;
    BMKAddrInfo * curAddrInfo;
    NSString * curLocationString;
    NSString * curCityCode;
    
    NSString * userTelphone;
    NSString * userPwd;
    
    NSMutableArray * usedAddrArray;
    NSMutableArray * searchKeyArray;
}

@property (nonatomic, retain) CLLocation * curUserLocation;
@property (nonatomic, retain) BMKAddrInfo * curAddrInfo;
@property (nonatomic, retain) NSString * curLocationString;
@property (nonatomic, retain) NSString * curCityCode;
@property (nonatomic, retain) NSString * userTelphone;
@property (nonatomic, retain) NSString * userPwd;
@property (nonatomic, retain) NSMutableArray * usedAddrArray;
@property (nonatomic, retain) NSMutableArray * searchKeyArray;

+(RunTimeShare*)sharedInstance;
-(void)saveData;
-(void)clearForLogin;

//读取常用地址
-(NSArray*)getusedAddrArray;
//添加常用地址
-(void)addUsedAddr:(NSDictionary*)addrDict;
//删除常用地址
-(void)removeUsedAddr:(NSDictionary*)addrDict;
//生成常用地址
+(NSDictionary*)makeAddrDict:(NSString*)addrName location:(CLLocation*)loc;

//搜索历史清单
-(NSArray*)getSearchKeyArray;
-(void)addSearchKey:(NSString*)addrDict;
-(void)removeSearchKeyArray;

//获取代理商
+(NSString*)getAgentID;
//设置代理商
+(void)setAgentID:(NSString*)agentID;

//获取用户电话号码
+(NSString*)getUserTel;
//设置用户电话号码
+(void)setUserTel:(NSString*)tel;

//获取用户密码
+(NSString*)getUserPassword;
//设置用户密码
+(void)setUserPassword:(NSString*)pwd;

//获取用户昵称
+(NSString*)getNickName;
//设置用户昵称
+(void)setNickName:(NSString*)name;

//是否首次运行
+(BOOL)isFirstTimeRun;
+(void)setFirstTimeRun:(BOOL)flag;

//设置自动登陆
+(BOOL)isAutoLogin;
+(void)setIsAutoLogin:(BOOL)isAutoLogin;

//设置记住密码
+(BOOL)isLoginRecord;
+(void)setIsLoginRecord:(BOOL)isLoginRecord;

//震动提示
+(BOOL)isVibrationAlert;
+(void)setIsVibrationAlert:(BOOL)isVibrationAlert;

//声音提示
+(BOOL)isSoundAlert;
+(void)setSoundAlert:(BOOL)isSoundAlert;

//个人二维码
+(UIImage*)getQRCodeImage;
+(void)setQRCodeImage:(UIImage*)img;

//二维码工具
+(UIImage*)generateQRImageWithStr:(NSString*)str size:(int)pix;

//用户信息
+(NSDictionary*)getUserInfo;
+(void)setUserInfo:(NSDictionary*)dict;
@end
