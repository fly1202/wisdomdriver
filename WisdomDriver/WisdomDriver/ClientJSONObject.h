//
//  ClientJSONObject.h
//  WisdomDriver
//
//  Created by  apple on 13-1-12.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClientJSONObject : NSObject


//类方法 定义
+(ClientJSONObject *)clientJSONObject;

//获取广告图片
//http://idriven.oicp.net:8080/khd/AdsServlet?agentID=
-(NSArray *)getADListWithAgentID:(NSString*)agentID;

//获取验证码
//http://idriven.oicp.net:8080/khd/CaptchaServlet?userTel=
-(NSDictionary*)getCaptcha:(NSString*)tel;

//登陆
//http://idriven.oicp.net:8080/khd/LoginServlet?userTel=&pwd=
-(NSDictionary*)loginWithTel:(NSString*)tel pwd:(NSString*)pwd;

//注册
//http://idriven.oicp.net:8080/khd/RegisterServlet?userTel=& captcha=& name=& pwd=& agentID= &sessionID=
-(NSDictionary*)regWithTel:(NSString*)tel captcha:(NSString*)code name:(NSString*)name pwd:(NSString*)pwd agentID:(NSString*)agentID sessionID:(NSString*)sessionID;

//密码修改
//http://idriven.oicp.net:8080/khd/ChangePwServlet.jsp?userTel+pwd +newPwd
-(NSDictionary*)changePwdWithTel:(NSString*)tel pwd:(NSString*)pwd newPwd:(NSString*)newPwd;

//密码重置
//http://idriven.oicp.net:8080/khd/ForgetPwServlet?usertel=&captcha=&pwd=
-(NSDictionary*)forgetPwdWithTel:(NSString*)tel captcha:(NSString*)cha pwd:(NSString*)pwd sessionID:(NSString*)sessionID;

//获取代理商列表
//http://idriven.oicp.net:8080/khd/Agents.jsp?city=0571
-(NSArray*)getAgentListWithCityCode:(NSString*)citycode;

//获取代理商基本信息
//http://idriven.oicp.net:8080/khd/Agent.jsp?agentID=
//{"agentLogo":"/a.jpg","agentName":"代理商1","agentAddress":"杭州","ratestandard":"收费标准","introduction":"代理商1是一家……的公司"}
-(NSDictionary*)getAgentBasicInfoWithID:(NSString*)agentID;


//用户信息获取
//http://idriven.oicp.net:8080/khd/user.jsp?userTel=
-(NSDictionary*)getUserInfoWithTel:(NSString*)tel;

//用户信息保存
//http://idriven.oicp.net:8080/khd/SaveUserServlet?userTel= +logo+name+ sex
-(NSDictionary*)saveUserWithTel:(NSString*)tel logo:(NSString*)logo name:(NSString*)name sex:(NSString*)sex;

//返驾指定位置最近的司机
- (NSArray*)getNearestDriverList:(NSString*)userTel
                    Withlatitude:(double)lat
                   Withlongitude:(double)lon
                withDriverNumber:(NSInteger)number
                withDirverStatus:(NSInteger)status;
//司机位置
//http://idriven.oicp.net:8080/khd/dirverLocation.jsp?lon=&lat=
-(NSArray*)getDriverListWithlatitude:(double)lat longitude:(double)lon;

//提交订单
//http://idriven.oicp.net:8080/khd/SaveOrderServlet.jsp?userTel+name+agentID+ startAddress+endAddress+daijiTime+driverNum
-(NSDictionary*)saveOrderWithTel:(NSString*)tel clientID:(NSString*)clientID contactName:(NSString*)name contactTel:(NSString*)contactTel start:(NSString*)startAddr end:(NSString*)endAddr time:(NSString*)time driverNum:(NSString*)num lon:(double)lon lat:(double)lat accuracy:(double)acc;

//预约清单列表
//http://idriven.oicp.net:8080/khd/orders.jsp?userTel=
-(NSArray*)getOrderListWithTel:(NSString*)tel;

//预约清单详情
//http://idriven.oicp.net:8080/khd/order.jsp?orderID=
-(NSDictionary*)getOrderDetail:(NSString*)orderID;

//取消订单
//http://idriven.oicp.net:8080/khd/cancelorder.jsp?orderID=
-(NSDictionary*)cancelOrder:(NSString*)orderID;

//二维码获取
//http://idriven.oicp.net:8080/khd/DimensionalCodeServlet?userTel=
-(NSDictionary*)getDimensionalCodeWithTel:(NSString*)tel;

//请客二维码获取
//http://idriven.oicp.net:8080/khd/GuestDCodeServlet?userTel=&guestTel=
-(NSDictionary*)getGuestDCodeWithTel:(NSString*)tel guestTel:(NSString*)guestTel;

//会员卡信息
//http://idriven.oicp.net:8080/khd/member.jsp?userTel=
-(NSDictionary*)getMembercardInfoWithTel:(NSString*)tel;

//消费记录列表
//http://idriven.oicp.net:8080/khd/conRecords.jsp?userTel=
-(NSArray*)getConsumeListWithTel:(NSString*)tel;

//消费记录详情
//http://idriven.oicp.net:8080/khd/conRecord.jsp?orderID=
-(NSDictionary*)getConsumeDetailWithOrderID:(NSString*)orderID;

//请客记录列表
//http://idriven.oicp.net:8080/khd/guestRecords.jsp?userTel=
-(NSArray*)getGuestConsumeListWithTel:(NSString*)tel;

//请客记录详情
//http://idriven.oicp.net:8080/khd/guestRecord.jsp?orderID=
-(NSDictionary*)getGuestConsumeDetailWithOrderID:(NSString*)orderID;

//是否评价
//http://idriven.oicp.net:8080/khd/IsNotEvaluateServlet?orderID=
-(NSDictionary*)isNotEvaluateWithOrderID:(NSString*)orderID;

//评价
//http://idriven.oicp.net:8080/khd/EvaluateServlet?orderID=&evaluate=&level=&star=
-(NSDictionary*)evaluateWithOrderID:(NSString*)orderID evalute:(NSString*)evalute level:(NSString*)level star:(NSString*)star;

//代理商系统信息
//http://idriven.oicp.net:8080/khd/SysInformationServlet?agentID=
-(NSArray*)getSystemInfoWithAgentID:(NSString*)agentID;

//修改代理商
//http://idriven.oicp.net:8080/khd/CAgentServlet?userTel=&agentID=
-(NSDictionary*)changeAgentWithTel:(NSString*)tel agentID:(NSString*)agentID;

//版本检测
//http://idriven.oicp.net:8080/khd/VersionServlet?versionID=
//http://idriven.oicp.net:8080/khd/update/ver.json
-(NSDictionary*)checkVersionWithVerID:(NSString*)verID;

@end

