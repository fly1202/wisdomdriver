//
//  main.m
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-6.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
