//
//  AppDelegate.h
//  WisdomDriver
//
//  Created by JohnsonLee on 13-1-6.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BMapKit.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    UINavigationController * JunYunNav;
    BMKMapManager* _mapManager;
}

@property (strong, nonatomic) UIWindow *window;

- (void)initLocalInfo;

@end
