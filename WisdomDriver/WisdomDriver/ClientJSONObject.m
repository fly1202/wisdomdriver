//
//  ClientJSONObject.m
//  WisdomDriver
//
//  Created by  apple on 13-1-12.
//  Copyright (c) 2013年 JunYunKJ. All rights reserved.
//

#import "ClientJSONObject.h"
#import "ASIFormDataRequest.h"
#import "SBJson.h"
#import "defines.h"

static ClientJSONObject *clientJson;

@interface ClientJSONObject()
-(id)soapWithUrl:(NSString*)url error:(NSError**)error;
@end

@implementation ClientJSONObject

-(id)init
{
    self=[super init];
    if (self) {
        
    }
    return self;
}

+(ClientJSONObject *)clientJSONObject
{
    if (!clientJson) {
        clientJson=[[ClientJSONObject alloc] init];
    }
    
    return clientJson;
}

// common soap method
-(id)soapWithUrl:(NSString*)urlStr error:(NSError**)error{
    NSLog(@"soapWithUrl:%@",urlStr);
    /*
    //post 方式
    NSArray * arr = [urlStr componentsSeparatedByString:@"?"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[arr objectAtIndex:0]]];
     [request addRequestHeader:@"Content-Type" value:@"text/x-gwt-rpc; charset=utf-8"];
     [request setRequestMethod:@"POST"];
    NSLog(@"%@",[arr objectAtIndex:0]);
    NSArray * arrKeyVlaue = [[arr objectAtIndex:1] componentsSeparatedByString:@"&"];
    for (int i = 0; i < arrKeyVlaue.count; i++) {
        NSArray * components = [[arrKeyVlaue objectAtIndex:i] componentsSeparatedByString:@"="];
        NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
        [request setPostValue:[[components objectAtIndex:1] stringByAddingPercentEscapesUsingEncoding:enc] forKey:[components objectAtIndex:0]];
        
        //NSData* data = [[components objectAtIndex:1] dataUsingEncoding:NSUTF8StringEncoding];
        //[request setPostValue:data forKey:[components objectAtIndex:0]];
        
        NSLog(@"POST key:%@ value:%@",[components objectAtIndex:0],[[components objectAtIndex:1] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
    }
    */
    
    //url 方式
    //NSString * str = [urlStr stringByAddingPercentEscapesUsingEncoding:CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000)];
    NSString * str = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"soapWithUrl:%@",str);
    NSURL *url = [NSURL URLWithString:str];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
     request.responseEncoding = NSUTF8StringEncoding;
     
    
    [request startSynchronous];
    NSError * asierror = [request error];
    if (!asierror) {
        
    }else{
        NSLog(@"soap error:%@",asierror.localizedDescription);
    }
    
    NSString *response = [request responseString];
    NSLog(@"response string:%@",response);
    SBJsonParser *jsonParser = [[SBJsonParser alloc] init];
    id jsonobject = [[[jsonParser objectWithString:response] copy] autorelease];
    [jsonParser release];
    jsonParser = nil;
    return jsonobject;
    
    *error = [asierror retain];
    return nil;
}

//获取广告图片
//http://idriven.oicp.net:8080/khd/AdsServlet?agentID=
-(NSArray *)getADListWithAgentID:(NSString*)agentID{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/AdsServlet?agentID=%@",HTTP_PREFIX,agentID];
    NSLog(@"getADListWithAgentID:");
    NSError * error = nil;
    NSArray * dict = (NSArray *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//获取验证码
//http://idriven.oicp.net:8080/khd/CaptchaServlet?userTel=
-(NSDictionary*)getCaptcha:(NSString*)tel{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/CaptchaServlet?userTel=%@",HTTP_PREFIX,tel];
    NSLog(@"getCaptcha:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//登陆
//http://idriven.oicp.net:8080/khd/LoginServlet?userTel=&pwd=
-(NSDictionary*)loginWithTel:(NSString*)tel pwd:(NSString*)pwd{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/LoginServlet?userTel=%@&pwd=%@",HTTP_PREFIX,tel,pwd];
    NSLog(@"loginWithTel:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//注册
//http://idriven.oicp.net:8080/khd/RegisterServlet?userTel=18688969596&captcha=2311%name=camel&pwd=1234&agentID=10&sessionID=
-(NSDictionary*)regWithTel:(NSString*)tel captcha:(NSString*)code name:(NSString*)name pwd:(NSString*)pwd agentID:(NSString*)agentID sessionID:(NSString*)sessionID{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/RegisterServlet?userTel=%@&captcha=%@&name=%@&pwd=%@&agentID=%@&sessionID=%@",HTTP_PREFIX,tel,code,name,pwd,agentID,sessionID];
    NSLog(@"regWithTel:captcha:name:pwd:agentID:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//密码修改
//http://idriven.oicp.net:8080/khd/ChangePwServlet.jsp?userTel+pwd +newPwd
-(NSDictionary*)changePwdWithTel:(NSString*)tel pwd:(NSString*)pwd newPwd:(NSString*)newPwd
{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/ChangePwServlet?userTel=%@&pwd=%@&newPwd=%@",HTTP_PREFIX,tel,pwd,newPwd];
    NSLog(@"changePwdWithTel:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//密码重置
//http://idriven.oicp.net:8080/khd/ForgetPwServlet?usertel=&captcha=&pwd=&sessionID=
-(NSDictionary*)forgetPwdWithTel:(NSString*)tel captcha:(NSString*)cha pwd:(NSString*)pwd sessionID:(NSString*)sessionID{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/ForgetPwServlet?userTel=%@&captcha=%@&pwd=%@&sessionID=%@",HTTP_PREFIX,tel,cha,pwd,sessionID];
    NSLog(@"forgetPwdWithTel:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}
//用户信息获取
//http://idriven.oicp.net:8080/khd/user.jsp?userTel=
-(NSDictionary*)getUserInfoWithTel:(NSString*)tel{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/user.jsp?userTel=%@",HTTP_PREFIX,tel];
    NSLog(@"getUserInfoWithTel:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//用户信息保存
//http://idriven.oicp.net:8080/khd/SaveUserServlet?userTel= +logo+name+ sex(userTel+ clientLogo（暂不提供上传）+name+ sex)
-(NSDictionary*)saveUserWithTel:(NSString*)tel logo:(NSString*)logo name:(NSString*)name sex:(NSString*)sex{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/SaveUserServlet?userTel=%@&name=%@&sex=%@",HTTP_PREFIX,tel,name,sex];
    NSLog(@"saveUserWithTel:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//返驾指定位置最近的司机

//http://119.37.193.93/khd/driverLocation?usertel=&lon=&lat=?qty=?stat=
//http://119.37.193.93/khd/driverLocation?usertel=18667931202&lon=120.170397&lat=30.174728&qty=-1&stat=3
- (NSArray*)getNearestDriverList:(NSString*)userTel
                    Withlatitude:(double)lat
                   Withlongitude:(double)lon
                withDriverNumber:(NSInteger)number
                withDirverStatus:(NSInteger)status{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/driverLocation?usertel=%@&lon=%f&lat=%f&qty=%d&stat=%d",HTTP_PREFIX,userTel,lon,lat,number,status];
    NSLog(@"getNearestDriverListWithlatitude:");
    NSError * error = nil;
    NSArray * array = (NSArray *)[self soapWithUrl:urlStr error:(&error)];
    return array;
}

//司机位置
//http://idriven.oicp.net:8080/khd/dirverLocation.jsp?lon=&lat=
-(NSArray*)getDriverListWithlatitude:(double)lat longitude:(double)lon{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/dirverLocation.jsp?lon=%f&lat=%f",HTTP_PREFIX,lon,lat];
    NSLog(@"getDriverListWithlatitude:");
    NSError * error = nil;
    NSArray * array = (NSArray *)[self soapWithUrl:urlStr error:(&error)];
    return array;
}

//获取代理商列表
//http://idriven.oicp.net:8080/khd/agents.jsp?city=0571
-(NSArray*)getAgentListWithCityCode:(NSString*)citycode{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/Agents.jsp?city=%@",HTTP_PREFIX,citycode];
    NSLog(@"getAgentListWithCityCode:");
    NSError * error = nil;
    NSArray * array = (NSArray *)[self soapWithUrl:urlStr error:(&error)];
    return array;
}

//获取代理商基本信息
//http://idriven.oicp.net:8080/khd/Agent.jsp?agentID=
-(NSDictionary*)getAgentBasicInfoWithID:(NSString*)agentID{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/Agent.jsp?agentID=%@",HTTP_PREFIX,agentID];
    NSLog(@"getAgentBasicInfoWithID:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//代理商系统信息
//http://idriven.oicp.net:8080/khd/SysInformationServlet?agentID=
-(NSArray*)getSystemInfoWithAgentID:(NSString*)agentID{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/SysInformationServlet?agentID=%@",HTTP_PREFIX,agentID];
    NSLog(@"getSystemInfoWithAgentID:");
    NSError * error = nil;
    NSArray * dict = (NSArray *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//获取二维码
//http://idriven.oicp.net:8080/khd/DimensionalCodeServlet?userTel=
-(NSDictionary*)getDimensionalCodeWithTel:(NSString*)tel{
    NSLog(@"getDimensionalCodeWithTel:");
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/DimensionalCodeServlet?userTel=%@",HTTP_PREFIX,tel];
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//请客二维码获取
//http://idriven.oicp.net:8080/khd/GuestDCodeServlet?userTel=&guestTel=
-(NSDictionary*)getGuestDCodeWithTel:(NSString*)tel guestTel:(NSString*)guestTel{
    NSLog(@"getGuestDCodeWithTel:");
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/DimensionalCodeServlet?userTel=%@&guestTel=%@",HTTP_PREFIX,tel,guestTel];
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//提交订单
//http://idriven.oicp.net:8080/khd/SaveOrderServlet.jsp?userTel + clientID+contactName+contactTel+ startAddress+endAddress+daijiaTime+driverNum+lon+lat+accuracy
-(NSDictionary*)saveOrderWithTel:(NSString*)tel clientID:(NSString*)clientID contactName:(NSString*)name contactTel:(NSString*)contactTel start:(NSString*)startAddr end:(NSString*)endAddr time:(NSString*)time driverNum:(NSString*)num lon:(double)lon lat:(double)lat accuracy:(double)acc{
    NSString * lonStr = [NSString stringWithFormat:@"%5f",lon];
    NSString * latStr = [NSString stringWithFormat:@"%5f",lat];
    NSString * accStr = [NSString stringWithFormat:@"%5f",acc];
    
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/SaveOrderServlet?userTel=%@&clientID=%@&contactName=%@&contactTel=%@&startAddress=%@&endAddress=%@&daijiaTime=%@&driverNum=%@&lon=%@&lat=%@&accuracy=%@",HTTP_PREFIX,tel,clientID,name,contactTel,startAddr,endAddr,time,num,lonStr,latStr,accStr];
    NSLog(@"saveOrderWithTel:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//预约清单列表
//http://idriven.oicp.net:8080/khd/orders.jsp?userTel=
-(NSArray*)getOrderListWithTel:(NSString*)tel{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/orders.jsp?userTel=%@",HTTP_PREFIX,tel];
    NSLog(@"getOrderListWithTel:");
    NSError * error = nil;
    NSArray * array = (NSArray *)[self soapWithUrl:urlStr error:(&error)];
    return array;
}

//预约清单详情
//http://idriven.oicp.net:8080/khd/order.jsp?orderID=
-(NSDictionary*)getOrderDetail:(NSString*)orderID{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/order.jsp?orderID=%@",HTTP_PREFIX,orderID];
    NSLog(@"getOrderDetail:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//取消订单
//http://idriven.oicp.net:8080/khd/cancelorder.jsp?orderID=
-(NSDictionary*)cancelOrder:(NSString*)orderID{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/cancelorder.jsp?orderID=%@",HTTP_PREFIX,orderID];
    NSLog(@"cancelOrder:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//消费记录列表
//http://idriven.oicp.net:8080/khd/conRecords.jsp?userTel=
-(NSArray*)getConsumeListWithTel:(NSString*)tel{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/conRecords.jsp?userTel=%@",HTTP_PREFIX,tel];
    NSLog(@"getConsumeListWithTel:");
    NSError * error = nil;
    NSArray * array = (NSArray *)[self soapWithUrl:urlStr error:(&error)];
    return array;
}

//消费记录详情
//http://idriven.oicp.net:8080/khd/conRecord.jsp?orderID=
-(NSDictionary*)getConsumeDetailWithOrderID:(NSString*)orderID{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/conRecord.jsp?orderID=%@",HTTP_PREFIX,orderID];
    NSLog(@"getConsumeDetailWithOrderID:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//请客记录列表
//http://idriven.oicp.net:8080/khd/guestRecords.jsp?userTel=
-(NSArray*)getGuestConsumeListWithTel:(NSString*)tel{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/guestRecords.jsp?userTel=%@",HTTP_PREFIX,tel];
    NSLog(@"getGuestConsumeListWithTel:");
    NSError * error = nil;
    NSArray * array = (NSArray *)[self soapWithUrl:urlStr error:(&error)];
    return array;
}

//请客记录详情
//http://idriven.oicp.net:8080/khd/guestRecord.jsp?orderID=
-(NSDictionary*)getGuestConsumeDetailWithOrderID:(NSString*)orderID{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/guestRecord.jsp?orderID=%@",HTTP_PREFIX,orderID];
    NSLog(@"getGuestConsumeDetailWithOrderID:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//版本检测
//http://idriven.oicp.net:8080/khd/VersionServlet?versionID=
-(NSDictionary*)checkVersionWithVerID:(NSString*)verID{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/VersionServlet?versionID=%@",HTTP_PREFIX,verID];
    NSLog(@"checkVersionWithVerID:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//会员卡信息
//http://idriven.oicp.net:8080/khd/member.jsp?userTel=
-(NSDictionary*)getMembercardInfoWithTel:(NSString*)tel{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/member.jsp?userTel=%@",HTTP_PREFIX,tel];
    NSLog(@"getMembercardInfoWithTel:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//是否评价
//http://idriven.oicp.net:8080/khd/IsNotEvaluateServlet?orderID=
-(NSDictionary*)isNotEvaluateWithOrderID:(NSString*)orderID
{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/IsNotEvaluateServlet?orderID=%@",HTTP_PREFIX,orderID];
    NSLog(@"isNotEvaluateWithOrderID:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//评价
//http://idriven.oicp.net:8080/khd/EvaluateServlet?orderID=&evaluate=&grade=&star=
-(NSDictionary*)evaluateWithOrderID:(NSString*)orderID evalute:(NSString*)evalute level:(NSString*)level star:(NSString*)star{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/EvaluateServlet?orderID=%@&evaluate=%@&level=%@&star=%@",HTTP_PREFIX,orderID,evalute,level,star];
    NSLog(@"evaluateWithOrderID:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

//修改代理商
//http://idriven.oicp.net:8080/khd/CAgentServlet?userTel=&agentID=
-(NSDictionary*)changeAgentWithTel:(NSString*)tel agentID:(NSString*)agentID{
    NSString * urlStr = [NSString stringWithFormat:@"%@khd/CAgentServlet?userTel=%@&agentID=%@",HTTP_PREFIX,tel,agentID];
    NSLog(@"evaluateWithOrderID:");
    NSError * error = nil;
    NSDictionary * dict = (NSDictionary *)[self soapWithUrl:urlStr error:(&error)];
    return dict;
}

@end
